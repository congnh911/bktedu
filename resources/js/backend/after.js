// Loaded after CoreUI app.js
$(function () {
    $.lockfixed("#option-bar", {offset: {top: 55} });

    $("input").attr("spellcheck", "false");
    $("textarea").attr("spellcheck", "false");

    $('.select2').select2({
        allowClear: true,
        width: 'auto'
    });
    $('.file_manager').filemanager('file');


    CKEDITOR.replace('description',{
        language: 'vi',
        customConfig: '/custom/ckeditor_config.js'
    });
});

$(document).on("keyup", ".price", function () {
    formatPrice($(this));
});

//Global function

window.select2 = function () {
    $('.select2').select2({
        allowClear: true,
        width: 'resolve'
    });
};

function addCommas(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = "";
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function formatPrice(obj) {
    var _x = $(obj).val().replace(/[^0-9]+/g, "");
    _x = parseInt(_x);
    if (isNaN(_x)) _x = '';
    if (_x > 999999999999) {
        _x = 999999999999
    }
    $(obj).val(addCommas(_x));
}


$(document).on('click', '.modal_action', function (e) {
    e.preventDefault();
    var element = $(this).data('target');
    $(element).modal('show').find('.modal-content').load($(this).attr('href'));
});

$(document).on('change', '#name', function () {
    var title = $(this).val();
    ChangeToSlug(title);
});

$(document).on('change', 'input[name=avatar]', function () {

});

$(document).on('click', '.action-confirm', function () {
    actionConfirm($(this))
});


function actionConfirm(element) {
    var message = $(element).data('trans-title');
    var method = $(element).data('method');
    var button_cancel = $(element).data('trans-button-cancel');
    var button_confirm = $(element).data('trans-button-confirm');
    var url = $(element).data('href');
    bootbox.confirm({
        message: message,
        buttons: {
            confirm: {
                label: button_confirm,
                className: 'btn-success'
            },
            cancel: {
                label: button_cancel,
                className: 'btn-light'
            }
        },
        callback: function (result) {
            if (result) {
                axios({
                    method: method,
                    url: url,
                })
                    .then(function (response) {
                        if (response.data.success) {
                            $.notify({message: response.data.message, type: 'success'});
                            $('.table').DataTable().ajax.reload();
                        } else {
                            $.notify({message: response.data.message, type: 'danger'});
                            return false;
                        }

                    });
            }
        }
    });
}

$(document).on('change', '.switch-input', function () {
    var _this = $(this);

    var message = $(this).data('trans-title');
    var method = $(this).data('method');
    var button_cancel = $(this).data('trans-button-cancel');
    var button_confirm = $(this).data('trans-button-confirm');

    var url = $(this).data('url');
    var id = $(this).data('id');
    var column = $(this).attr('name');
    var value = $(this).is(':checked') ? 1 : 0;

    bootbox.confirm({
        message: message,
        buttons: {
            confirm: {
                label: button_confirm,
                className: 'btn-success'
            },
            cancel: {
                label: button_cancel,
                className: 'btn-light'
            }
        },
        callback: function (result) {
            if (result) {
                axios({
                    method: method,
                    url: url,
                    params: {
                        id: id,
                        column: column,
                        value: value
                    }
                })
                    .then(function (response) {
                        if (response.data.success) {
                            $.notify({message: response.data.message, type: 'success'});
                            $('table').DataTable().ajax.reload();
                        } else {
                            $.notify({message: response.data.message, type: 'danger'});
                            return false;
                        }

                    });
            } else {
                if (_this.is(':checked')) {
                    _this.prop('checked', false)
                } else {
                    _this.prop('checked', true)
                }
            }
        }
    });

});

function ChangeToSlug(title) {
    var slug;

    //Đổi chữ hoa thành chữ thường
    slug = title.toLowerCase();

    //Đổi ký tự có dấu thành không dấu
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    //Xóa các ký tự đặt biệt
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    //Đổi khoảng trắng thành ký tự gạch ngang
    slug = slug.replace(/ /gi, "-");
    //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
    //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    //Xóa các ký tự gạch ngang ở đầu và cuối
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    //In slug ra textbox có id “slug”
    console.log(slug);
    $('input[name=slug]').val(slug);
}
