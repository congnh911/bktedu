/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// Loaded before CoreUI app.js
import '../bootstrap';
import 'pace';
import '../plugins';
import 'select2';
import 'datatables';
import 'webpack-jquery-ui/sortable'
import 'jquery-validation';
import 'plyr';
