$(function () {
    var frm_blog = $('#frm-blog');

    frm_blog.validate({
        wrapper: "div",
        rules: {
            name: {
                required:true,
                minlength:4
            },
            description: {
                required:true,
                minlength:4
            },
            regular_price: {
                required:true,
                minlength:4
            },
            sale_price: {
                required:true,
                minlength:4
            },
            category_id: {
                required:true,
            }
        },
        messages: {
            name: {
                required:"Tiêu đề không để trống",
                minlength: "Độ dài tối thiểu là 4 ký tự"
            },
            category_id: {
                required:"Danh mục không để trống",
            },
            description: {
                required:"Mô tả không để trống",
                minlength: "Độ dài tối thiểu là 4 ký tự"
            },
            regular_price: {
                required:"Giá niêm yết không để trống",
                minlength: "Độ dài tối thiểu là 4 ký tự"
            },
            sale_price: {
                required:"Giá bán không để trống",
                minlength: "Độ dài tối thiểu là 4 ký tự"
            },
        },
        onfocusout: function(){
            if (frm_blog.valid()) {
                $('button[type=submit]').prop('disabled', false);
            } else {
                $('button[type=submit]').prop('disabled', 'disabled');
            }
        }

    });


    $('select[name=category_id]').select2({
        tags: [],
        language: {
            errorLoading:function(){ return "Nhập tên công ty, tổ chức, đối tác bạn muốn tìm kiếm..." }
        },
        allowClear: true,
        ajax: {
            url: '',
            dataType: 'json',
            type: "GET",
            quietMillis: 50,
            data: function (params) {
                return {term: params.term}
            },
            processResults: function (data) {
                return {
                    results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.id
                        }
                    })
                };
            }
        }

    });
});
