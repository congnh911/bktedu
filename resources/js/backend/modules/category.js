$(function () {
    var frm_category = $('#frm-category');

    frm_category.validate({
        wrapper: "div",
        rules: {
            name: {
                required:true,
                minlength:4
            }
        },
        messages: {
            name: {
                required:"Tiêu đề không để trống",
                minlength: "Độ dài tối thiểu là 4 ký tự"
            }
        },
        onfocusout: function(){
            if (frm_category.valid()) {
                $('button[type=submit]').prop('disabled', false);
            } else {
                $('button[type=submit]').prop('disabled', 'disabled');
            }
        }

    });
});
