class Content {

    constructor() {
    }

    static delete(e) {

        bootbox.confirm('Bạn có chắc muốn xóa nội dung này', function (result) {
            if (result) {
                $(e).parents('.box_content').remove();
            }
        });
    }

    static nameAutocomplete(e) {
        var value = $(e).find('option:selected').text();
        $('input[name=content_name]').val(value);
    }

    static store(e) {
        var frm = $(e);
        frm.validate({
            wrapper: "div",
            rules: {
                content_type: {
                    required: true,
                },
                content_name: {
                    required: true,
                },
                content_summary: {
                    required: true,
                }
            },
            messages: {
                content_type: {
                    required: "Loại nội dung không để trống",
                },
                content_name: {
                    required: "Tiêu đề nội dung không để trống",
                },
                content_summary: {
                    required: "Nội dung không để trống",
                }
            }
        });

        var content = CKEDITOR.instances['content_summary'].getData();
        $('textarea[name=content_summary]').val(content);

        if (frm.valid()) {
            var data = frm.serialize();
            axios({
                method: 'post',
                url: frm.attr('action'),
                data: data
            })
                .then(function (response) {
                    if (response.data.success) {
                        $('.modal').modal('hide');
                        $.notify({message: response.data.message}, {type: 'success'});
                        $('#list-content').append(response.data.data)
                        $('#collapseContent').addClass('show');
                    } else {
                        $.notify({message: response.data.message}, {type: 'danger', z_index: 9999});
                        return false;
                    }

                });

        }

        return false;
    }
    static update(e) {
        var frm = $(e);
        frm.validate({
            wrapper: "div",
            rules: {
                content_type: {
                    required: true,
                },
                content_name: {
                    required: true,
                },
                content_summary: {
                    required: true,
                }
            },
            messages: {
                content_type: {
                    required: "Loại nội dung không để trống",
                },
                content_name: {
                    required: "Tiêu đề nội dung không để trống",
                },
                content_summary: {
                    required: "Nội dung không để trống",
                }
            }
        });

        var content = CKEDITOR.instances['content_summary'].getData();
        $('textarea[name=content_summary]').val(content);

        if (frm.valid()) {
            var data = frm.serialize();
            axios({
                method: 'patch',
                url: frm.attr('action'),
                data: data
            })
                .then(function (response) {
                    if (response.data.success) {
                        $('.modal').modal('hide');
                        $.notify({message: response.data.message}, {type: 'success'});
                        $('#content_item_'+response.data.id).html(response.data.data_html);
                    } else {
                        $.notify({message: response.data.message}, {type: 'danger', z_index: 9999});
                        return false;
                    }

                });

        }

        return false;
    }

    static delete(e){
        var url = $(e).data('url');
        var message = $(e).data('trans-title');
        var button_cancel = $(e).data('trans-button-cancel');
        var button_confirm = $(e).data('trans-button-confirm');

        bootbox.confirm({
            message: message,
            buttons: {
                confirm: {
                    label: button_confirm,
                    className: 'btn-success'
                },
                cancel: {
                    label: button_cancel,
                    className: 'btn-light'
                }
            },
            callback: function (result) {
                if (result) {
                    axios({
                        method: 'delete',
                        url: url,
                    })
                        .then(function (response) {
                            if (response.data.success) {
                                $.notify({message: response.data.message, type: 'success'});
                                $(e).parents('.list-group-item').remove();
                            } else {
                                $.notify({message: response.data.message, type: 'danger'});
                                return false;
                            }

                        });
                }
            }
        });
    }

}

