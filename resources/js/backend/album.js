class Album {

    constructor() {
    }

    static delete(e) {
        var url = $(e).data('url');
        var message = $(e).data('trans-title');
        var button_cancel = $(e).data('trans-button-cancel');
        var button_confirm = $(e).data('trans-button-confirm');

        bootbox.confirm({
            message: message,
            buttons: {
                confirm: {
                    label: button_confirm,
                    className: 'btn-success'
                },
                cancel: {
                    label: button_cancel,
                    className: 'btn-light'
                }
            },
            callback: function (result) {
                if (result) {
                    axios({
                        method: 'delete',
                        url: url,
                    })
                        .then(function (response) {
                            if (response.data.success) {
                                $.notify({message: response.data.message, type: 'success'});
                                $(e).parents('.image-item').remove();
                            } else {
                                $.notify({message: response.data.message, type: 'danger'});
                                return false;
                            }

                        });
                }
            }
        });
    }


    static store(e) {
        var model_type = $(e).data('model_type');
        var model_id = $(e).data('model_id');
        var action = $(e).data('action');

        var data = new FormData();
        data.append('model_type', model_type);
        data.append('model_id', model_id);
        $.each($(e)[0].files, function (i, file) {
            data.append('files[]', file);
        });

        axios({
            method: 'post',
            url: action,
            headers: {
                'content-type': 'multipart/form-data;',
            },
            data: data
        })
            .then(function (response) {
                if (response.data.success) {
                    $('.album-listing').empty();
                    $('.album-listing').append(response.data.data);
                    $('#collapseAlbum').addClass('show')
                } else {

                }

            });
    }

    static update(e) {
        var url = $(e).data('url');
        var column = $(e).attr('name');
        column = column.replace('image_','');
        var value = $(e).val();
        var params = {column:column,value:value};
        axios({
            method: 'patch',
            url: url,
            headers: {
                'content-type': 'multipart/form-data;',
            },
            params: params
        })
            .then(function (response) {
                if (response.data.success) {
                    return true;
                } else {
                    return false;
                }

            });
    }

    static isAvatar(e) {
        var url = $(e).data('url');
        var id = $(e).val();
        var model_type = $(e).data('model_type');
        var model_id = $(e).data('model_id');
        axios({
            method: 'post',
            url: url,
            headers: {
                'content-type': 'multipart/form-data;',
            },
            params: {
                id: id,
                model_type: model_type,
                model_id: model_id,
            }
        })
            .then(function (response) {
                if (response.data.success) {
                    return true;
                } else {
                    return false;
                }

            });
    }
}
