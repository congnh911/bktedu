class File {

    constructor() {
    }

    static delete(e) {
        var url = $(e).data('url');
        var message = $(e).data('trans-title');
        var button_cancel = $(e).data('trans-button-cancel');
        var button_confirm = $(e).data('trans-button-confirm');

        bootbox.confirm({
            message: message,
            buttons: {
                confirm: {
                    label: button_confirm,
                    className: 'btn-success'
                },
                cancel: {
                    label: button_cancel,
                    className: 'btn-light'
                }
            },
            callback: function (result) {
                if (result) {
                    axios({
                        method: 'delete',
                        url: url,
                    })
                        .then(function (response) {
                            if (response.data.success) {
                                $.notify({message: response.data.message, type: 'success'});
                                $(e).parents('li').remove();
                            } else {
                                $.notify({message: response.data.message, type: 'danger'});
                                return false;
                            }

                        });
                }
            }
        });
    }


    static store(e) {
        var model_type = $(e).data('model-type');
        var model_id = $(e).data('model-id');
        var action = $(e).data('action');

        var data = new FormData();
        data.append('model_type', model_type);
        data.append('model_id', model_id);
        $.each($(e)[0].files, function (i, file) {
            data.append('files[]', file);
        });

        axios({
            method: 'post',
            url: action,
            headers: {
                'content-type': 'multipart/form-data;',
            },
            data: data
        })
            .then(function (response) {
                if (response.data.success) {
                    $('.file-attachment').empty();
                    $('.file-attachment').append(response.data.data);
                    $('#collapseFile').addClass('show')
                } else {

                }

            });
    }

    static update(e) {
        var url = $(e).data('url');
        var name = $(e).val();
        axios({
            method: 'patch',
            url: url,
            headers: {
                'content-type': 'multipart/form-data;',
            },
            params: {
                name: name
            }
        })
            .then(function (response) {
                if (response.data.success) {
                    return true;
                } else {
                    return false;
                }

            });
    }
}
