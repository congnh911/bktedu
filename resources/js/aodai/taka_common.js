$(document).ready(function() {
    $(".plus-minus").keydown(function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || // Allow: Ctrl/cmd+A
        (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) || // Allow: Ctrl/cmd+C
        (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) || // Allow: Ctrl/cmd+X
        (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) || // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
        return;
    }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('.items .item:hover .name .quick-view').on('touchstart', function(e) {
        'use strict';
        //satisfy code inspectors
        var link = $(this);
        //preselect the link
        alert(link);
        if (link.hasClass('hover')) {
            return true;
        } else {
            link.addClass('hover');
            $('a.taphover').not(this).removeClass('hover');
            e.preventDefault();
            return false;
            //extra, and to make sure the function has consistent return points
        }
    });
})
function headCart() {
    $('#cart').load('index.php?route=common/cart/info #cart > *');
}
function detectmob() {
    return !(navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i))
}
function stickyHeader() {
    var header = $('.header_navigation')
    , scrollPosition = 0
    , headerTopHeight = $('.header .header__top').outerHeight()
    , checkpoint = 200;
    $(window).scroll(function(event) {
        var currentPosition = $(this).scrollTop();
        if (currentPosition < scrollPosition) {
            // On top
            if (currentPosition == 0) {
                header.removeClass('navigation--sticky navigation--unpin navigation--pin');
                header.css("margin-top", 0);
            }// on scrollUp
            else if (currentPosition > checkpoint) {
                header.removeClass('navigation--unpin').addClass('navigation--sticky navigation--pin');
            }
        }// On scollDown
        else {
            if (currentPosition > checkpoint) {
                header.addClass('navigation--pin navigation--sticky');
                header.css("margin-top", -headerTopHeight);
            }
        }
        scrollPosition = currentPosition;
    });
}
function resizeHeader() {
    var header = $('.header_navigation')
    var checkPoint = header.data('responsive');
    var windowWidth = $(window).innerWidth();
    // mobile
    if (checkPoint > windowWidth) {
        $('.menu').find('.sub-menu').hide();
        header.find('.menu').addClass('menu--mobile');
        $('.menu').prependTo('.header--sidebar');
    } else {
        $('.menu').find('.sub-menu').show();
        header.removeClass('header--mobile');
        header.find('.menu').removeClass('menu--mobile');
        $('.menu--left').prependTo('.navigation__left');
        $('.menu--right').prependTo('.navigation__right');
        $('body').removeClass('menu-sidebar--active');
        $('.header--sidebar').removeClass('active');
        $('.menu-toggle').removeClass('menu-toggle--active');
    }
}
$(document).ready(function() {
    $(window).on('load resize', function() {
        if (detectmob()) {
            resizeHeader();
            stickyHeader();
        }
    });
    if ($('#product-product .description') != undefined) {
        if ($('#product-product .description').height() <= 80) {
            $('.more_detail').remove();
        }
    }
    $('.more_detail').on('click', function(e) {
        e.preventDefault();
        if ($('.details').hasClass('show-more')) {
            $('.details').removeClass('show-more');
            $('.more_detail i').remove();
            $('.more_detail').text($(this).data('than-more') + ' ');
            $('.more_detail').append('<i class="fa fa-plus"></i>');
        } else {
            $('.details').addClass('show-more');
            $('.more_detail i').remove();
            $('.more_detail').text($(this).data('less-more') + ' ');
            $('.more_detail').append('<i class="fa fa-minus"></i>');
        }
    });
    //Search Product Page Caregory
    $('#searchs select#category').on('change', function() {
        if ($(this).find(':selected').data('href')) {
            location.href = $(this).find(':selected').data('href');
        } else {
            renderDetailAlbum($(this).find(':selected'));    
            scrollTop('#searchs');
        }
    });
    $('#searchs input[name="search"]').on('keyup', function() {
        if ($(this).val().length > $(this).data('length-search'))
            renderDetailProduct($(this));
    });
    //Read More
    $(document).delegate('button.readmore', 'click', function() {
        renderDetailAlbum($(this));
    });
    //Read More
    $(document).delegate('.readProduct', 'click', function() {
        $(this).parent().parent().find('.active').removeClass('active');
        $(this).parent().addClass('active');
        renderDetailAlbum($(this), $('button.readmore'));
    });
    $(window).on('resize', function() {
        if ($('.navigation__left').css('display') == 'block') {
            $('.display-mobile').css('display', 'none');
        }
    });
    $(".menu-toggle i.fa-bars").on('click', function(e) {
        $('.display-mobile').slideToggle('slow');
    });
    $('.display-mobile li').bind().click(function(e) {
        $(this).find('>ul').stop(true, true).slideToggle(500).end().siblings().find('>ul').slideUp().parent();
        e.stopPropagation();
    });
    $('.display-mobile li a').click(function(e) {
        e.stopPropagation();
    });
    function renderDetailAlbum(element, removeThis) {
        console.log('hihi');
        if(element.data('id') == 0)
        {
            location.reload();
            return;
        }
        if (!removeThis) {
            removeThis = element;
        }
        if (element.data('position') != undefined)
            var position = element.data('position');
        else
            var position = 'left';
        var lenght = typeof (element.data('lenght')) != 'undefined' ? element.data('lenght') : 1;
        var total = typeof (element.data('total')) != 'undefined' ? element.data('total') : 0;
        var totals = typeof (element.data('totals')) != 'undefined' ? element.data('totals') : 0;
        var ajax = typeof (element.data('ajax')) != 'undefined' ? element.data('ajax') : true;
        var container = typeof (element.data('container')) != 'undefined' ? element.data('container') : '';
        var append = typeof (element.data('append')) != 'undefined' ? element.data('append') : true;
        var reset_total = typeof (element.data('reset')) != 'undefined' ? element.data('reset') : false;
        var page = typeof (element.data('page')) != 'undefined' ? element.data('page') : false;
        console.log(page);
        if (element.data('loading-text') != undefined)
            var loading = true;
        else
            var loading = false;
        var one = 0;
        if (element.data('id') != undefined) {
            if (ajax && element.data('type') != undefined) {
                var href = 'index.php?route=ajax/' + element.data('type') + '&id=' + element.data('id');
                one = 1;
            }
        } else {
            var href = element.data('href');
        }
        if (href) {
            $.ajax({
                url: href,
                type: 'post',
                data: 'position=' + position + '&lenght=' + (typeof (lenght) != 'undefined' ? lenght : 1) + '&total=' + (typeof (total) != 'undefined' ? total : 1) + '&totals=' + totals + '&ajax=' + ajax + '&one=' + one + '&page=' + page,
                dataType: 'json',
                beforeSend: function() {
                    if (loading) {
                        element.button('loading');
                    }
                    $('.layered-navigation-block').show();
                    $('.ajax-loader').show();
                },
                complete: function() {
                    if (loading) {
                        element.button('reset');
                    }
                    $('.layered-navigation-block').hide();
                    $('.ajax-loader').hide();
                },
                success: function(json) {
                    if (json['total'] == element.data('totals')) {
                        $('#button-readmore').addClass('hidden');
                    }
                    if (json['total'] && !reset_total) {
                        element.data('total', json['total']);
                    }
                    html = '';
                    if (json['result_html']) {
                        html = json['result_html'];
                    } else if (json['html'])
                    html = json['html'];
                    if (json['append']) {
                        append = json['append'];
                    }
                    if (html) {
                        if (append) {
                            $('#' + container).append(html);
                        } else {
                            $('#' + container).html('<div class="layered-navigation-block"></div><div class="ajax-loader"><img src="image/catalog/d_quickcheckout/svg-loaders/puff.svg" alt=""></div>' + html);
                        }
                    }
                    if (json['position']) {
                        element.data('position', json['position']);
                    }
                    if (json['disabled']) {
                        $('#button-readmore').addClass('hidden');
                    } else if (json['readmore']) {
                        $('#button-readmore').removeClass('hidden');
                        $('#button-readmore').html(json['readmore'])
                    } else {
                        $('#button-readmore').removeClass('hidden');
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
        scrollTop(element);
    }
    function renderDetailProduct(element) {
        var ajax = typeof (element.data('ajax')) != 'undefined' ? element.data('ajax') : true;
        var container = typeof (element.data('container')) != 'undefined' ? element.data('container') : '';
        var append = typeof (element.data('append')) != 'undefined' ? element.data('append') : true;
        if (element.data('loading-text') != undefined)
            var loading = true;
        else
            var loading = false;
        var search = $(element).val();
        var category_id = $(element).data('category-id');
        var sub_category = $(element).data('sub-category');
        var sort = $(element).data('sort');
        var order = $(element).data('order');
        var limit = $(element).data('limit');
        $.ajax({
            url: 'index.php?route=ajax/search',
            type: 'post',
            data: 'search=' + search + '&category_id=' + category_id + '&sub_category=' + sub_category + '&sort=' + sort + '&order=' + order + '&limit=' + limit,
            dataType: 'json',
            beforeSend: function() {
                if (loading)
                    element.button('loading');
            },
            complete: function() {
                if (loading)
                    element.button('reset');
            },
            success: function(json) {
                if (json['html']) {
                    if (append) {
                        $('#' + container).append(json['html']);
                    } else {
                        $('#' + container).html(json['html']);
                    }
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
        scrollTop(element);
    }
    function scrollTop(div) {
        $("html, body").animate({
            scrollTop: $(div).offset().top - 65
        }, 1000);
    }
    //Product Image Option
    $('.clean-option-image .single-option').on('click', function(e) {
        $('#product-product .thumb-product a img').attr('src', $(this).data('image'));
    });
});
$(document).ready(function() {
    $('.layered.selected').before('<div class="group-color" style="display: none;">' + $('.filter-color #filter-group-color').html() + '</div>');
    // if ($('.sidebar ul') && $('.sidebar ul').height() > 34) {
    //     $('.sidebar').append('<i class="fa fa-plus"></i>');
    // }
    // $(document).delegate('.sidebar .fa', 'click', function() {
    //     if ($(this).hasClass('fa-plus')) {
    //         $('.sidebar').css('height', 'auto');
    //         $(this).removeClass('fa-plus');
    //         $(this).addClass('fa-minus');
    //     } else if ($(this).hasClass('fa-minus')) {
    //         $('.sidebar').css('height', '34');
    //         $(this).removeClass('fa-minus');
    //         $(this).addClass('fa-plus');
    //     }
    // });
});
var layerednavigationajax = {

    /* Filter action */
    'filter': function(filter_url) {
       

        var old_route = 'route=product/category';
        var new_route = 'route=extension/module/layerednavigation/category';

        if (filter_url.search(old_route) != -1) {
            filter_url = filter_url.replace(old_route, new_route);
        }

        if (filter_url.search(new_route) != -1) {
           
            $.ajax({
                url: filter_url,
                type: 'post',
                dataType: 'json',
                beforeSend: function() {
                    $('.layered-navigation-block').show();
                    $('.ajax-loader').show();
                },

                success: function(json) {
                    $('.filter-url').val(json['filter_action']);
                    $('.price-url').val(json['price_action']);
                    $('.price-value').val(json['price_value']);
                    $('.option-value').val(json['option_value']);
                    if (json['append']) {
                        $('.categories').append('<div class="layered-navigation-block"></div><div class="ajax-loader"><img src="image/catalog/d_quickcheckout/svg-loaders/puff.svg" alt="" /></div>' + json['result_html']).attr('id', 'category-product');
                    } else
                    $('.categories').html('<div class="layered-navigation-block"></div><div class="ajax-loader"><img src="image/catalog/d_quickcheckout/svg-loaders/puff.svg" alt="" /></div>' + json['result_html']).attr('id', 'category-product');

                    if (json['layered_html']) {
                        $('.layered.selected').removeClass('hidden').css('padding', '15px 0');
                    } else {
                        $('.layered.selected').removeClass('hidden').css('padding', '0');
                    }
                    if(json['sort_text'] != "")
                    {
                        $("#label_sort").html(json['sort_text']);
                    }
                    $('.layered.selected').html(json['layered_html']);
                    if (json['readmore']) {
                        $('#button-readmore').removeClass('hidden');
                        $('#button-readmore').html(json['readmore'])
                    } else {
                        $('#button-readmore').addClass('hidden');
                    }

                    $(".layernavigation-module .list-group-item").removeClass('show');
                    $('.layered-navigation-block').hide();
                    $('.ajax-loader').hide();
                }
            });
        }

    }
};
$(document).click(function(event) {
    if ($(event.target).hasClass('filter-label') || $(event.target).parent().hasClass('filter-label')) {
        if ($(event.target).hasClass('filter-label')) {
            element = $(event.target).parent()
        } else {
            element = $(event.target).parent().parent()
        }
        target = $(event.target).data('target');
        if (element.find('.list-group-item').hasClass('show')) {
            element.find('.list-group-item').removeClass('show').addClass('hide')
        } else {
            element.find('.list-group-item').removeClass('hide').addClass('show')
        }
        if (target == 'group-color') {
            if ($('.' + target).css('display') == 'none') {
                $('.filter-label .b2u_arrow').css('display', 'block');
            } else if ($('.' + target).css('display') == 'block') {
                $('.filter-label .b2u_arrow').css('display', 'none');
            }
        }
        if (target) {
            if ($('.' + target).css('display') == 'none') {
                $('.' + target).slideToggle("slow");
            } else if ($('.' + target).css('display') == 'block') {
                $('.' + target).slideToggle("slow");
            }
        }
    } else {
        $(".layernavigation-module .list-group-item").removeClass('show');
    }
});
function quickView(href, title) {
    $('#modal-product').remove();
    html = '<div id="modal-product" class="modal fade">';
    html += '  <div class="modal-dialog bounceIn animated modal-lg" style="margin-top: 10px;">';
    html += '    <div class="modal-content">';
    html += '     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
    html += '      <div class="modal-body"></div>';
    html += '    </div>';
    html += '  </div>';
    html += '</div>';
    $('body').addClass('quickview');
    $('body').append(html);
    $('#modal-product').modal('show');
    var element = this;
    $.ajax({
        url: href,
        type: 'get',
        beforeSend: function(argument) {
            $('#modal-product .modal-body').html('<div class="loader"></div>');
        },
        dataType: 'html',
        success: function(data) {
            $('#modal-product .modal-body').html(data);
        }
    });
}
$(document).on('hidden.bs.modal', '#modal-product', function(event) {
    $('body').removeClass('quickview');
});
$(document).delegate('.a-filter', 'click', function() {

    if ($(this).find('label').hasClass('active')) {
        $(this).find('label').removeClass('active');
    } else {
        $(this).find('label').addClass('active');
    }
    var filter_ids;

    var title = $(this).attr('title');
    var option_value_id = $(this).data('option-id');
    filter_url = $('.filter-url').val() + '&append=0';

    if ($(this).hasClass('remove-filter-price') == true) {
        base_value = $('.base-value').val();
        option_url = $('.option-value').val();
        var new_url = base_value + option_url + '&append=0';
        console.log(new_url);
        layerednavigationajax.filter(new_url);

        $(this).parent().remove();
        resetSlider(); // Reset Slider
    } else {
        if ($(this).hasClass('add-filter') == true) {
            if (!$(this).find('label').hasClass('active')) {
                ids = $.grep(ids, function(value) {
                    return value != option_value_id;
                });
            } else {
                ids.push(option_value_id);
            }
        } else if ($(this).hasClass('remove-filter') == true) {
            ids = $.grep(ids, function(value) {
                return value != option_value_id;
            });
        }


        filter_ids = ids.join(',');
        if (filter_ids)
        {
            filter_url += '&filter-option-value-id=' + filter_ids;
        }
        else
        {
            filter_url = filter_url.replace("filter-option-value-id" , "");
        }


        layerednavigationajax.filter(filter_url);
    }
});
$(document).delegate('.filter-attribute-container select#input-sort', 'change', function() {
    filter_url = $('.filter-url').val();
    filter_url = filter_url.split('&price');
    if (filter_url[1] != undefined) {
        layerednavigationajax.filter($(this).val() + '&price' + filter_url[1]);
    } else {
        layerednavigationajax.filter($(this).val());
    }
});
//Price
Number.prototype.format = function(n, x) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
    return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re,'g'), '$&,');
}
;
function getCaculatePrice(str_price, quantity) {

    var symbolright = $("html").attr("currency");
    if(symbolright === "VND")
    {
        symbolright = "VNĐ";
    }
    var price = str_price.split(symbolright);
    price = price.shift().split(",");
    var pr = '';
    for (var n = 0; n < price.length; n++) {
        pr += price[n];
    }
    ;price = pr;
    if (quantity) {
        price_new = price * quantity;
        return price_new.format(0, 3) + ' ' + symbolright;
    } else {
        return str_price;
    }
}
function getPrice(str_price) {
    if (str_price) {
        var symbolright = $("html").attr("currency");
        var price = str_price.split(symbolright);
        price = price.shift().split(",");
        var pr = '';
        for (var n = 0; n < price.length; n++) {
            pr += price[n];
        }
        ;price = pr;
        return price;   
    }
}

// Duc
function resetSlider() {
    var min_price = $("input[name=min_price_cons]").val();
    var max_price = $("input[name=max_price_cons]").val();
    var min_price_f = parseFloat(getPrice(min_price));
    var max_price_f = parseFloat(getPrice(max_price));
    var symbolright = $("html").attr("currency");
    $("#slider-price").slider("values", 0, min_price_f);  
    $("#slider-price").slider("values", 1, max_price_f ); 
    $('#price-from').val(min_price_f.format(0, 3) + ' '+symbolright);
    $('#price-to').val(max_price_f.format(0, 3) + ' '+symbolright);
  // var $slider = $("#slider-price");
  // var current_min_price = parseFloat(getPrice($('#price-from').val()));
  // var current_max_price = parseFloat(getPrice($('#price-to').val()));
  // $slider.slider("values", 0, current_min_price);
  // $slider.slider("values", 1, current_max_price);
}
$(document).delegate('.filter-sort', 'click', function() {
    var data_sort = $(this).attr("data-sort");
    var data_order = $(this).attr("data-order");
    filter_url = $('.filter-url').val() + '&append=0&sort='+data_sort+'&order='+data_order;
    layerednavigationajax.filter(filter_url);
});
// Duc