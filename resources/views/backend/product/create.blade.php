@extends('backend.layouts.app')

@section('title', app_name().' | ' . __('labels.general.create') . ' '. __('labels.general.product'))

@section('content')
    {{ html()->form('POST', route('admin.product.store'))->id('frm-product')->class('form-horizontal')->acceptsFiles()->open() }}

    <div class="row mb-3">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                @lang('labels.general.management')
                @lang('labels.general.product')
                <small class="text-muted">@lang('labels.general.create')</small>
            </h4>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-lg-8">
            <div class="card card-accent-primary">
                <div class="card-header">
                    <strong class="card-title">@lang('labels.general.general')</strong>
                    <div class="card-header-actions">
                        <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                           data-target="#collapseGeneral" aria-expanded="true">
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseGeneral" style="">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-2 form-control-label">@lang('labels.general.title') <span
                                    class="text-danger">(*)</span></label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" maxlength="300" name="name" id="name"
                                       placeholder="{{__('labels.general.title')}}" autofocus value="{!! old('name') !!}">
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="link" class="col-md-2 form-control-label">Link</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" maxlength="305" name="slug" id="link"
                                       placeholder="Link. Ví dụ: ten-danh-muc.html" value="{!! old('link') !!}">
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="ckeditor" class="col-md-2 form-control-label">@lang('labels.general.description')</label>

                            <div class="col-md-10">
                                    <textarea name="description" id="description"
                                              class="editor form-control">{!! old('description') !!}</textarea>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="active" class="col-md-2 form-control-label">@lang('labels.general.active')</label>

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" name="active" id="active"
                                           value="1" {!! old('active',1) == 1 ? 'checked' : '' !!}>
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="active" class="col-md-2 form-control-label">@lang('labels.general.highlight')</label>

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" name="highlight" id="highlight"
                                           value="1" {!! old('highlight',1) == 1 ? 'checked' : '' !!}>
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-accent-info">
                <div class="card-header">
                    <strong class="card-title">@lang('labels.general.info') @lang('labels.general.product')</strong>
                    <div class="card-header-actions">
                        <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                           data-target="#collapseInfoProduct" aria-expanded="true">
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseInfoProduct" style="">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="active" class="form-control-label">@lang('labels.general.avatar')</label>
                            <input type="file" name="avatar">
                            {{--<div class="input-group">--}}
                            {{--<span class="input-group-btn">--}}
                            {{--<a data-input="thumbnail" data-preview="holder" class="btn btn-primary file_manager">--}}
                            {{--<i class="fa fa-picture-o"></i> Choose--}}
                            {{--</a>--}}
                            {{--</span>--}}
                            {{--<input id="thumbnail" class="form-control" type="text" name="avatar">--}}
                            {{--</div>--}}
                            {{--<img id="holder" style="margin-top:15px;max-height:100px;">--}}
                        </div>
                        <div class="form-group">
                            <label for="link" class="form-control-label">@lang('labels.general.category')</label>

                            <select class="select2 form-control" name="category_id[]"
                                    data-placeholder="{{trans('common.root')}}" multiple>
                                @foreach($categories as $key => $product)
                                    <option value="{{$key}}">{{$product}}</option>
                                @endforeach
                            </select>
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="name" class="col-md-4 form-control-label">Giá niêm yết<span
                                    class="text-danger">(*)</span></label>

                            <div class="col-md-8">
                                <input type="text" class="form-control price text-primary" maxlength="11"
                                       name="regular_price" id="regular_price"
                                       placeholder="Giá niêm yết" autofocus value="{!! old('regular_price') !!}">

                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="name" class="col-md-4 form-control-label">Giá Sale<span
                                    class="text-danger">(*)</span></label>

                            <div class="col-md-8">
                                <input type="text" class="form-control price text-danger text-bold" maxlength="11"
                                       name="sale_price" id="sale_price"
                                       placeholder="Giá Sale" autofocus value="{!! old('sale_price') !!}">
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="name" class="col-md-4 form-control-label">Giảm giá<span
                                    class="text-danger">(*)</span></label>

                            <div class="col-md-8">
                                <div class="input-group">
                                    <input type="text" class="form-control price text-bold" maxlength="11"
                                           name="discount" id="discount"
                                           placeholder="Giảm giá" autofocus value="{!! old('discount') !!}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">vnđ</span>
                                    </div>
                                </div>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.includes.seo')

    <div class="row mb-3">
        <div class="col">
            {{ form_cancel(route('admin.product.index',['type' => request()->get('type')]), __('buttons.general.cancel')) }}
        </div><!--col-->

        <div class="col text-right">
            {{ form_submit(__('buttons.general.crud.create')) }}
        </div><!--col-->
    </div><!--row-->

    {{ html()->form()->close() }}
@endsection
