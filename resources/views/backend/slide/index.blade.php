@extends('backend.layouts.app')

@section('title', app_name().' | '. __('labels.general.management') . ' ' . __('labels.general.slide'))

@section('content')
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-controls="home">
                <i class="icon-calculator"></i> @lang('labels.general.all')
                {{--<span class="badge badge-success">New</span>--}}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="all" role="tabpanel">
            <form method="get" action="{{route('admin.slide.index')}}" enctype="multipart/form-data">
                @include('backend.slide.includes.filter')
            </form>
            <div class="row mt-4">
                <div class="col-sm-6">
                    <h4 class="card-title mb-0">
                        @lang('labels.general.management') @lang('labels.general.slide')
                        <a href="{{ route('admin.slide.index',['type' => request()->get('type')] ) }}"
                           class="btn btn-{{empty(request()->get('trashed')) ? 'success' : '' }} btn-xs ml-1"
                           data-toggle="tooltip" title="">
                            <i class="fas fa-list"></i> @lang('labels.general.active')
                        </a>
                    </h4>
                </div><!--col-->

                <div class="col-sm-6">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.add')">
                        <a href="{{ route('admin.slide.create',['type' => request()->get('type')]) }}"
                           class="btn btn-light btn-sm ml-1"
                           data-toggle="tooltip" title="@lang('labels.general.add')"><i
                                class="fas fa-plus-circle"></i> @lang('labels.general.add')</a>
                    </div><!--btn-toolbar-->
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-2">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table table-hover nowap" id="slide-table">
                            <thead>
                            <tr>
                                <th style="width: 20px">ID</th>
                                <th style="width: 20%">@lang('labels.general.gallery')</th>
                                <th>@lang('labels.general.title')</th>
                                <th style="width: 15%">@lang('labels.general.position')</th>
{{--                                <th style="width: 15%">@lang('labels.general.highlight')</th>--}}
                                <th style="width: 15%">@lang('labels.general.status')</th>
                                <th style="width: 15%">@lang('labels.general.created_at')</th>
                                <th style="width: 10%">@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div>
    </div>
@endsection
@push('after-scripts')
    <script type="text/javascript">
        var slide_table = $('#slide-table');
        $(function () {
            slide_table.dataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bAutoWidth: false,
                bLengthChange: false,
                searching: false,
                iDisplayLength: 20,
                bSortable: false,
                jQueryUI: true,
                select: {
                    style: 'multi'
                },
                ajax: {
                    url: '{{route('admin.slide.get')}}',
                    type: 'get',
                    data: {
                        query: '{{ base64_encode(urldecode(request()->getQueryString())) }}'
                    }
                },
                columns: [
                    {sortable: false, name: 'id', data: 'id'},
                    {sortable: false, name: 'gallery', data: 'gallery'},
                    {sortable: false, name: 'name', data: 'name'},
                    {sortable: false, name: 'position', data: 'position'},
                    // {sortable: false, name: 'highlight', data: 'highlight'},
                    {sortable: false, name: 'active', data: 'active'},
                    {sortable: false, name: 'created_at', data: 'created_at'},
                    {sortable: false, name: 'actions', data: 'actions'},
                ],
                language: {
                    "lengthMenu": "Hiển thị _MENU_ bản ghi",
                    "zeroRecords": "Không tìm bản ghi phù hợp",
                    "infoEmpty": "Không có dữ liệu",
                    "infoFiltered": "(lọc từ tổng số _MAX_ bản ghi)",
                    "search": "Tìm kiếm:",
                    "searchPlaceholder": "Tìm kiếm",
                    "info": "Hiển thị từ _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "paginate": {
                        "first": "Đầu tiên",
                        "last": "Cuối cùng",
                        "next": "Sau",
                        "previous": "Trước"
                    },
                    "sProcessing": '<i class="fa fa-spinner fa-pulse fa-fw"></i> Đang lấy dữ liệu'
                },
                //order: [[0, "desc"]],
                searchDelay: 500,
            });

        });

    </script>
@endpush
