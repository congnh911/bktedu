@extends('backend.layouts.app')

@section('title', app_name().' | Thêm mới sản phẩm')

@section('content')
    {{ html()->form('POST', route('admin.slide.store'))->id('frm-slide')->class('form-horizontal')->acceptsFiles()->open() }}

    <div class="row mb-3">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                Quản lý Slide
                <small class="text-muted">Thêm mới</small>
            </h4>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-accent-primary">
                <div class="card-header">
                    <strong class="card-title">Thông tin chung</strong>
                    <div class="card-header-actions">
                        <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                           data-target="#collapseGeneral" aria-expanded="true">
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseGeneral" style="">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-2 form-control-label">Tiêu đề <span
                                    class="text-danger">(*)</span></label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" maxlength="300" name="name" id="name"
                                       placeholder="Tiêu đề" autofocus value="{!! old('name') !!}">
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="ckeditor" class="col-md-2 form-control-label">Mô tả</label>

                            <div class="col-md-10">
                                    <textarea name="description"
                                              class="editor form-control">{!! old('description') !!}</textarea>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="ckeditor" class="col-md-2 form-control-label">Vị trí</label>

                            <div class="col-md-10">
                                    <select name="position" class="select2 form-control" data-placeholder="Chọn Vị trí">
                                        <option value="">Chọn vị trí</option>
                                        @if(config('slide.position'))
                                            @foreach(config('slide.position') as $key => $item)
                                                <option value="{{$item['id']}}" {{$data->position == $item['id'] ? 'selected' : ''}} >{{$item['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="active" class="col-md-2 form-control-label">Hoạt động</label>

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" name="active" id="active"
                                           value="1" {!! old('active',1) == 1 ? 'checked' : '' !!}>
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="active" class="col-md-2 form-control-label">Nổi bật</label>

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" name="highlight" id="highlight"
                                           value="1" {!! old('highlight',1) == 1 ? 'checked' : '' !!}>
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col">
            {{ form_cancel(route('admin.slide.index',['type' => request()->get('type')]), 'Hủy') }}
        </div><!--col-->

        <div class="col text-right">
            {{ form_submit('Thêm mới') }}
        </div><!--col-->
    </div><!--row-->

    {{ html()->form()->close() }}
@endsection
