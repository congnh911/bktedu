<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel2">{{$data->name}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
</div>

<div class="modal-body">
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <strong>{{__('labels.general.description')}}: </strong>
                @if(!empty($data->description))
                    <p>{!! $data->description !!}</p>
                @endif
            </div>
            <div class="form-group">
                <strong>{{__('labels.general.active')}}: </strong>
                @if(!empty($data->active == 1))
                    <span class="badge badge-success">{{__('labels.general.yes')}}</span>
                @else
                    <span class="badge badge-secondary">{{__('labels.general.no')}}</span>
                @endif
            </div>
            <div class="form-group">
                <strong>{{__('labels.general.highlight')}}: </strong>
                @if(!empty($data->highlight == 1))
                    <span class="badge badge-success">{{__('labels.general.yes')}}</span>
                @else
                    <span class="badge badge-secondary">{{__('labels.general.no')}}</span>
                @endif
            </div>
            <div class="form-group">
                <strong class="form-control-label">{{__('labels.general.avatar')}}</strong>

                <div class="form-group">
                    @if(!empty($data->path))
                        <div class="image-thumnai image-avatar" style="width: 200px">
                            <div title="Xóa file" class="button-delete-image"><i
                                    class="fas fa-times"></i></div>
                            <img id="holder" src="{!! $data->avatar() !!}">
                        </div>
                    @endif

                </div><!--col-->
            </div><!--form-group-->
            <div class="form-group">
                <strong class="form-control-label">{{__('labels.general.file-attach')}}</strong>
                <div class="form-group">
                    <ul class="file-attachment list-group ml-10">
                        @if(!empty($files))
                            @foreach($files as $file)
                                <li>
                                    <div class="file-icon-type text-grey font18"></div>
                                    <a target="_blank"
                                       href="{{ route('admin.file.view',['type' => $file->type, 'id' => $file->id]) }}"
                                       data-toggle="tooltip" title="{{ $file->original_name }}"><i
                                            class="fa fa-paperclip"></i> {{ $file->original_name }}
                                    </a><br/>
                                    <small class="text-gray">Size
                                        : {{ number_format($file->original_size) }} KB
                                    </small>
                                    <div class="icon-download">
                                        <a href="{{ route('admin.file.download',$file) }}"
                                           class="btn btn-sm btn-light" data-toggle="tooltip"
                                           title="Download File"><i
                                                class="fa fa-cloud-download"></i> {{__('labels.general.download')}}</a>

                                        <a href="{{ route('admin.file.delete', $file).'?url='.Request::url() }}"
                                           data-method="delete"
                                           data-trans-button-cancel="{{__('buttons.general.cancel')}}"
                                           data-trans-button-confirm="{{__('buttons.general.crud.delete')}}"
                                           data-trans-title="{{__('strings.backend.general.are_you_sure')}}"
                                           class="btn btn-sm btn-danger">{{__('buttons.general.crud.delete')}}</a>
                                    </div>
                                </li>
                            @endforeach
                        @endif

                    </ul>
                </div><!--col-->
            </div><!--form-group-->
            @include('backend.includes.partials.show_baseinfo',['data' => $data])
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-light" data-dismiss="modal">{{__('labels.general.close')}}</button>
    {{--<button type="button" class="btn btn-primary">Đồng ý</button>--}}
</div>
