<div class="box">
    <input type="hidden" name="type" value="{{request()->get('type')}}">
    <input type="hidden" name="trashed" value="{{request()->get('trashed')}}">

    <div class="row">
        <div class="col-md-6 col-xs-12">
            <input type="text" class="form-control" value="{{request()->get('name')}}" name="name"
                   placeholder="{{__('labels.general.title')}}" autocomplete="off">
        </div>
        @if(!empty($module) && $module == 'category')
            <div class="col-md-3 col-xs-12">
                <select name="parent[]" class="select2 form-control" data-placeholder="Parent">
                    <option value="">Parent</option>
                    @if(!empty($categories) && count($categories) > 0)
                        @foreach($categories as $category)
                            <option value="{{$category->id}}"
                                {{request()->get('parent') == $category->id ? 'selected' : ''}}>
                                {{$category->name}}
                            </option>
                        @endforeach
                    @endif
                </select>
            </div>
        @endif
        <div class="col-md-3 col-xs-12">
            <select name="category_id[]" class="select2 form-control category_product"
                    data-placeholder="@lang('labels.general.category')" multiple>
                <option value="">@lang('labels.general.category')</option>
                @if(!empty($category_product_selected) && count($category_product_selected) > 0)
                    @foreach($category_product_selected as $key => $category)
                        <option value="{{$category->id}}"  selected>
                            {{$category->name}}
                        </option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-md-2 col-xs-12">
            <select name="active" class="select2 form-control" data-placeholder="@lang('labels.general.status')">
                <option value="">@lang('labels.general.status')</option>
                <option value="1" {{request('active') != null && request('active') == 1 ? 'selected' : ''}}>
                    @lang('labels.general.active')
                </option>
                <option value="0" {{request('active') != null && request('active') == 0 ? 'selected' : ''}}>
                    @lang('labels.general.inactive')
                </option>
            </select>
        </div>
        <div class="col-1">
            <button type="submit" class="form-control btn btn-light">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
    <div class="collapse" id="collapseFilter">
    </div>
    <a data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter">Mở rộng bộ lọc
    </a>
</div>
