@extends('backend.layouts.app')

@section('title', app_name().' | '.__('labels.general.update') . ' ' . __('labels.general.slide'))

@section('content')
    {{ html()->form('PATCH', route('admin.slide.update',$data))->id('frm-slide')->class('form-horizontal')
    ->acceptsFiles()->open() }}
    <input type="hidden" name="_method" value="PATCH"/>
    <div class="bg-light p-2 block" id="option-bar" style="margin-bottom:20px;z-index: 9;box-shadow: 0 0 10px rgba(0,0,0,0.2)">
        <div class="row">
            <div class="col-12 text-right">
                {{ form_submit('<i class="fa fa-upload"></i> ' .__('labels.general.update')) }}
                {{ form_cancel(route('admin.slide.index',['type' => request()->get('type')]), __('buttons.general.cancel')) }}
            </div><!--col-->
        </div>
    </div><!--row-->
    <div class="row mb-3 mt-3">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                @lang('labels.general.management') @lang('labels.general.slide')
                <small class="text-muted">@lang('labels.general.update')</small>
            </h4>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-12">
            <div class="card card-accent-primary">
                <div class="card-header">
                    <strong class="card-title">@lang('labels.general.general')</strong>
                    <div class="card-header-actions">
                        <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                           data-target="#collapseGeneral" aria-expanded="true">
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseGeneral" style="">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-2 form-control-label">@lang('labels.general.title') <span
                                    class="text-danger">(*)</span></label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" maxlength="300" name="name" id="name"
                                       placeholder="@lang('labels.general.title') " autofocus
                                       value="{!! old('name',$data->name) !!}">
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="ckeditor"
                                   class="col-md-2 form-control-label">@lang('labels.general.description')</label>

                            <div class="col-md-10">
                            <textarea name="description"
                                      class=" form-control" rows="5">{!! old('description',$data->description) !!}</textarea>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="ckeditor" class="col-md-2 form-control-label">Vị trí</label>

                            <div class="col-md-10">
                                <select name="position" class="select2 form-control" data-placeholder="Chọn Vị trí">
                                    <option value="">Chọn vị trí</option>
                                    @if(config('slide.position'))
                                        @foreach(config('slide.position') as $key => $item)
                                            <option value="{{$item['id']}}" {{$data->position == $item['id'] ? 'selected' : ''}} >{{$item['name']}}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="active"
                                   class="col-md-2 form-control-label">@lang('labels.general.active')</label>

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" name="active" id="active"
                                           value="1" {!! old('active',$data->active) == 1 ? 'checked' : '' !!}>
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="highlight"
                                   class="col-md-2 form-control-label">@lang('labels.general.highlight')</label>

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" name="highlight" id="highlight"
                                           value="1" {!! old('highlight',$data->highlight) == 1 ? 'checked' : '' !!}>
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.includes.album',['model_id' => $data->id,'model_type' => $data->getMorphClass()])
    {{ html()->form()->close() }}
@endsection
@push('after-scripts')
    <script type="text/javascript">
        $(function () {

        })
    </script>
@endpush
