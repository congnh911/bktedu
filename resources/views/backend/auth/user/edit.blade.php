@extends('backend.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.edit'))

@section('breadcrumb-links')
    @include('backend.auth.user.includes.breadcrumb-links')
@endsection

@section('content')
    {{ html()->modelForm($user, 'PATCH', route('admin.auth.user.update', $user->id))->class('form-horizontal')->open() }}

    <div class="row mb-3">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                @lang('labels.backend.access.users.management')
                <small class="text-muted">@lang('labels.backend.access.users.edit')</small>
            </h4>
        </div><!--col-->
    </div><!--row-->

    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    <span class="card-title text-center"><strong>Avatar</strong></span>
                </div>
                <div class="card-body">
                    <img style="height: 200px;width: 100%;object-fit: cover;object-position: center center"
                         class="img-responsive"
                         src="https://www.gravatar.com/avatar/5b08c3317e3ddb52a3904de887ce8aa7.jpg?s=80&amp;d=mm&amp;r=g"
                         onclick="clickAvatar()">
                    <hr/>
                    <h3 class="profile-username text-center">Nguyễn Thị Yến</h3>
                    <p class="text-muted text-center">Marketing Tago (Nhân viên)</p>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <span class="card-title"><strong>Thông tin lý lịch</strong></span>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.first_name'))->class('form-control-label')->for('first_name') }}

                                {{ html()->text('first_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.first_name'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.last_name'))->class('form-control-label')->for('last_name') }}

                                {{ html()->text('last_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.last_name'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.email'))->class('form-control-label')->for('email') }}

                                {{ html()->email('email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.email'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.phone'))->class('form-control-label')->for('phone') }}

                                {{ html()->text('phone')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.phone'))
                                    ->attribute('maxlength', 191)}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.home_land'))->class('form-control-label')->for('home_land') }}

                                {{ html()->text('home_land')
                                    ->value(!empty($user->profile) ? $user->profile->home_land : null)
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.home_land'))
                                    ->attribute('maxlength', 191)}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.identity_card'))->class('form-control-label')->for('identity_card') }}

                                {{ html()->text('identity_card')
                                    ->value(!empty($user->profile) ? $user->profile->identity_card : null)
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.identity_card'))
                                    ->attribute('maxlength', 191)}}
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.rank'))->class('form-control-label')->for('rank') }}

                                {{ html()->select('rank',config('user_profile.rank'))
                                    ->value(!empty($user->profile) ? $user->profile->rank : null)
                                    ->class('form-control select2')
                                    ->placeholder(__('validation.attributes.backend.access.users.rank'))
                                    ->attribute('maxlength', 191)}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.time_start'))->class('form-control-label')->for('time_start') }}

                                {{ html()->text('time_start')
                                    ->value(!empty($user->profile) ? \Carbon\Carbon::parse($user->profile->time_start)->format('d/m/Y') : null)
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.backend.access.users.time_start'))
                                    ->attribute('maxlength', 191)}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.gender'))->class('form-control-label')->for('gender') }}

                                {{ html()->select('gender',config('user_profile.gender'))
                                    ->value(!empty($user->profile) ? $user->profile->gender : null)
                                    ->class('form-control select2')}}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.department'))->class('form-control-label')->for('department') }}

                                {{ html()->select('department_id',$departments)
                                    ->value(!empty($user->profile) ? $user->profile->department_id : null)
                                    ->class('select2 form-control ')}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.backend.access.users.description'))->class('form-control-label')->for('description') }}

                                {{ html()->textarea('description')
                                    ->value(!empty($user->profile) ? $user->profile->description : null)
                                    ->class('form-control')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <span class="card-title"><strong>Phân quyền</strong></span>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>@lang('labels.backend.access.users.table.roles')</th>
                            <th>@lang('labels.backend.access.users.table.permissions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                @if($roles->count())
                                    @foreach($roles as $role)
                                        <div class="card">
                                            <div class="card-header">
                                                <div class="checkbox d-flex align-items-center">
                                                    {{ html()->label(
                                                            html()->checkbox('roles[]', in_array($role->name, $userRoles), $role->name)
                                                                    ->class('switch-input')
                                                                    ->id('role-'.$role->id)
                                                            . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                        ->class('switch switch-label switch-pill switch-primary mr-2')
                                                        ->for('role-'.$role->id) }}
                                                    {{ html()->label(ucwords($role->name))->for('role-'.$role->id) }}
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                @if($role->id != 1)
                                                    @if($role->permissions->count())
                                                        @foreach($role->permissions as $permission)
                                                            <i class="fas fa-dot-circle"></i> {{ ucwords($permission->name) }}
                                                        @endforeach
                                                    @else
                                                        @lang('labels.general.none')
                                                    @endif
                                                @else
                                                    @lang('labels.backend.access.users.all_permissions')
                                                @endif
                                            </div>
                                        </div><!--card-->
                                    @endforeach
                                @endif
                            </td>
                            <td>
                                @if($permissions->count())
                                    @foreach($permissions as $permission)
                                        <div class="checkbox d-flex align-items-center">
                                            {{ html()->label(
                                                    html()->checkbox('permissions[]', in_array($permission->name, $userPermissions), $permission->name)
                                                            ->class('switch-input')
                                                            ->id('permission-'.$permission->id)
                                                        . '<span class="switch-slider" data-checked="on" data-unchecked="off"></span>')
                                                    ->class('switch switch-label switch-pill switch-primary mr-2')
                                                ->for('permission-'.$permission->id) }}
                                            {{ html()->label(ucwords($permission->name))->for('permission-'.$permission->id) }}
                                        </div>
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!--col-->
            </div><!--form-group-->
        </div>
    </div>

    <div class="card">
        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div>
    {{ html()->closeModelForm() }}
@endsection
