@extends('backend.layouts.app')

@section('title', app_name().' | '. __('labels.general.management') . ' ' . __('labels.general.blog'))

@section('content')
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#all" role="tab" aria-controls="home">
                <i class="icon-calculator"></i> @lang('labels.general.all')
                {{--<span class="badge badge-success">New</span>--}}
            </a>
        </li>
{{--        @if ($logged_in_user->isAdmin())--}}
{{--            <li class="nav-item">--}}
{{--                <a class="nav-link" data-toggle="tab" href="#history" role="tab" aria-controls="profile">--}}
{{--                    <i class="icon-basket-loaded"></i> @lang('labels.general.history')--}}
{{--                    --}}{{--<span class="badge badge-pill badge-danger">29</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        @endif--}}
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="all" role="tabpanel">
            <form method="get" action="{{route('admin.blog.index')}}" enctype="multipart/form-data">
                @include('backend.blog.includes.filter')
            </form>
            <div class="row mt-4">
                <div class="col-sm-6">
                    <h4 class="card-title mb-0">
                        @lang('labels.general.management') @lang('labels.general.blog')
                        <a href="{{ route('admin.blog.index',['type' => request()->get('type')] ) }}"
                           class="btn btn-{{empty(request()->get('trashed')) ? 'success' : '' }} btn-xs ml-1"
                           data-toggle="tooltip" title="">
                            <i class="fas fa-list"></i> @lang('labels.general.active')
                        </a>
                        <a href="{{ route('admin.blog.index',['type' => request()->get('type'),'trashed' => true] ) }}"
                           class="btn btn-{{request()->get('trashed') == true ? 'danger' : '' }} btn-xs ml-1"
                           data-toggle="tooltip" title="">
                            <i class="fas fa-trash-alt"></i> @lang('labels.general.deleted')
                        </a>
                    </h4>
                </div><!--col-->

                <div class="col-sm-6">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.add')">
                        <a href="{{ route('admin.blog.create',['type' => request()->get('type')]) }}"
                           class="btn btn-light btn-sm ml-1"
                           data-toggle="tooltip" title="@lang('labels.general.add')"><i class="fas fa-plus-circle"></i> @lang('labels.general.add')</a>
                    </div><!--btn-toolbar-->
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-2">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table table-hover nowap" id="blog-table">
                            <thead>
                            <tr>
                                <th style="width: 20px">ID</th>
                                <th>@lang('labels.general.avatar')</th>
                                <th>@lang('labels.general.title')</th>
                                <th style="width: 15%">@lang('labels.general.category')</th>
                                <th style="width: 15%">@lang('labels.general.highlight')</th>
                                <th style="width: 15%">@lang('labels.general.status')</th>
                                <th style="width: 15%">@lang('labels.general.created_at')</th>
                                <th style="width: 10%">@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->

        </div>
{{--        @if ($logged_in_user->isAdmin())--}}
{{--            <div class="tab-pane" id="history" role="tabpanel">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-12">--}}
{{--                        @include('backend.includes.history',['model_type' => App\Models\Blog\Blog::class])--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endif--}}
    </div>
@endsection
@push('after-scripts')
    <script type="text/javascript">
        var blog_table = $('#blog-table');
        $(function () {
            blog_table.dataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bAutoWidth: false,
                bLengthChange: false,
                searching: false,
                iDisplayLength: 20,
                bSortable: false,
                jQueryUI: true,
                select: {
                    style: 'multi'
                },
                ajax: {
                    url: '{{route('admin.blog.get')}}',
                    type: 'get',
                    data: {
                        query: '{{ base64_encode(urldecode(request()->getQueryString())) }}'
                    }
                },
                columns: [
                    {sortable: false, name: 'id', data: 'id'},
                    {sortable: false, name: 'avatar', data: 'avatar'},
                    {sortable: false, name: 'name', data: 'name'},
                    {sortable: false, name: 'categories', data: 'categories'},
                    {sortable: false, name: 'highlight', data: 'highlight'},
                    {sortable: false, name: 'active', data: 'active'},
                    {sortable: false, name: 'created_at', data: 'created_at'},
                    {sortable: false, name: 'actions', data: 'actions'},
                ],
                language: {
                    "lengthMenu": "Hiển thị _MENU_ bản ghi",
                    "zeroRecords": "Không tìm bản ghi phù hợp",
                    "infoEmpty": "Không có dữ liệu",
                    "infoFiltered": "(lọc từ tổng số _MAX_ bản ghi)",
                    "search": "Tìm kiếm:",
                    "searchPlaceholder": "Tìm kiếm",
                    "info": "Hiển thị từ _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "paginate": {
                        "first": "Đầu tiên",
                        "last": "Cuối cùng",
                        "next": "Sau",
                        "previous": "Trước"
                    },
                    "sProcessing": '<i class="fa fa-spinner fa-pulse fa-fw"></i> Đang lấy dữ liệu'
                },
                //order: [[0, "desc"]],
                searchDelay: 500,
            });

            $('.category_blog').select2({
                tags: false,
                language: {
                    errorLoading: function () {
                        return "Nhập tên danh mục..."
                    }
                },
                allowClear: true,
                multiple: true,
                height: 30,
                selectionTitleAttribute: false,
                ajax: {
                    url: '{{route('admin.category.get_category_blog')}}',
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 2000,
                    cache: true,
                    data: function (params) {
                        return {term: params.term}
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }

            });
        });

    </script>
@endpush
