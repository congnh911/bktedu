@extends('backend.layouts.app')

@section('title', app_name().' | Thêm mới sản phẩm')

@section('content')
    {{ html()->form('POST', route('admin.blog.store'))->id('frm-blog')->class('form-horizontal')->acceptsFiles()->open() }}

    <div class="row mb-3">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                Quản lý sản phẩm
                <small class="text-muted">Thêm mới</small>
            </h4>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col-lg-8">
            <div class="card card-accent-primary">
                <div class="card-header">
                    <strong class="card-title">Thông tin chung</strong>
                    <div class="card-header-actions">
                        <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                           data-target="#collapseGeneral" aria-expanded="true">
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseGeneral" style="">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-2 form-control-label">Tên sản phẩm <span
                                    class="text-danger">(*)</span></label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" maxlength="300" name="name" id="name"
                                       placeholder="Tên sản phẩm" autofocus value="{!! old('name') !!}">
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="link" class="col-md-2 form-control-label">Link</label>

                            <div class="col-md-10">
                                <input type="text" class="form-control" maxlength="305" name="slug" id="link"
                                       placeholder="Link. Ví dụ: ten-danh-muc.html" value="{!! old('link') !!}">
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="ckeditor" class="col-md-2 form-control-label">Mô tả</label>

                            <div class="col-md-10">
                                    <textarea name="description"
                                              class="editor form-control">{!! old('description') !!}</textarea>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="active" class="col-md-2 form-control-label">Hoạt động</label>

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" name="active" id="active"
                                           value="1" {!! old('active',1) == 1 ? 'checked' : '' !!}>
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                        <div class="form-group row">
                            <label for="active" class="col-md-2 form-control-label">Nổi bật</label>

                            <div class="col-md-10">
                                <label class="switch switch-label switch-pill switch-primary">
                                    <input class="switch-input" type="checkbox" name="highlight" id="highlight"
                                           value="1" {!! old('highlight',1) == 1 ? 'checked' : '' !!}>
                                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                </label>
                            </div><!--col-->
                        </div><!--form-group-->
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card card-accent-info">
                <div class="card-header">
                    <strong class="card-title">Thông tin sản phẩm</strong>
                    <div class="card-header-actions">
                        <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                           data-target="#collapseInfoProduct" aria-expanded="true">
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseInfoProduct" style="">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="active" class="form-control-label">Ảnh đại diện</label>
                            <input type="file" name="avatar">
                            {{--<div class="input-group">--}}
                            {{--<span class="input-group-btn">--}}
                            {{--<a data-input="thumbnail" data-preview="holder" class="btn btn-primary file_manager">--}}
                            {{--<i class="fa fa-picture-o"></i> Choose--}}
                            {{--</a>--}}
                            {{--</span>--}}
                            {{--<input id="thumbnail" class="form-control" type="text" name="avatar">--}}
                            {{--</div>--}}
                            {{--<img id="holder" style="margin-top:15px;max-height:100px;">--}}
                        </div>
                        <div class="form-group">
                            <label for="link" class="form-control-label">Danh mục</label>

                            <select class="select2 form-control" name="category_id[]"
                                    data-placeholder="{{trans('common.root')}}" multiple>
                                @foreach($categories as $key => $blog)
                                    <option value="{{$key}}">{{$blog}}</option>
                                @endforeach
                            </select>
                        </div><!--form-group-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <strong class="card-title">SEO</strong>
            <div class="card-header-actions">
                <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                   data-target="#collapseSeo" aria-expanded="true">
                    <i class="fas fa-chevron-up"></i>
                </a>
            </div>
        </div>
        <div class="collapse show" id="collapseSeo" style="">
            <div class="card-body">
                <div class="tab-pane" id="seo" role="tabpanel">
                    <div class="form-group row">
                        <label for="seo_title" class="col-md-2 form-control-label">SEO Title</label>

                        <div class="col-md-10">
                            <input type="text" class="form-control" maxlength="300" name="seo_title"
                                   id="seo_title" placeholder="Seo Title" value="{!! old('seo_title') !!}">
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="seo_keyword" class="col-md-2 form-control-label">SEO Keyword</label>

                        <div class="col-md-10">
                                    <textarea id="seo_keyword" name="seo_keyword" maxlength="500" class="form-control"
                                              placeholder="Từ khóa 1, Tu khoa 1, Tu khoa 2,...">{!! old('seo_keyword') !!}</textarea>
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        <label for="seo_description" class="col-md-2 form-control-label">SEO Description</label>

                        <div class="col-md-10">
                                    <textarea id="seo_description" name="seo_description" maxlength="1000"
                                              class="form-control"
                                              placeholder="Thông tin mô tả về sản phẩm...">{!! old('seo_description') !!}</textarea>
                        </div><!--col-->
                    </div><!--form-group-->
                </div>
            </div>
        </div>
    </div>

    <div class="row mb-3">
        <div class="col">
            {{ form_cancel(route('admin.blog.index',['type' => request()->get('type')]), 'Hủy') }}
        </div><!--col-->

        <div class="col text-right">
            {{ form_submit('Thêm mới') }}
        </div><!--col-->
    </div><!--row-->

    {{ html()->form()->close() }}
@endsection
