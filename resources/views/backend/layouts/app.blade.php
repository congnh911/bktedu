<!DOCTYPE html>
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'BKT Education')">
    <meta name="author" content="@yield('meta_author', 'CONA')">
    <link rel="shortcut icon" type="image/png" href="{{url('/img/bkt/logo.ico')}}"/>
    @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    {{ style(mix('css/backend.css')) }}

    @stack('after-styles')
</head>

<body class="{{ config('backend.body_classes') }}">
    @include('backend.includes.header')

    <div class="app-body">
        @include('backend.includes.sidebar')

        <main class="main">
            @include('includes.partials.demo')
            @include('includes.partials.logged-in-as')
            {!! Breadcrumbs::render() !!}

            <div class="container-fluid">
                <div class="animated fadeIn">
                    <div class="content-header">
                        @yield('page-header')
                    </div><!--content-header-->

                    @include('includes.partials.messages')
                    @yield('content')
                </div><!--animated-->
            </div><!--container-fluid-->
        </main><!--main-->

        @include('backend.includes.aside')
    </div><!--app-body-->

    @include('backend.includes.modal')
    @include('backend.includes.footer')
    {{--@include('backend.includes.photoswipe')--}}
    <div id="loading" style="display:none">
        <img src="{{url('img/loading.gif')}}" alt="Loading..."/>
        {{--<span>Chờ một chút ...</span>--}}
    </div>
    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/backend.js')) !!}

    {{--<script type="text/javascript" src="{{url('/plugin/tinymce/tinymce.min.js')}}"></script>--}}
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{url('/plugin/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{url('/plugin/bootbox/dist/bootbox.all.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/plugin/photoswipe/dist/photoswipe.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/plugin/photoswipe/dist/photoswipe-ui-default.min.js')}}"></script>
    <script src="{{url('/plugin/lockfixed/jquery.lockfixed.min.js')}}" type="text/javascript"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    @stack('after-scripts')
</body>
</html>
