@extends('backend.layouts.app')

@section('title', app_name().' | '.__('labels.general.add') . ' ' . __('labels.general.category'))

@section('content')
    {{ html()->form('POST', route('admin.category.store'))->id('frm-category')->class('form-horizontal')->acceptsFiles()->open() }}
    <input type="hidden" value="{{request()->get('type')}}" name="type">

    <div class="row mb-3">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                @lang('labels.general.management') {{strtolower(__('labels.general.department'))}}
                <small class="text-muted">@lang('labels.general.add')</small>
            </h4>
        </div><!--col-->
    </div><!--row-->
    <div class="row">
        <div class="col">
            <div class="card card-accent-primary">
                <div class="card-header">
                    <strong class="card-title">@lang('labels.general.general')</strong>
                    <div class="card-header-actions">
                        <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                           data-target="#collapseGeneral" aria-expanded="true">
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseGeneral" style="">
                    <div class="card-body">
                        <div class="tab-pane active" id="basic" role="tabpanel">
                            <div class="form-group row">
                                <label for="name" class="col-md-2 form-control-label">
                                    @lang('labels.general.name_category')
                                    <span class="text-danger">(*)</span></label>

                                <div class="col-md-10">
                                    <input type="text" class="form-control" maxlength="300" name="name" id="name"
                                           placeholder="{{__('labels.general.title')}}" autofocus
                                           value="{!! old('name') !!}">
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                <label for="link" class="col-md-2 form-control-label">Link</label>

                                <div class="col-md-10">
                                    <input type="text" class="form-control" maxlength="305" name="slug" id="link"
                                           placeholder="Link slug" value="{!! old('link') !!}">
                                </div><!--col-->
                            </div><!--form-group-->
                            <div class="form-group row">
                                <label for="link" class="col-md-2 form-control-label">Parent</label>

                                <div class="col-md-10">
                                    <select class="select2 form-control" name="parent_id"
                                            data-placeholder="{{trans('common.root')}}">
                                        @foreach($categories as $key => $category)
                                            <option value="{{$key}}">{{$category}}</option>
                                        @endforeach
                                    </select>
                                </div><!--col-->
                            </div><!--form-group-->
                            <div class="form-group row">
                                <label for="link"
                                       class="col-md-2 form-control-label">@lang('labels.general.avatar')</label>

                                <div class="col-md-10">

                                    <input type="file" name="avatar">
                                    {{--<div class="input-group">--}}
                                    {{--<span class="input-group-btn">--}}
                                    {{--<a data-input="thumbnail" data-preview="holder" class="btn btn-primary file_manager">--}}
                                    {{--<i class="fa fa-picture-o"></i> Choose--}}
                                    {{--</a>--}}
                                    {{--</span>--}}
                                    {{--<input id="thumbnail" class="form-control" type="text" name="avatar">--}}
                                    {{--</div>--}}
                                    {{--<img id="holder" style="margin-top:15px;max-height:100px;">--}}
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                <label for="ckeditor"
                                       class="col-md-2 form-control-label">@lang('labels.general.description')</label>

                                <div class="col-md-10">
                                    <textarea name="description" id="description"
                                              class="editor form-control">{!! old('description') !!}</textarea>
                                </div><!--col-->
                            </div><!--form-group-->

                            <div class="form-group row">
                                <label for="active"
                                       class="col-md-2 form-control-label">@lang('labels.general.active')</label>

                                <div class="col-md-10">
                                    <label class="switch switch-label switch-pill switch-primary">
                                        <input class="switch-input" type="checkbox" name="active" id="active"
                                               value="1" {!! old('active',1) == 1 ? 'checked' : '' !!}>
                                        <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                    </label>
                                </div><!--col-->
                            </div><!--form-group-->
                        </div>
                    </div>
                </div>
            </div>
            @if(config('category.type.department.id') != request()->get('type'))
                @include('backend.includes.seo')
            @endif
        </div><!--col-->

    </div><!--row-->

    <div class="row mb-3">
        <div class="col">
            {{ form_cancel(route('admin.category.index',['type' => request()->get('type')]), __('buttons.general.cancel')) }}
        </div><!--col-->

        <div class="col text-right">
            {{ form_submit(__('labels.general.add')) }}
        </div><!--col-->
    </div><!--row-->

    {{ html()->form()->close() }}
@endsection
