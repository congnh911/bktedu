@extends('backend.layouts.app')

@section('title', app_name().' | ' .__('labels.general.management') .' '.__('labels.general.category'))

@section('content')
    {{ html()->form('PATCH', route('admin.category.update',$data))->id('frm-category')->class('form-horizontal')->acceptsFiles()->open() }}
    <input type="hidden" name="_method" value="PATCH"/>
    <input type="hidden" value="{{request()->get('type')}}" name="type">
    <div class="row  mb-3">
        <div class="col-sm-5">
            <h4 class="card-title mb-0">
                @lang('labels.general.management') @lang('labels.general.department')
                <small class="text-muted">@lang('labels.general.update')</small>
            </h4>
        </div><!--col-->
    </div><!--row-->

    <div class="row">
        <div class="col">
            <div class="card card-accent-primary">
                <div class="card-header">
                    <strong class="card-title">@lang('labels.general.general')</strong>
                    <div class="card-header-actions">
                        <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
                           data-target="#collapseGeneral" aria-expanded="true">
                            <i class="fas fa-chevron-up"></i>
                        </a>
                    </div>
                </div>
                <div class="collapse show" id="collapseGeneral" style="">
                    <div class="card-body">
                        <div class="row tab-pane active" id="basic" role="tabpanel">
                            <div class="col-lg-9">
                                <div class="form-group row">
                                    <label for="name" class="col-md-2 form-control-label">@lang('labels.general.title')
                                        <span
                                                class="text-danger">(*)</span></label>

                                    <div class="col-md-10">
                                        <input type="text" class="form-control" maxlength="300" name="name" id="name"
                                               placeholder="{{__('labels.general.title')}}" required autofocus
                                               value="{!! $data->name !!}">
                                    </div><!--col-->
                                </div><!--form-group-->
                                <div class="form-group row">
                                    <label for="link" class="col-md-2 form-control-label">Link</label>

                                    <div class="col-md-10">
                                        <input type="text" class="form-control" maxlength="305" name="slug" id="link"
                                               placeholder=""
                                               value="{!! $data->slug !!}">
                                    </div><!--col-->
                                </div><!--form-group-->
                                <div class="form-group row">
                                    <label for="link" class="col-md-2 form-control-label">Parent</label>

                                    <div class="col-md-10">
                                        <select class="select2 form-control" name="parent_id"
                                                data-placeholder="{{trans('common.root')}}">
                                            @foreach($categories as $key => $item)
                                                @if($key != $data->id)
                                                    <option
                                                            value="{{$key}}" {!! $data->parent_id == $key ? 'selected' : '' !!}>{{$item}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div><!--col-->
                                </div><!--form-group-->
                                <div class="form-group row">
                                    <label for="ckeditor"
                                           class="col-md-2 form-control-label">@lang('labels.general.description')</label>

                                    <div class="col-md-10">
                                        <textarea name="description" id="description"
                                                  class="editor form-control">{!! old('description',$data->description) !!}</textarea>
                                    </div><!--col-->
                                </div><!--form-group-->
                                <div class="form-group row">
                                    <label for="active"
                                           class="col-md-2 form-control-label">@lang('labels.general.active')</label>

                                    <div class="col-md-10">
                                        <label class="switch switch-label switch-pill switch-primary">
                                            <input class="switch-input" type="checkbox" name="active" id="active"
                                                   value="1" {!! old('active',$data->active) == 1 ? 'checked' : '' !!}>
                                            <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                                        </label>
                                    </div><!--col-->
                                </div><!--form-group-->
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group row">
                                    <label for="link"
                                           class="form-control-label">@lang('labels.general.avatar')</label><br/>

                                    <input type="file" name="avatar">
                                    @if(!empty($data->path))
                                        <div class="image-thumnai image-avatar">
                                            <div title="{{__('labels.general.delete')}}" class="button-delete-image"><i
                                                        class="fas fa-times"></i></div>
                                            <img id="avatar" src="{!! $data->avatar() !!}">
                                        </div>
                                    @endif

                                </div><!--col-->
                                <div class="form-group row">
                                    <label>@lang('labels.general.creator') : </label>
                                    <span> {{$data->creator->name}}</span>
                                </div>
                                <div class="form-group row">
                                    <label>@lang('labels.general.created_at') : </label>
                                    <span> {{ Carbon\Carbon::parse($data->created_at)->format('d/m/Y H:i')}}</span>
                                </div>
                                <div class="form-group row">
                                    <label>@lang('labels.general.last_update') : </label>
                                    <span> {{ Carbon\Carbon::parse($data->updated_at)->format('d/m/Y H:i')}}</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            @if(config('category.type.department.id') != request()->get('type'))
                @include('backend.includes.album')

                @include('backend.includes.files')

                @include('backend.includes.seo')
            @endif
        </div><!--col-->

    </div><!--row-->
    <div class="row mb-3">
        <div class="col">
            {{ form_cancel(route('admin.category.index',['type' => request()->get('type')]), __('buttons.general.cancel')) }}
        </div><!--col-->

        <div class="col text-right">
            {{ form_submit(__('buttons.general.crud.update')) }}
        </div><!--col-->
    </div><!--row-->
    {{ html()->form()->close() }}
@endsection
