<div class="box">

    <form method="get" action="{{route('admin.category.index')}}" enctype="multipart/form-data">
        <input type="hidden" name="type" value="{{request()->get('type')}}">
        <input type="hidden" name="trashed" value="{{request()->get('trashed')}}">

        <div class="row">
            <div class="col-md-6 col-xs-12">
                <input type="text" class="form-control" value="{{request()->get('name')}}" name="name"
                       placeholder="{{__('labels.general.title')}}" autocomplete="off">
            </div>
            <div class="col-md-3 col-xs-12">
                <select name="parent" class="select2 form-control" data-placeholder="Parent">
                    <option value="">Parent</option>
                    @if(!empty($categories))
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-md-2 col-xs-12">
                <select name="active" class="select2 form-control" data-placeholder="{{__('labels.general.status')}}">
                    <option value="">@lang('labels.general.active')</option>
                    <option value="1" {{request('active') != null && request('active') == 1 ? 'selected' : ''}}>
                        @lang('labels.general.active')
                    </option>
                    <option value="0" {{request('active') != null && request('active') == 0 ? 'selected' : ''}}>
                        @lang('labels.general.inactive')
                    </option>
                </select>
            </div>
            <div class="col-1">
                <button type="submit" class="form-control btn btn-light">
                    <i class="fa fa-search"></i>
                </button>
            </div>
        </div>
    </form>
</div>
