<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link {{
                    active_class(Active::checkUriPattern('admin/dashboard'))
                }}" href="{{ route('admin.dashboard') }}">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    @lang('menus.backend.sidebar.dashboard')
                </a>
            </li>

            <li class="divider"></li>

            <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern(['admin/product*']) ||
                     Active::checkUriPattern('admin/category*') && request()->get('type') == config('category.type.product.id'), 'open')
                }}">
                <a class="nav-link nav-dropdown-toggle {{
                        active_class(Active::checkUriPattern('admin/product*') || Active::checkUriPattern(['admin/product*']) ||
                     Active::checkUriPattern('admin/category*') && request()->get('type') == config('category.type.product.id'))
                    }}" href="#">
                    <i class="nav-icon fas fa-coins"></i>
                    @lang('labels.general.management') @lang('labels.general.product')

                    {{--@if ($pending_approval > 0)--}}
                    {{--<span class="badge badge-danger">{{ $pending_approval }}</span>--}}
                    {{--@endif--}}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{
                                active_class(Active::checkRouteParam('type',config('category.type.product.id')) ||
                     Active::checkUriPattern('admin/category*') && request()->get('type') == config('category.type.product.id'))
                            }}"
                           href="{{ route('admin.category.index',['type' => config('category.type.product.id')]) }}">
                            @lang('labels.general.category')

                            {{--<span class="badge badge-danger">0</span>--}}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/product*'))
                            }}" href="{{ route('admin.product.index') }}">
                            @lang('labels.general.list')
                        </a>
                    </li>
                </ul>
            </li>


            <li class="divider"></li>


            <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern(['admin/blog*']) ||
                     Active::checkUriPattern('admin/category*') && request()->get('type') == config('category.type.blog.id'), 'open')
                }}">
                <a class="nav-link nav-dropdown-toggle {{
                        active_class(Active::checkUriPattern('admin/blog*') || Active::checkUriPattern(['admin/blog*']) ||
                     Active::checkUriPattern('admin/category*') && request()->get('type') == config('category.type.blog.id'))
                    }}" href="#">
                    <i class="nav-icon fas fa-blog"></i>
                    @lang('labels.general.management') @lang('labels.general.blog')

                    {{--@if ($pending_approval > 0)--}}
                    {{--<span class="badge badge-danger">{{ $pending_approval }}</span>--}}
                    {{--@endif--}}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link {{
                                active_class(Active::checkRouteParam('type',config('category.type.blog.id')) ||
                     Active::checkUriPattern('admin/category*') && request()->get('type') == config('category.type.blog.id'))
                            }}" href="{{ route('admin.category.index',['type' => config('category.type.blog.id')]) }}">
                            @lang('labels.general.category')

                            {{--<span class="badge badge-danger">0</span>--}}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/blog*'))
                            }}" href="{{ route('admin.blog.index') }}">
                            @lang('labels.general.list')
                        </a>
                    </li>
                </ul>
            </li>


            <li class="divider"></li>

            <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern(['admin/slide*']), 'open')
                }}">
                <a class="nav-link nav-dropdown-toggle {{
                        active_class(Active::checkUriPattern('admin/slide*'))
                    }}" href="#">
                    <i class="nav-icon fas fa-cogs"></i>
                    @lang('labels.general.setup')

                    {{--@if ($pending_approval > 0)--}}
                    {{--<span class="badge badge-danger">{{ $pending_approval }}</span>--}}
                    {{--@endif--}}
                </a>

                <ul class="nav-dropdown-items">
                    <li class="nav-item">
                        <a class="nav-link " href="{{ route('admin.slide.index') }}">
                            Quản lý Slide
                        </a>
                    </li>
                </ul>
            </li>


            <li class="divider"></li>

            <li class="nav-title">
                @lang('menus.backend.sidebar.system')
            </li>

            @if ($logged_in_user->isAdmin())
                <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern('admin/auth*') ||
                     Active::checkUriPattern('admin/category*') && request()->get('type') == config('category.type.department.id') && request()->get('type') == config('category.type.department.id'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                        active_class(Active::checkUriPattern('admin/auth*'))
                    }}" href="#">
                        <i class="nav-icon far fa-user"></i>
                        @lang('menus.backend.access.title')

                        @if ($pending_approval > 0)
                            <span class="badge badge-danger">{{ $pending_approval }}</span>
                        @endif
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/auth/user*'))
                            }}" href="{{ route('admin.auth.user.index') }}">
                                @lang('labels.backend.access.users.management')

                                @if ($pending_approval > 0)
                                    <span class="badge badge-danger">{{ $pending_approval }}</span>
                                @endif
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/auth/role*'))
                            }}" href="{{ route('admin.auth.role.index') }}">
                                @lang('labels.backend.access.roles.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Active::checkUriPattern('admin/auth/permission*'))
                            }}" href="{{ route('admin.auth.permission.index') }}">
                                @lang('labels.backend.access.permissions.management')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                                active_class(Active::checkRouteParam('type',config('category.type.department.id')) ||
                     Active::checkUriPattern('admin/category*') && request()->get('type') == config('category.type.department.id'))
                            }}"
                               href="{{ route('admin.category.index',['type' => config('category.type.department.id')]) }}">
                                @lang('labels.general.management')
                                @lang('labels.general.department')

                                {{--<span class="badge badge-danger">0</span>--}}
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>

                <li class="nav-item nav-dropdown {{
                    active_class(Active::checkUriPattern('admin/log-viewer*'), 'open')
                }}">
                    <a class="nav-link nav-dropdown-toggle {{
                            active_class(Active::checkUriPattern('admin/log-viewer*'))
                        }}" href="#">
                        <i class="nav-icon fas fa-list"></i> @lang('menus.backend.log-viewer.main')
                    </a>

                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Active::checkUriPattern('admin/log-viewer'))
                        }}" href="{{ route('log-viewer::dashboard') }}">
                                @lang('menus.backend.log-viewer.dashboard')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{
                            active_class(Active::checkUriPattern('admin/log-viewer/logs*'))
                        }}" href="{{ route('log-viewer::logs.list') }}">
                                @lang('menus.backend.log-viewer.logs')
                            </a>
                        </li>
                    </ul>
                </li>
            @endif
        </ul>
    </nav>

    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div><!--sidebar-->
