@if($column == 'created_at')
    {!! \Carbon\Carbon::parse($item->created_at)->format('H:i d/m/Y') !!}
@endif
@if($column == 'avatar')
    <div class="avatar-table">
        <img src="{{$item->avatar()}}" alt="{{$item->name}}" class="zoom lg">
    </div>
@endif
@if($column == 'name')
    @if(!empty($item->link()))
        <a href="{{$item->link()}}" target="_blank" title="{{$item->name}}">{{$item->name}}</a>
    @else
        {{$item->name}}
    @endif
@endif
@if($column == 'gallery')
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @if(!empty($item->albums))
                @foreach($item->albums as $key => $value)
                    <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                        <img class="d-block w-100" src="{{$value->gallery()}}" alt="">
                    </div>
                @endforeach
            @endif
        </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

@endif
@if($column == 'actions')
    {!! $item->action_buttons !!}
@endif
@if($column == 'active')
    {!! $item->switch_active !!}
@endif
@if($column == 'highlight')
    {!! $item->switch_highlight !!}
@endif
@if($column == 'parent')
    <div class="">
        @if(!empty($item->ancestorsCategories()))
            @foreach($item->ancestorsCategories() as $key => $value)
                <a href="{{route('admin.category.show', $value->id) }}" data-toggle="modal" data-target="#modal-md"
                   data-toggle="tooltip" data-placement="top"
                   title="{{$value->name}}"
                   class="btn btn-xs btn-light modal_action">{{$value->name}}</a>
            @endforeach
        @endif
    </div>
@endif

@if($column == 'categories')
    <div class="">
        @if(!empty($item->blogCategories))
            @foreach($item->blogCategories as $key => $value)
                <a href="{{route('admin.category.show', $value->id) }}" data-toggle="modal" data-target="#modal-md"
                   data-toggle="tooltip" data-placement="top"
                   title="{{$value->name}}"
                   class="btn btn-xs btn-light modal_action">{{$value->name}}</a>
            @endforeach
        @endif
    </div>
@endif


@if(!empty($module) && $module == 'audit')
    @if($column == 'event')
        <label class="badge badge-{{config('audit.field_events.'.$item->event.'.status')}}">
            <i class="{{config('audit.field_events.'.$item->event.'.icon')}}"></i>
            {{config('audit.field_events.'.$item->event.'.name')}}
        </label>
    @endif
@endif

@if($column == 'category_name')
    {{$category_name}}
@endif
@if($column == 'position')
    {!! config('slide.position.'.$item->position.'.name')!!}
@endif
