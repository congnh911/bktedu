{{ html()->form('patch',route('admin.content.update',$data))->class('form-horizontal')->id('frm-content')
        ->attribute('onsubmit','return Content.update(this);')
        ->acceptsFiles()->open() }}
<input type="hidden" name="model_id" value="{{$data->model_id}}">
<input type="hidden" name="model_type" value="{{$data->model_type}}">
<div class="modal-header">
    <h4 class="modal-title">@lang('labels.general.update') @lang('labels.general.content')</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <div class="form-group">
        <label>@lang('labels.general.type')</label>
        <select class="select2 form-control pull-right" style="width:100%"
                name="content_type" data-placeholder="{{__('labels.general.type')}}" onchange="Content.nameAutocomplete(this)">
            <option value="">@lang('labels.general.type')</option>
            @if(!empty(config('blog.content_type')))
                @foreach(config('blog.content_type') as $key => $item)
                    <option value="{{$item['id']}}" {{$data->type == $item['id'] ? 'selected' : ''}}>{{$item['name']}}</option>
                @endforeach
            @endif
        </select>

    </div>
    <div class="form-group">
        <label>@lang('labels.general.title')</label>
        <input class="form-control" name="content_name" value="{{$data->name}}" placeholder="{{__('labels.general.title')}}">
    </div>
    <div class="form-group">
        <label>@lang('labels.general.content')</label>
        <textarea class="editor form-control" name="content_summary" id="content_summary">{!! $data->content !!}</textarea>
    </div>
</div>
<div class="modal-footer">
    {{ form_submit('Lưu') }}
</div>
{{html()->form()->close()}}

<script>

    $(function () {
        window.data = function(){
            console.log(1234);
        }
        $('.select2').select2({tag:true});

        CKEDITOR.replace('content_summary',{
            language: 'vi'
        });
    })
</script>
