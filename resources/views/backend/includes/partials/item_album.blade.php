@if(!empty($albums) && count($albums) > 0)
    @foreach($albums as $key => $album)
        <div class="col-md-12 image-item ui-state-default" data-id="{{$album->id}}">
            <div class="location-listing row">
                <div class="col-md-4">

                    <div class="location-image">
                        <a href="{{$album->avatar()}}" itemprop="contentUrl" data-size="1024x768" target="_blank">
                            <img src="{{$album->avatar()}}" itemprop="thumbnail"
                                 alt="{{$album->name}}" style="width: 100%;"> </a>
                    </div>
                </div>
                <div class="col-md-8">
                    <input type="text" name="image_name" value="{{$album->name}}" data-id="{{$album->id}}"
                           data-url="{{route('admin.album.update',$album)}}" class="form-control mb-1"
                           onchange="Album.update(this)"
                           placeholder="Tiêu đề">
                    <input type="text" name="image_link" value="{{$album->link}}" data-id="{{$album->id}}"
                           data-url="{{route('admin.album.update',$album)}}" class="form-control mb-1"
                           onchange="Album.update(this)"
                           placeholder="Url">
                    <textarea name="image_description" data-url="{{route('admin.album.update',$album)}}" class="form-control"
                              onchange="Album.update(this)" placeholder="Mô tả">{{$album->description}}</textarea>

                    @if(!in_array($data->getMorphClass(),[\App\Models\Slide::class]))
                    <label class="container-radio mt-2" for="avatar_{{$album->id}}">
                        <input type="radio" name="is_avatar"
                               data-url="{{route('admin.album.is_avatar')}}"
                               data-model_type="{{$data->getMorphClass()}}" data-model_id="{{$data->id}}"
                               onchange="Album.isAvatar(this)" {{$album->avatar == 1 ? 'checked' : null}}
                               value="{{$album->id}}" id="avatar_{{$album->id}}">
                        <span class="checkmark"></span> <span style="font-size: 14px !important;" >is Avatar</span>
                    </label>
                    @endif
                    <button type="button" onclick="Album.delete(this)" class="btn btn-outline-danger btn-xs float-right mt-2"
                            data-trans-button-cancel="{{ trans('buttons.general.cancel')}}"
                            data-trans-button-confirm="{{ trans('buttons.general.yes') }}"
                            data-trans-title="{{trans('strings.backend.general.are_you_sure') }}"
                            data-url="{{route('admin.album.delete',$album)}}">
                        <i class="fas fa-trash-alt"></i> Xóa ảnh</button>
                </div>
            </div>
            <hr/>
        </div>
    @endforeach
@else

    <p class="pl-3">Không có ảnh nào</p>
@endif
@push('after-scripts')
    <script>
        $( function() {
            $( ".album-listing" ).sortable({
                update:function (event,ui) {
                    var listSort = $(this).find('.image-item');
                    var dataSort = [];
                    listSort.each(function (key,item) {
                        var dataJson = $(item).data('id');
                        dataSort.push(dataJson)
                    });
                    ajaxOrderAlbums(dataSort);
                }
            });
        } );

        function ajaxOrderAlbums(data) {
            var formData = new FormData();
            formData.append('data',data);
            axios({
                method: 'post',
                url: '{{route('admin.album.sortable')}}',
                headers: {
                    'content-type': 'multipart/form-data;',
                },
                data: formData
            })
                .then(function (response) {
                    if (response.data.success) {
                        $.notify({message:response.data.message},{type:'success'});
                    } else {
                        $.notify({message:response.data.message},{type:'danger'});
                        return false;
                    }

                });
        }
    </script>
@endpush
