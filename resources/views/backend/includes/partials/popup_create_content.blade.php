{{ html()->form('post',route('admin.content.store'))->class('form-horizontal')->id('frm-content')
        ->attribute('onsubmit','return Content.store(this);')
        ->acceptsFiles()->open() }}
<input type="hidden" name="model_id" value="{{$model_id}}">
<input type="hidden" name="model_type" value="{{$model_type}}">
<div class="modal-header">
    <h4 class="modal-title">Thêm mới nội dung</h4>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
</div>
<div class="modal-body">
    <div class="form-group">
        <label>Kiểu nội dung</label>
        <select class="select2 form-control pull-right" style="width:100%"
                name="content_type" data-placeholder="Kiểu nội dung" onchange="Content.nameAutocomplete(this)">
            <option value="">Kiểu nội dung</option>
            @if(!empty(config('blog.content_type')))
                @foreach(config('blog.content_type') as $key => $item)
                    <option value="{{$item['id']}}">{{$item['name']}}</option>
                @endforeach
            @endif
        </select>

    </div>
    <div class="form-group">
        <label>Tiêu đề</label>
        <input class="form-control" name="content_name" value="" placeholder="Tiêu đề nội dung ">
    </div>
    <div class="form-group">
        <label>Nội dung</label>
        <textarea class="editor form-control" name="content_summary" id="content_summary"></textarea>
    </div>
</div>
<div class="modal-footer">
    {{ form_submit('Lưu') }}
</div>
{{html()->form()->close()}}

<script>

    $(function () {
        window.data = function(){
            console.log(1234);
        }
        $('.select2').select2({tag:true});
        CKEDITOR.replace('content_summary');
    })
</script>
