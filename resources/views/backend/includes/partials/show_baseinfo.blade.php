<div class="form-group">
    <label class="text-muted">@lang('labels.general.creator') : </label>
    <a href="{!! $data->creator->link() !!}" title="{{$data->creator->name}}">{!! $data->creator->name !!}</a><br/>
    <label class="text-muted">@lang('labels.general.created_at') : </label>
    {!! \Carbon\Carbon::parse($data->created_at)->format('H:i d/m/Y') !!}<br/>
    <label class="text-muted">@lang('labels.general.last_update') : </label>
    {!! \Carbon\Carbon::parse($data->updated_at)->format('H:i d/m/Y') !!}<br/>
    @if(!empty($data->deleted_at))
    <label class="text-muted">@lang('labels.general.deleted_at') : </label>
    <span class="text-danger">{!! \Carbon\Carbon::parse($data->deleted_at)->format('h:i d/m/Y A') !!}<br/></span>
    @endif
</div>
