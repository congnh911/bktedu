<ol class="file-attachment video-attachment list-group ml-10 row">
    @if(!empty($videos) && count($videos) > 0)
        @foreach($videos as $video)
            <li class="col-md-6">
                <video poster="" id="player" playsinline controls style="width: 100%">
                    <source src="{{ route('admin.video.view',['type' => $video->type, 'id' => $video->id]) }}" type="video/mp4" />
                    <source src="{{ route('admin.video.view',['type' => $video->type, 'id' => $video->id]) }}" type="video/webm" />

                    <!-- Captions are optional -->
                    <track kind="captions" label="English captions" src="/path/to/captions.vtt" srclang="en" default />
                </video>

                <div class="form-group mt-1">
                    <input type="text" name="video_title" value="{{$video->name}}" data-id="{{$video->id}}"
                           data-url="{{route('admin.video.update',$video)}}" class="form-control" data-field="name"
                           onchange="Video.update(this)"
                           placeholder="Tiêu đề">
                    <textarea type="text" name="video_description" data-id="{{$video->id}}"
                           data-url="{{route('admin.video.update',$video)}}" data-field="description" class="form-control"
                           onchange="Video.update(this)"
                           placeholder="Mô tả">{{$video->description}}</textarea>
                </div>
                <div class="video-icon-type text-grey font18"></div>
                <a target="_blank"
                   href="{{ route('admin.video.view',['type' => $video->type, 'id' => $video->id]) }}"
                   data-toggle="tooltip" title="{{ $video->original_name }}"><i
                        class="fas fa-paperclip"></i> {{ $video->original_name }}
                </a><br/>
                <small class="text-gray">Size
                    : {{ number_format($video->original_size) }} KB
                </small>
                <div class="icon-download">
                    <a href="{{ route('admin.video.download',$video) }}"
                       class="btn btn-xs btn-light" data-toggle="tooltip"
                       title="Download File"><i
                            class="fas fa-download"></i> Tải xuống</a>

                    <a href="javascript:void(0)"
                       onclick="Video.delete(this)"
                       data-url="{{ route('admin.video.delete', $video) }}"
                       data-trans-button-cancel="{{__('buttons.general.cancel')}}"
                       data-trans-button-confirm="{{__('buttons.general.crud.delete')}}"
                       data-trans-title="{{__('strings.backend.general.are_you_sure')}}"
                       class="btn btn-xs btn-danger"><i
                            class="fas fa-trash-alt"></i> {{__('buttons.general.crud.delete')}}</a>
                </div>
            </li>
        @endforeach
    @else

        <p>Không có tệp đính kèm</p>
    @endif

</ol>
