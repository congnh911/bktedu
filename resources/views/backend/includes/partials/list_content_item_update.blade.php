<div class="d-flex w-100 justify-content-between">
    <h5 class="mb-1 content_name">{{$data->name}}</h5>
    <small>Cập nhật {{\Carbon\Carbon::parse($data->updated_at)->diffForHumans()}}</small>
</div>
<p class="mb-1 text-truncate content_summary" style="max-height: 50px">
    {!! strip_tags(substr($data->content,0,400)) !!}
</p>
<small>Tạo bởi : {{$data->creator->name}} | Lúc : {{\Carbon\Carbon::parse($data->created_at)->format('H:i d/m/Y')}} </small>
<a href="{{route('admin.content.popup_edit',$data)}}"
   class="btn btn-secondary btn-xs modal_action" data-backdrop="static" data-toggle="modal" data-target="#modal-lg">
    <i class="fas fa-edit"></i> Cập nhật</a>
<a href="javascript:void(0)" data-url="{{route('admin.content.delete',$data)}}"
   class="btn btn-danger btn-xs" onclick="Content.delete(this)"
   data-trans-button-cancel="{{trans('buttons.general.cancel') }}"
   data-trans-button-confirm="{{trans('buttons.general.crud.delete') }}"
   data-trans-title="{{ trans('strings.backend.general.are_you_sure') }}">
    <i class="fas fa-trash"></i> Xóa</a>