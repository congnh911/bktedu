<ol class="file-attachment list-group ml-10">
    @if(!empty($files) && count($files) > 0)
        @foreach($files as $file)
            <li>
                <div class="file-icon-type text-grey font18"></div>
                <a target="_blank"
                   href="{{ route('admin.file.view',['type' => $file->type, 'id' => $file->id]) }}"
                   data-toggle="tooltip" title="{{ $file->original_name }}"><i
                        class="fas fa-paperclip"></i> {{ $file->original_name }}
                </a><br/>
                <small class="text-gray">Size
                    : {{ number_format($file->original_size) }} KB
                </small>
                <div class="icon-download">
                    <a href="{{ route('admin.file.download',$file) }}"
                       class="btn btn-xs btn-light" data-toggle="tooltip"
                       title="Download File"><i
                            class="fas fa-download"></i> Tải xuống</a>

                    <a href="javascript:void(0)"
                       onclick="File.delete(this)"
                       data-url="{{ route('admin.file.delete', $file) }}"
                       data-trans-button-cancel="{{__('buttons.general.cancel')}}"
                       data-trans-button-confirm="{{__('buttons.general.crud.delete')}}"
                       data-trans-title="{{__('strings.backend.general.are_you_sure')}}"
                       class="btn btn-xs btn-danger"><i
                            class="fas fa-trash-alt"></i> {{__('buttons.general.crud.delete')}}</a>
                </div>
            </li>
        @endforeach
    @else

        <p>Không có tệp đính kèm</p>
    @endif

</ol>
