<div class="list-group" id="list-content">
    @if(!empty($data->contents))
        @foreach($data->contents()->orderBy('type','asc')->get() as $key => $item)
            @include('backend.includes.partials.list_content_item',['data' => $item])
        @endforeach
    @endif
</div>
