<div class="card">
    <div class="card-header">
        <strong class="card-title">@lang('labels.general.content')</strong>
        <div class="card-header-actions">
            <a href="{{route('admin.content.popup_create',['model_id' => $data->id,'model_type' => $data->getMorphClass()])}}" class="btn btn-secondary btn-xs modal_action" data-backdrop="static" data-toggle="modal" data-target="#modal-lg"
                   style="margin-bottom: 0;margin-right: 10px; !important;" >
                <i class="fas fa-plus"></i> @lang('labels.general.add')
            </a>
            <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
               data-target="#collapseContent" aria-expanded="true">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="collapse {{ !empty($data->contents) && count($data->contents) > 0 ? 'show' : ''}}" id="collapseContent"
         style="">
        <div class="card-body">
            @include('backend.includes.list_content')
        </div>
    </div>
</div>
@push('after-scripts')
    <script type="text/javascript" src="{{url('/js/backend/content.js')}}"></script>
@endpush
