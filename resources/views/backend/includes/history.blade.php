<table class="table" id="history-table">
    <thead class="">
    <tr>
        <th scope="col">Tài khoản</th>
        <th scope="col">Hành động</th>
        <th scope="col">Thời gian</th>
        <th scope="col">Item_id</th>
        <th scope="col">Data cũ</th>
        <th scope="col">Data mới</th>
    </tr>
    </thead>
    <tbody id="audits">
    </tbody>
</table>
@push('after-scripts')
    <script>
        var history_table = $('#history-table');
        $(function () {
            history_table.dataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                bAutoWidth: false,
                bLengthChange: false,
                searching: false,
                iDisplayLength: 15,
                bSortable: true,
                jQueryUI: true,
                ajax: {
                    url: '{{route('admin.audit.get')}}',
                    type: 'get',
                    data: {
                        query: '{{ base64_encode(urldecode(request()->getQueryString()
                        .'&auditable_type='.$model_type)) }}'
                    }
                },
                columns: [
                    {sortable: false, name: 'user', data: 'user'},
                    {sortable: false, name: 'event', data: 'event'},
                    {sortable: false, name: 'created_at', data: 'created_at'},
                    {sortable: false, name: 'auditable_id', data: 'auditable_id'},
                    {sortable: false, name: 'data_old', data: 'data_old'},
                    {sortable: false, name: 'data_new', data: 'data_new'}
                ],
                language: {
                    "lengthMenu": "Hiển thị _MENU_ bản ghi",
                    "zeroRecords": "Không tìm bản ghi phù hợp",
                    "infoEmpty": "Không có dữ liệu",
                    "infoFiltered": "(lọc từ tổng số _MAX_ bản ghi)",
                    "search": "Tìm kiếm:",
                    "searchPlaceholder": "Tìm kiếm",
                    "info": "Hiển thị từ _START_ đến _END_ trong tổng số _TOTAL_ kết quả",
                    "paginate": {
                        "first": "Đầu tiên",
                        "last": "Cuối cùng",
                        "next": "Sau",
                        "previous": "Trước"
                    },
                    "sProcessing": '<i class="fa fa-spinner fa-pulse fa-fw"></i> Đang lấy dữ liệu'
                },
                //order: [[0, "desc"]],
                searchDelay: 500,
            });

        });
    </script>
@endpush
