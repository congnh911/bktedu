<div class="card">
    <div class="card-header">
        <strong class="card-title">Video</strong>
        <div class="card-header-actions">
            <label for="input-video" class="btn btn-secondary btn-xs"
                   style="margin-bottom: 0;margin-right: 10px; !important;">
                <i class="fas fa-plus"></i> @lang('labels.general.add')
            </label>
            <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
               data-target="#collapseVideo" aria-expanded="true">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="collapse {{!empty($data->videos) && count($data->videos) ? 'show' : ''}}" id="collapseVideo" style="">
        <div class="card-body">

            @include('backend.includes.partials.item_video',['videos' => $data->videos])

            <input type="file" id="input-video" name="videos" style="visibility: hidden !important;"
                   onchange="Video.store(this)"
                   data-model-type="{{$data->getMorphClass()}}" data-model-id="{{$data->id}}"
                   data-action="{{route('admin.video.upload')}}" multiple>
        </div>
    </div>
</div>
@push('after-scripts')
    <script type="text/javascript" src="{{url('/js/backend/videos.js')}}"></script>
@endpush
