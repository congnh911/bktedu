<div class="card">
    <div class="card-header">
        <strong class="card-title">@lang('labels.general.file-attach')</strong>
        <div class="card-header-actions">
            <label for="input-file" class="btn btn-secondary btn-xs"
                   style="margin-bottom: 0;margin-right: 10px; !important;">
                <i class="fas fa-plus"></i> @lang('labels.general.add')
            </label>
            <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
               data-target="#collapseFile" aria-expanded="true">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="collapse {{!empty($data->files) && count($data->files) ? 'show' : ''}}" id="collapseFile" style="">
        <div class="card-body">

            @include('backend.includes.partials.item_file',['files' => $data->files])

            <input type="file" id="input-file" name="files" style="visibility: hidden !important;"
                   onchange="File.store(this)"
                   data-model-type="{{$data->getMorphClass()}}" data-model-id="{{$data->id}}"
                   data-action="{{route('admin.file.upload')}}" multiple>
        </div>
    </div>
</div>
@push('after-scripts')
    <script type="text/javascript" src="{{url('/js/backend/files.js')}}"></script>
@endpush
