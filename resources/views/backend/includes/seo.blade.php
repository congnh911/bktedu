<div class="card">
    <div class="card-header">
        <strong class="card-title">SEO</strong>
        <div class="card-header-actions">
            <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
               data-target="#collapseSeo" aria-expanded="true">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="collapse show" id="collapseSeo" style="">
        <div class="card-body">
            <div class="tab-pane" id="seo" role="tabpanel">
                <div class="form-group row">
                    <label for="seo_title" class="col-md-2 form-control-label">SEO @lang('labels.general.title')</label>

                    <div class="col-md-10">
                        <input type="text" class="form-control" maxlength="300" name="seo_title"
                               id="seo_title" placeholder="Seo {{__('labels.general.title')}}"
                               value="{!! old('seo_title',!empty($data->seo_title) ? $data->seo_title : '') !!}">
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    <label for="seo_keyword" class="col-md-2 form-control-label">SEO @lang('labels.general.keyword')</label>

                    <div class="col-md-10">
                                    <textarea id="seo_keyword" name="seo_keyword" maxlength="500" class="form-control"
                                              placeholder="keyword 1, keyword 2, keyword 3,...">{!! old('seo_keyword',!empty($data->seo_keyword) ? $data->seo_keyword : '') !!}</textarea>
                    </div><!--col-->
                </div><!--form-group-->

                <div class="form-group row">
                    <label for="seo_description" class="col-md-2 form-control-label">SEO @lang('labels.general.description')</label>

                    <div class="col-md-10">
                                    <textarea id="seo_description" name="seo_description" maxlength="1000"
                                              class="form-control"
                                              placeholder="SEO {{__('labels.general.description')}}...">{!! old('seo_description',!empty($data->seo_description) ? $data->seo_description : '') !!}</textarea>
                    </div><!--col-->
                </div><!--form-group-->
            </div>
        </div>
    </div>
</div>
