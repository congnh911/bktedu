<div class="card">
    <div class="card-header">
        <strong class="card-title">@lang('labels.general.album')</strong>
        <div class="card-header-actions">
            <label for="input-album" class="btn btn-secondary btn-xs"
                   style="margin-bottom: 0;margin-right: 10px; !important;">
                <i class="fas fa-plus"></i> @lang('labels.general.add')
            </label>
            <a class="card-header-action btn-minimize" href="#" data-toggle="collapse"
               data-target="#collapseAlbum" aria-expanded="true">
                <i class="fas fa-chevron-up"></i>
            </a>
        </div>
    </div>
    <div class="collapse {{ !empty($data->albums) && count($data->albums) > 0 ? 'show' : ''}}" id="collapseAlbum" style="">
        <div class="card-body">
            <div class="album-listing row">
                @include('backend.includes.partials.item_album',['albums' => $data->albums])
            </div>
            <input type="file" id="input-album" name="images" style="visibility: hidden !important;"
                   onchange="Album.store(this)"
                   data-model_type="{{$data->getMorphClass()}}" data-model_id="{{$data->id}}"
                   data-action="{{route('admin.album.store')}}" multiple>

        </div>
    </div>
</div>

@push('after-scripts')
    <script type="text/javascript" src="{{url('/js/backend/album.js')}}"></script>
@endpush
