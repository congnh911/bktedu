@if(!empty($notification->data['data']) && !empty($notification->data['user']))
    <div class="notification-item">
        <a href="{{$notification->data['data']['link']}}" title="" id="noti{{$notification->id}}"
           onclick="markAsRead('{{$notification->id}}')"
           class="notify-item {{$notification->read_at == null ? 'unread' : null}}">
            <div class="avatar pull-left">
                <img src="{{$notification->data['user']['avatar']}}" class="noti-avatar"/>
            </div>
            <div class="mt-2">
                <strong>{{$notification->data['user']['name']}}</strong>
                <span class="fs12"></span> <span>{{$notification->data['data']['action']}}</span>
                <strong>{{$notification->data['data']['message']}}</strong><br/>
                <i class="fa fa-clock"></i>
                <span
                    class="fs12 text-grey">{{\Carbon\Carbon::parse($notification->created_at)->diffForHumans()}}</span>
            </div>


        </a>
        <span class="btn-read" data-toggle="tooltip"
              data-title="Đánh dấu đã đọc" onclick="markAsRead(this)"
              data-id="{{$notification->id}}"
              data-url="{{route('frontend.v1.notification.mark_as_read',$notification->id)}}">
            <i class="fa fa-dot-circle"></i></span>
    </div>
@endif
