@extends('frontend.v1.layouts.app')

@section('title', app_name() . ' | Thông báo' )

@section('content')
    <div class="row">
        <div class="col-2 no-padding no-margin">
            @include('frontend.v1.includes.sidebar')
        </div><!--col-->
        <div class="col-md-7">
            <notification-component
                total-notification-unread="{{$total_unread_notification}}"
                route-get-notifications="{{route('frontend.v1.notification.get')}}"
                route-mark-as-read-all="{{route('frontend.v1.notification.mark_as_read_all')}}"
                route-mark-as-read="{{route('frontend.v1.notification.mark_as_read')}}">
            </notification-component>
        </div>
        <div class="col-md-3"></div>
    </div>
@endsection

