@extends('frontend.v1.layouts.app')

@section('title', 'Profile ' .auth()->user()->fullName())

@section('content')
    <div class="row">
        <div class="col-md-2 hidden-md p-0 m-0">
            @include('frontend.v1.includes.sidebar')
        </div><!--col-->
        <div class="col-md-7">
            <div class="box-card">
                <div class="profile-box">
                    <div class="prof-header">
                        <a href="#">
                            <img src="{{url('/')}}/img/cover1.jpeg" alt="" class="img_cover"/>
                        </a>
                        <a href="#" class="avatar-box">
                            <img src="{{auth()->user()->avatar()}}" alt="" class="img_avatar"/>
                        </a>
                        <div class="pull-right p-1">
                            <a href="{{route('frontend.v1.user.popup_edit_account')}}"
                               class="btn btn-light pull-right btn-sm modal_action"
                               data-toggle="modal" data-target="#modal-md">
                                <i class="fa fa-edit"></i> Chỉnh sửa hồ sơ</a>
                        </div>
                        <div class="info-detail pt-1 pl-4 mt-4 overflow-hidden">
                            <strong class="fs20 full-name">{{auth()->user()->name}}</strong>
                            <p>{{auth()->user()->note}}</p>
                        </div>
                    </div>
                    <div class="p-4"></div>
                    <div class="box-information mt-5">
                        <div class="my-content">
                            <div class="group-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs nav-pills  nav-justified" id="tab-profile" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#newsfeed" aria-controls="home" role="tab" class="active"
                                           data-toggle="tab">
                                            <i class="fa fa-newspaper"></i> Tin đã đăng</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#blog-list" aria-controls="profile" role="tab"
                                           data-toggle="tab"> <i class="fa fa-blog"></i> Bài viết đã đăng</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#friends" aria-controls="messages" role="tab" data-toggle="tab">
                                            <i class="fa fa-users"></i> Bạn bè</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content p-3">
                                    <div role="tabpanel" class="tab-pane active" id="newsfeed">This is Newsfeed</div>
                                    <div role="tabpanel" class="tab-pane" id="blog-list">
                                        <list-blog-component
                                            route-list-blog="{{route('frontend.v1.blog.get_data',['user_id' => auth()->user()->id])}}"></list-blog-component>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="friends">This is Friends</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 hidden-xs p-0">
            <div class="box-card p-5"></div>
        </div>
    </div>
@endsection

@push('after-scripts')
    @if(config('access.captcha.registration'))
        @captchaScripts
    @endif
@endpush
