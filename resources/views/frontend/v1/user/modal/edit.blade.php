<form method="patch" action="{{route('frontend.v1.user.profile.update')}}" enctype="multipart/form-data" id="update-account">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="modal-header p-2 text-center">
        <span class="modal-title fs18 text-center text-bold" id="myModalLabel2">Chỉnh sửa hồ sơ</span>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
    </div>

    <div class="modal-body">
        <div class="row">
            <div class="col-6">
                <div class="form-group">
                    <label for="first_name" class="fs13 mb-0 text-bold">Tên</label>
                    <input class="form-control" name="first_name" type="text" value="{{$user->first_name}}">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="last_name" class="fs13 mb-0 text-bold">Họ</label>
                    <input class="form-control" name="last_name" type="text" value="{{$user->last_name}}">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="fist_name" class="fs13 mb-0 text-bold">Điện thoại</label>
                    <input class="form-control" name="phone" type="text" value="{{$user->phone}}">
                </div>
            </div>
            <div class="col-6">
                <div class="form-group">
                    <label for="birthday" class="fs13 mb-0 text-bold">Ngày sinh</label>
                    <input class="form-control" name="birthday" type="text"
                           value="{{! empty($user->birthday) ? \Carbon\Carbon::createFromTimestamp($user->birthday)->format('d/m/Y') :''}}">
                </div>
            </div>
            <div class="col-12">
                <div class="form-group">
                    <label for="note" class="fs13 mb-0 text-bold">Mô tả về bản thân</label>
                    <textarea class="form-control" name="note" rows="5">{{$user->note}}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer p-0">
        <button type="button" class="btn btn-outline-primary btn-sm" onclick="updateAccount(this)"><i class="fa fa-save"></i> Lưu
        </button>
    </div>
</form>
<script>
    function updateAccount(e) {
        let element = $(e).parents('form');
        let url = element.attr('action');
        let method = element.attr('method');
        let data = $('#update-account').serialize();
        $.ajax(url,{
            data:data,
            type:method,
            dataType:'json',
            success :function (data) {
                if(data.success){
                    $('.modal').modal('hide');
                    $.notify({message:data.message},{type:'success'});
                    $('.full-name').text(data.user.first_name + ' ' + data.user.last_name)
                }else{
                    $.notify({message:data.message},{type:'danger'});
                    return false;
                }
            }
        })
    }
</script>
