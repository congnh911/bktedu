<!DOCTYPE html>
@langrtl
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    @endlangrtl
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        {!! SEOMeta::generate() !!}
        <meta name="description" content="@yield('meta_description', 'BKT Education')">
        <meta name="author" content="@yield('meta_author', 'CONA')">
        <link rel="shortcut icon" type="image/png" href="{{url('/img/bkt/logo.ico')}}"/>

        @yield('meta')

    {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}}
    @stack('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        {{ style(mix('css/frontend.css')) }}

        @stack('after-styles')
    </head>
    <body>
    @include('includes.partials.demo')

    <div id="app">
        @include('includes.partials.logged-in-as')
        @include('frontend.v1.includes.nav')

        <div class="col-md-12">
            @include('includes.partials.messages')
            @yield('content')
        </div><!-- container -->
        @include('frontend.v1.includes.footer_menu')
    </div><!-- #app -->
    <a href="#" id="back-to-top" title="Back to top" class="fs20"><span class="fa fa-chevron-up"></span></a>
    @include('includes.modal')
    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/manifest.js')) !!}
    {!! script(mix('js/vendor.js')) !!}
    {!! script(mix('js/frontend.js')) !!}
    {!! script('js/frontend/scripts.js') !!}
    @stack('after-scripts')
    <script>
        (function(s, u, b, i, z){
            u[i]=u[i]||function(){
                u[i].t=+new Date();
                (u[i].q=u[i].q||[]).push(arguments);
            };
            z=s.createElement('script');
            var zz=s.getElementsByTagName('script')[0];
            z.async=1; z.src=b; z.id='subiz-script';
            zz.parentNode.insertBefore(z,zz);
        })(document, window, 'https://widgetv4.subiz.com/static/js/app.js', 'subiz');
        subiz('setAccount', 'acqpasymliejggkmfmfl');
    </script>
    @include('includes.partials.ga')
    </body>
</html>
