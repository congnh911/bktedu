<nav class="navbar navbar-expand-lg navbar-light bg-main w-100" id="navbar-top">
    <a href="{{ url('/') }}" class="navbar-brand ml-4"><img src="{{url('img/bkt/logo.png')}}" alt="{{ app_name() }}" style="width:150px;"></a>

    <button class="navbar-toggler navbar-toggler-right btn-light" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="@lang('labels.general.toggle_navigation')">
        <span class="navbar-toggler-icon"></span>
    </button>


    <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
        <ul class="navbar-nav">
            @if(!empty($category_menu_global))
                @foreach($category_menu_global as $key => $item)
                <li class="nav-item {{url()->current() == $item->linkBlog() ? 'active' : ''}}">
                    <a href="{{$item->linkBlog()}}" class=" nav-link text-light
                        {{url()->current() == $item->linkBlog() ? 'text-bold' : ''}}">{{$item->name}}</a>

                </li>
                @endforeach
            @endif
        </ul>
    </div>

    <span class="text-bold text-orange fr hidden-xs">
        <span class="fa fa-mobile-alt"></span> 0989.999.666<br/>
        <span class="fa fa-mobile-alt"></span> 0917.888.568<br/>
    </span>
</nav>
{{--<div class="nav-category">--}}
{{--    <div class="container p-0">--}}
{{--        <ul>--}}
{{--            @if(!empty($category_menu_global))--}}
{{--                @foreach($category_menu_global as $key => $item)--}}
{{--                    <li><a href="{{$item->linkBlog()}}" class="{{url()->current() == $item->linkBlog() ? 'text-bold text-primary' : ''}}">{{$item->name}}</a></li>--}}
{{--                @endforeach--}}
{{--            @endif--}}
{{--        </ul>--}}
{{--    </div>--}}
{{--</div>--}}


