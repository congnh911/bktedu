<div class="footer-menu mt-3 bg-white">
    <div class="container pt-4 pb-3">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="menu-foot-1">Thông tin hỗ trợ</div>
                <ul>
                    <li><a href="#">Câu hỏi thường gặp</a></li>
                    <li><a href="#">Chi nhánh của BKT - EDU</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="menu-foot-2">Giới thiệu</div>
                <ul>
                    <li><a href="#">Trung tâm BKT - EDU</a></li>
                    <li><a href="#">Thông điệp từ ban giám đốc</a></li>
                </ul>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="menu-foot-3">Kết nối với chúng tôi</div>
                <ul>
                    <li><a href="#">Câu hỏi thường gặp</a></li>
                    <li><a href="#">Chi nhánh của BKT - EDU</a></li>
                </ul>
            </div>
        </div>

        <div class="mt-4">
{{--            Trụ sở chính: Công ty TNHH Hợp tác Quốc tế BKT<br/>--}}
{{--            Hà Nội: Số 80 ngõ 61 Hoàng Cầu - Ô chợ Dừa - Đống Đa - Hà Nội<br/>--}}

{{--            Nhật Bản:<br/>--}}

{{--            Trung tâm ngoại ngữ: 102 Trung Kính - Cầu Giấy - Hà Nội.<br/>--}}

{{--            Chính nhánh BKT<br/>--}}
            Hà Nội: Số 15/5, ngõ 104 Yên Lãng - Đống Đa - Hà Nội.<br/>
            TP. Hồ Chí Minh: 366 Cao Thắng - Quận 10 - Tp. Hồ Chí Minh<br/>
            Long An: 432 Châu Thị Kim - Tân An - Long An<br/>
            Vũng Tàu: 561-565 Trương Công Định - Tp. Vũng Tàu.<br/>
            Đà Nẵng: 28 Lê Đình Lý - Hải Châu - Đà Nẵng<br/>
            <hr/>

            <span class="fs16">© Bản quyền 2020 thuộc về Tổng công ty BKT-Edu</span>
        </div>
    </div>
</div>

