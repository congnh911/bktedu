<div class="sidebar-menu">
    @if(auth()->check())
    <a href="{{route('frontend.v1.user.account')}}" class="{{active_class(Active::checkUriPattern('account'))}}" title="{{auth()->user()->fullName()}}">
        <div class="sb-menu-item" style="padding-bottom: 5px !important;padding-top: 5px !important;">
            <div class="item-image" style="margin-left: -3px;margin-right: 12px">
                <img src="{{auth()->user()->avatar()}}" class="img-circle"
                     style="width: 30px;height: 30px;border: 1px solid #ccc;border-radius: 100%;">
            </div>
            <div class="item-text full-name" style="line-height: 30px;">
                {{auth()->user()->fullName()}}
            </div>
        </div>
    </a>
    @endif

    @foreach(config('setting.menu_sidebar') as $key => $item)
        <a href="{{route($item['link'])}}" class="
        {{\Request::is($item['key'].'*') || Active::checkRoute('frontend.v1.index') && $item['key'] == 'home' ? 'active' : null}}"
           title="{{$item['text']}}">
            <div class="sb-menu-item">
                <div class="item-image">
                    <i class="{{$item['icon']}}"></i>
                </div>
                <div class="item-text">
                    {{$item['text']}}
                    @if($item['key'] == 'notification' && $total_unread_notification > 0)
                        <span class="text-red badge badge-danger">{{$total_unread_notification}}</span>
                    @endif
                </div>

            </div>
        </a>
    @endforeach
</div>
