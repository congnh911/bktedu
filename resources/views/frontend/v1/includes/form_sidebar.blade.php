<div class="card mt-3">
    <div class="card-body">
        <h2 class="title-card text-bold text-primary">
            <img src="{{url('img/bkt/icon-header.png')}}" class="mr-2 fl">
            <div class="ml-3 fs28">Tư vấn <br/> trực tuyến</div>
        </h2>
        <div class="form-regis mt-3">
            <form method="post" action="{{route('frontend.v1.contact.store')}}" id="frm-contact">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="text" class="form-control" name="full_name" placeholder="Họ và tên">
                <input type="text" class="form-control" name="birthday" placeholder="Ngày sinh">
                <input type="text" class="form-control" name="email" placeholder="Email">
                <input type="text" class="form-control" name="phone" placeholder="Điện thoại">
                <input type="text" class="form-control" name="address" placeholder="Địa chỉ">
                <select name="chooose" class="form-control">
                    <option value="">Nước du học</option>
                </select>
                <textarea name="description" class="form-control" placeholder="Nội dung"
                          rows="2"></textarea>
                <button type="submit" class="btn btn-primary mt-2 pl-4 pr-4">ĐĂNG KÝ</button>
            </form>
        </div>
    </div>
</div>
@push('after-scripts')
<script type="text/javascript">
    $(function () {
        var frm_category = $('#frm-contact');

        frm_category.validate({
            wrapper: "div",
            rules: {
                full_name: {
                    required:true,
                    minlength:4
                }
            },
            messages: {
                full_name: {
                    required:"Tiêu đề không để trống",
                    minlength: "Độ dài tối thiểu là 4 ký tự"
                }
            },
            onfocusout: function(){
                if (frm_category.valid()) {
                    $('button[type=submit]').prop('disabled', false);
                } else {
                    $('button[type=submit]').prop('disabled', 'disabled');
                }
            }

        });
    })
</script>
@endpush
