<div class="card">
    <div class="card-body">
        <h2 class="title-card text-bold text-primary">
            <img src="{{url('img/bkt/icon-header_highlight.png')}}" class="mr-2 fl">
            <div class="ml-3 fs28">Hotline<br/> tư vấn khóa học</div>
        </h2>
        <div class="info-sidebar pt-3 fs20">
            <strong class="text-primary">Tư vấn học bổng du học</strong><br/>
            <span class="text-orange text-bold">0917.888.568</span>
            <hr/>
            <strong class="text-primary">Tư vấn du học hè</strong><br/>
            <span class="text-orange text-bold">0989.999.666</span>
            <hr/>
            <strong class="text-primary">Tư vấn du học Châu Á</strong><br/>
            <span class="text-orange text-bold">0902.222.000</span>
            <hr/>
            <strong class="text-primary">Tư vấn du học Châu Âu</strong><br/>
            <span class="text-orange text-bold">099.9999.111</span>
        </div>
    </div>
</div>
