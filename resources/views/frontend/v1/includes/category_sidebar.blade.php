<div class="card mt-3">
    <div class="card-body">
        <h2 class="title-card text-bold text-primary">
            <img src="{{url('img/bkt/icon-header.png')}}" class="mr-2 fl">
            <div class="ml-3 fs28">{!! $cat_title !!}</div>
        </h2>
        <div class="news-highligh pt-2">
            <div class="row">
                @if(!empty($data))
                    @foreach($data as $key => $item)
                        @if($key == 0)
                            <div class="col-12">
                                <a href="{{$item->link()}}"><img src="{{$item->avatar()}}" style="width: 100%"></a>
                                <a href="{{$item->link()}}"><h3
                                        class="text-primary fs18 text-bold mt-2">{{$item->name}}</h3></a>
                                <p>
                                    {{strip_tags($item->description)}}
                                </p>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
            <div class="row">
                <div class="col-12 ml-3">
                    <ul class="news-relate-more">
                        @if(!empty($data))
                            @foreach($data as $key => $item)
                                @if($key > 0)
                                    <li><a href="{{$item->link()}}">{{$item->name}}</a></li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
