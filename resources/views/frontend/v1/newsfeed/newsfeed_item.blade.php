<div class="newsfeed-item">
    @include('frontend.v1.newsfeed.includes.item_header')
    @include('frontend.v1.newsfeed.includes.item_body')
    @include('frontend.v1.newsfeed.includes.item_footer')
</div>
