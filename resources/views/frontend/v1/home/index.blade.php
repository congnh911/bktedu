@extends('frontend.v1.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')
    <div class="container">
        <div class="row " style="padding-top: 75px;">
            <div class="col-md-12 p-0 m-0">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                    </ol>
                    <div class="carousel-inner">
                        @if(!empty($slide_header->albums))
                            @foreach($slide_header->albums as $key => $item)
                                <div class="carousel-item {{$key == 0 ? 'active' : ''}}">
                                    <a href="{{$item->link}}" title="{{$item->name}}"><img class="d-block w-100"
                                                                                           src="{{$item->gallery()}}"/></a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row mt-3 mb-3">
            <div class="col-md-8 p-0 m-0 col-xs-12">

                @include('frontend.v1.home.includes.blog_hot',['data' => $blog_news ,'cat_title' => 'Tin tức <br/> tổng hợp mới nhất'])
                @include('frontend.v1.home.includes.box_category',['data' => $blog_duhoc ,'cat_title' => 'Học bổng <br/> du học'])
                @include('frontend.v1.home.includes.box_category',['data' => $blog_duhocchaua,'cat_title' => 'Du học <br/> châu Á'])
                @include('frontend.v1.home.includes.box_category',['data' => $blog_duhocchauau,'cat_title' => 'Du học <br/> châu Âu'])
                @include('frontend.v1.home.includes.box_category',['data' => $blog_duhocchaumy,'cat_title' => 'Du học <br/> châu Mỹ'])
                @include('frontend.v1.home.includes.box_category',['data' => $blog_duhocchauuc,'cat_title' => 'Du học <br/> châu Úc'])
            </div>
            <div class="col-md-4 col-xs-12 pr-0 sidebar-right">
                @include('frontend.v1.includes.info_sidebar')
                @include('frontend.v1.includes.category_sidebar',['data' => $blog_handbook ?? null,'cat_title' => 'Cẩm nang <br/> du học'])
                @include('frontend.v1.includes.category_sidebar',['data' => $blog_video ?? null,'cat_title' => 'Video clip <br/> những hoạt động'])
                @include('frontend.v1.includes.form_sidebar')
            </div>
        </div>
    </div>
@endsection
