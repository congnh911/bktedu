<div class="box-card mt-3">
    {{--                <div class="box-card-header"><h4 class="title-header fs16">Bài viết hay nhất <a href="#" class="pull-right">Xem thêm</a></h4> </div>--}}

    <div class="box-card-body">
        <div class="row no-padding no-margin">
            @if(!empty($blog_highlight))
                @foreach($blog_highlight as $key => $item)
                    <div class="col-md-4 no-padding no-margin">
                        <div class="item_blog">
                            <a href="{{$item->link()}}" title="{{$item->name}}">
                                <img src="{{$item->avatar()}}">
                                <div class="item_info">
                                    <h4 class="title_item">{{$item->name}}</h4>
                                    <span class="date_item fs13"><i class="fa fa-clock"></i> {{\Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</span>
                                </div>
                            </a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
