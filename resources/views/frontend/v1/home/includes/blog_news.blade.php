<div class="box-card mt-3">
    <div class="box-card-header"><h4 class="title-header fs16">Bài viết mới nhất</h4></div>
    <div class="box-card-body">
        <ul class="list-blog">
            @if(!empty($blog_news))
                @foreach($blog_news as $key => $item)
                    <li class="list-blog-item">
                        <div class="blog-avatar">
                            <a href="#">
                                <img src="{{$item->avatar()}}" class="">
                            </a>
                        </div>
                        <div class="blog-description">
                            <a href="#">
                                <h4 class="blog-title">{{$item->name}}</h4>
                            </a>
                            <p class="blog-text">
                                {{$item->description}}
                            </p>
                            <span class="blog-more">
                        <img src="/images/image1.jpg" class="avatar"> <a href="#" class="text-bold">Admin istrator</a>
                        <span class="text-grey">4 giờ trước - 30 người xem</span>
                    </span>
                        </div>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
    <div class="box-card-footer">
        <a href="#" class="load_more"> <i class="fa fa-spinner"></i> Xem thêm </a>
    </div>
</div>
