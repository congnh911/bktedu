<div class="box-category-item mt-3">
    <div class="box-category-header">
        <h4>
            <a href="#" class="category-title text-uppercase text-bold">Hello World</a>
        </h4>
    </div>
    <div class="box-content">
        <div class="row no-padding no-margin">
            @if(!empty($blog_highlight))
                @foreach($blog_highlight as $key => $item)
                    @if($key == 0)
                        <div class="col-md-6 pl-0">
                            <div class="item_blog_top">
                                <a href="{{$item->link()}}" title="{{$item->name}}">
                                    <img src="{{$item->avatar()}}">
                                </a>
                                <div class="item_info mt-2">
                                    <h4 class="title_item">
                                        <a href="{{$item->link()}}" title="{{$item->name}}">{{$item->name}}</a>
                                    </h4>
                                    <p>{!! $item->description !!}</p>
                                    <span class="date_item fs13"><i class="fa fa-clock"></i> {{\Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</span>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
            <div class="col-md-6 no-padding no-margin">
                @if(!empty($blog_highlight))
                    @foreach($blog_highlight as $key => $item)
                        @if($key > 0)
                            <div class="item_blog_small">
                                <a href="{{$item->link()}}" title="{{$item->name}}">
                                    <img src="{{$item->avatar()}}" alt="">
                                </a>
                                <div class="cat_item_blog">

                                    <h5 class="title_item"><a href="{{$item->link()}}"
                                                              title="{{$item->name}}">{{$item->name}}</a></h5>
                                    {{--                                    <p class="text-truncate">{{$item->description}}</p>--}}
                                    <span class="date_item fs13"><i class="fa fa-clock"></i> {{\Carbon\Carbon::parse($item->created_at)->diffForHumans()}}</span>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
