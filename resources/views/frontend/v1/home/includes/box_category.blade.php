<div class="card mt-3">
    <div class="card-body">
        <h2 class="title-card text-bold text-primary">
            <img src="{{url('img/bkt/icon-header.png')}}" class="mr-2 fl">
            <div class="ml-3 fs28">{!! $cat_title !!}</div>
        </h2>
        <div class="news-highligh pt-2">
            @foreach($data as $key => $item)
                @if($key == 0)
                    <div class="row">
                        <div class="col-md-5 col-xs-12 ">
                            <a href="{{$item->link()}}"><img src="{{$item->avatar()}}" style="width: 100%"></a>
                        </div>
                        <div class="col-md-7 col-xs-12">
                            <a href="{{$item->link()}}">
                                <h3 class="text-primary text-title fs18 text-bold">
                                    {{$item->name}}
                                </h3></a>
                            <p>
                                {{ $item->description }}
                            </p>
                        </div>
                    </div>
                @endif
            @endforeach
            <div class="row mt-3">
                <div class="col-12 ml-3">
                    <ul class="news-relate-more">
                        @foreach($data as $key => $item)
                            @if($key > 0)
                                <li><a href="{{$item->link()}}">{{$item->name}}</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
