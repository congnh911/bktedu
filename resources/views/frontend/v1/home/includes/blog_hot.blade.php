<div class="card">
    <div class="card-body">
        <h2 class="title-card text-bold text-primary">
            <img src="{{url('img/bkt/icon-header_highlight.png')}}" class="mr-2 fl">
            <div class="ml-3 fs28">{!! $cat_title !!}</div>
        </h2>
        <div class="news-highligh pt-2">
            <div class="row">
                @foreach($data as $key => $item)
                    @if($key == 0)
                        <div class="col-md-5 col-xs-12 pr-0">
                            <a href="{{$item->link()}}"><img src="{{$item->avatar()}}" style="width: 100%"></a>
                        </div>
                        <div class="col-md-7 col-xs-12">
                            <a href="{{$item->link()}}">
                                <h3 class="text-primary text-title fs18 text-bold">{{$item->name}} </h3></a>
                            <p>
                                {{$item->description}}
                            </p>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="row mt-3 mr-0">
                @foreach($data as $key => $item)
                    @if($key > 0)
                        <div class="col-md-4 pr-0">
                            <a href="{{$item->link()}}">
                                <img src="{{$item->avatar()}}" style="width: 100%;height:120px;object-fit:cover;">
                                <h4 class="fs14 mt-1 text-primary">{{$item->name}}</h4>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
</div>
