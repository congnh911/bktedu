<div class="box-card">
    <div class="box-card-header">
        <span class="text-bold"><i class="fa fa-tags"></i> Tag</span>
    </div>
    <div class="box-card-body pb-2">
        <div class="tags-item p-2">
            @if(!empty($category_menu_global))
                @foreach($category_menu_global as $key => $item)
                    <div class="badge badge-light float-left mb-2 mr-2">
                        <a href="{{$item->linkBlog()}}"> <i class="fa fa-hashtag"></i> {{$item->name}}</a>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>

