<div class="box-card">
    <div class="box-card-header pl-3">
        <div class="card-tile text-bold">
            Bài viết nổi bật
        </div>
    </div>
    <div class="box-card-body p-3">
        <ul class="list-blog">
            @if(!empty($listBlog))
                @foreach($listBlog as $key => $item)
                    <li style="display: block;overflow: hidden" class="mb-3">
                        <a href="{{$item->link()}}" title="{{$item->name}}" class="block">
                            <img src="{{$item->avatar()}}" alt="" class="pull-left" style="width: 70px;height: 70px">
                            <h4 class="fs16 text-bold" style="margin-left: 80px">{{$item->name}}</h4>
                        </a>
                    </li>
                @endforeach
            @endif
        </ul>
    </div>
</div>
