<div class="card">
    <div class="card-header pl-3">
        <div class="card-tile text-bold">
            Danh mục
            @if(!empty($category))
                <span class="text-bold text-dark">"{{$category->name}}"</span>
            @endif
        </div>
    </div>
    <div class="box-card-body p-3">
        @if(!empty($listBlog))
            @foreach($listBlog as $key => $item)
                <div class="row mb-3 list-blog">
                    <div class="col-md-5 col-xs-12">
                        <a href="{{$item->link()}}">
                            <img src="{{$item->avatar()}}" class="w-100" style="max-height:200px;object-fit: cover">
                        </a>
                    </div>
                    <div class="col-md-7 col-xs-12">
                        <a href="{{$item->link()}}" title="{{$item->name}}">
                            <h4 class="blog-title m-0 fs20 text-body">{{$item->name}}</h4>
                        </a>
                        <p class="blog-text mb-1">
                            {!! $item->description !!}
                        </p>
                        <span class="blog-more">
                            <img src="{{$item->creator->avatar()}}" class="avatar">
                            <a href="{{$item->creator->link()}}" class="text-bold">
                                {{$item->creator->fullName()}}
                            </a>
                            <span class="text-grey fs13">
                                {{\Carbon\Carbon::parse($item->created_at)->diffForHumans()}} - {{$item->views}} lượt xem
                            </span>
                        </span>
                    </div>
                </div>
                <hr/>
            @endforeach
        @endif

    </div>
    <div class="card-footer">
            <div class="fl">Có tất cả {{$listBlog->total()}} bài viết</div>
        <div class="fr">
            {!! $listBlog->appends(request()->query())->links()  !!}
        </div>
    </div>
</div>
