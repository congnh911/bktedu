<div class="item-header mt-4 mb-4">
    <div class="avatar pull-left">
        <a href="{{route('frontend.v1.user.account')}}">
            <img src="{{$creator->avatar()}}">
        </a>
    </div>
    <div></div>
    <div class="content-header mt-1">
        <a href="{{route('frontend.v1.user.account')}}" class="">
            <span class="text-bold">{{$creator->fullName()}}</span>
        </a>
        <br>
        @if(!empty($data))
            <small class="text-grey">đã đăng </small>
            <span class="fs12 text-grey">{{\Carbon\Carbon::parse($data->created_at)->diffForHumans()}}</span>
        @endif
    </div>
</div>
