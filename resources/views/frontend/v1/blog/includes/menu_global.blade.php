<div class="box-card mb-3">
    <div class="box-card-header">
        <a href="{{route('frontend.v1.blog.index')}}">
            <span class="text-bold card-title"><i class="fa fa-list-alt"></i> Tất cả {{mb_strtolower(__('labels.general.category'))}}</span>
        </a>
    </div>
    <div class="box-card-body">
        <div class="menu-category-global p-3">
            <ul>
                @if(!empty($category_menu_global))
                    @foreach($category_menu_global as $key => $item)
                        <li>
                            <a href="{{$item->linkBlog()}}" title="{{$item->name}}"
                               class="{{url()->current() == $item->linkBlog() || (!empty($category_id) && $category_id == $item->id) ? 'active' : null}} fs16">
                                <i class="far fa-caret-square-right"></i> {{$item->name}}
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
</div>

