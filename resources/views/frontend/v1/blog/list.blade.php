@extends('frontend.v1.layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))
@section('content')

    <div class="row pt-5">
        <div class="container">
            <div class="row mt-4 mb-3 ">
                <div class="col-md-8 p-0 m-0 col-xs-12">
                    @include('frontend.v1.blog.includes.list_blog')
                </div>
                <div class="col-md-4 col-xs-12 sidebar-right">
                    @include('frontend.v1.includes.info_sidebar')
                    @include('frontend.v1.includes.category_sidebar',['data' => $blog_handbook ?? null,'cat_title' => 'Cẩm nang <br/> du học'])
                    @include('frontend.v1.includes.category_sidebar',['data' => $blog_video ?? null,'cat_title' => 'Video clip <br/> những hoạt động'])
                    @include('frontend.v1.includes.form_sidebar')
                </div>
            </div>
        </div>
    </div>
@endsection
