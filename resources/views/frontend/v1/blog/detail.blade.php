@extends('frontend.v1.layouts.app')

@section('content')

    <div class="row pt-5">
        <div class="container">
            <div class="row mt-4 mb-3 ">
                <div class="col-md-8 p-0 m-0 col-xs-12">
                    <div class="card">
                        <div class="card-body content-detail">
                            <h1 class="fs28">{{$blog->name}}</h1>

                            <span style="color:#999999" class="fs13"><small>Người đăng</small> {{$blog->creator->name}}  <small>lúc</small> {{\Carbon\Carbon::parse($blog->created_at)->format('H:i d/m/Y')}}</span>
                            <hr/>
                            @if(!empty($blog->contentMain))
                                <p>
                                    {!! $blog->contentMain->content !!}
                                </p>
                            @else
                                Đang cập nhật
                            @endif
                        </div>
                    </div>

                    <div class="mt-3 card">
                        <div class="card-body blog-related">
                            <h4>Bài viết liên quan</h4>
                            <div class="row">
                                @if(!empty($blogRelated))
                                    @foreach($blogRelated as $key => $item)
                                        <div class="col-xs-12 col-md-4">
                                            <img src="{{$item->avatar()}}" alt="{{$item->name}}"
                                                 style="width: 100%;height:120px;object-fit:cover;">
                                            <p class="mt-10">
                                                <a href="{{$item->link()}}" class="text-title"
                                                   title="{{$item->name}}">{{$item->name}}</a>
                                            </p>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12 sidebar-right">
                    @include('frontend.v1.includes.info_sidebar')
                    @include('frontend.v1.includes.category_sidebar',['data' => $blog_handbook ?? null,'cat_title' => 'Cẩm nang <br/> du học'])
                    @include('frontend.v1.includes.category_sidebar',['data' => $blog_video ?? null,'cat_title' => 'Video clip <br/> những hoạt động'])
                    @include('frontend.v1.includes.form_sidebar')
                </div>
            </div>
        </div>
    </div>
@endsection
