<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Truy cập',

            'roles' => [
                'all' => 'Tất cả vai trò',
                'create' => 'Tạo vai trò',
                'edit' => 'Sửa vai trò',
                'management' => 'Quản lý vai trò',
                'main' => 'Vai trò',
            ],
            'permissions' => [
                'all' => 'Tất cả quyền',
                'create' => 'Tạo quyền',
                'edit' => 'Sửa quyền',
                'management' => 'Quản lý quyền',
                'main' => 'Quyền',
            ],

            'users' => [
                'all' => 'Tất cả người dùng',
                'change-password' => 'Thay đổi mật khẩu',
                'create' => 'Tạo người dùng',
                'deactivated' => 'Hủy kích hoạt người dùng',
                'deleted' => 'Xóa người dùng',
                'edit' => 'Sửa người dùng',
                'main' => 'Người dùng',
                'view' => 'Xem người dùng',
            ],
        ],

        'log-viewer' => [
            'main' => 'Quản lý Logs',
            'dashboard' => 'Dashboard',
            'logs' => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Bảng điều khiển',
            'general' => 'Chung',
            'history' => 'Lịch sử',
            'system' => 'Hệ thông',
        ],
    ],

    'language-picker' => [
        'language' => 'Ngôn ngữ',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'ar' => 'Arabic',
            'zh' => 'Chinese Simplified',
            'zh-TW' => 'Chinese Traditional',
            'da' => 'Danish',
            'de' => 'German',
            'el' => 'Greek',
            'en' => 'English',
            'es' => 'Spanish',
            'fa' => 'Persian',
            'fr' => 'French',
            'he' => 'Hebrew',
            'id' => 'Indonesian',
            'it' => 'Italian',
            'ja' => 'Japanese',
            'nl' => 'Dutch',
            'no' => 'Norwegian',
            'pt_BR' => 'Brazilian Portuguese',
            'ru' => 'Russian',
            'sv' => 'Swedish',
            'th' => 'Thai',
            'tr' => 'Turkish',
            'uk' => 'Ukrainian',
            'vi' => 'Việt Nam',
        ],
    ],
];
