<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'roles' => [
            'created' => 'Tạo Role thành công.',
            'deleted' => 'Xóa Role thành công.',
            'updated' => 'Cập nhật Role thành công.',
        ],
        'permissions' => [
            'created' => 'Tạo quyền thành công.',
            'deleted' => 'Xóa quyền thành công.',
            'updated' => 'Cập nhật quyền thành công.',
        ],

        'users' => [
            'cant_resend_confirmation' => 'Ứng dụng hiện được thiết lập để phê duyệt thủ công người dùng.',
            'confirmation_email' => 'Một email xác nhận mới đã được gửi đến địa chỉ trong hồ sơ.',
            'confirmed' => 'Người dùng đã được xác nhận thành công.',
            'created' => 'Người dùng đã được tạo thành công.',
            'deleted' => 'Người dùng đã bị xóa thành công.',
            'deleted_permanently' => 'Người dùng đã bị xóa vĩnh viễn.',
            'restored' => 'Người dùng đã được khôi phục thành công.',
            'session_cleared' => "Phiên của người dùng đã được xóa thành công.",
            'social_deleted' => 'Đã xóa thành công Tài khoản xã hội',
            'unconfirmed' => 'Người dùng chưa được xác nhận',
            'updated' => 'Người dùng đã được cập nhật thành công.',
            'updated_password' => "Mật khẩu người dùng đã được cập nhật thành công.",
        ],
    ],

    'frontend' => [
        'contact' => [
            'sent' => 'Your information was successfully sent. We will respond back to the e-mail provided as soon as we can.',
        ],
    ],
];
