<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'roles' => [
                'already_exists' => 'Vai trò đó đã tồn tại. Vui lòng chọn một tên khác.',
                'cant_delete_admin' => 'Bạn không thể xóa vai trò Quản trị viên.',
                'create_error' => 'Có lỗi xảy ra khi tạo vai trò. Vui lòng thử lại.',
                'delete_error' => 'Có lỗi xảy ra khi xóa vai trò. Vui lòng thử lại.',
                'has_users' => 'Bạn không thể xóa vai trò khi có người dùng đang được liên kết.',
                'needs_permission' => 'Bạn phải chọn ít nhất một quyền cho vai trò này.',
                'not_found' => 'Vai trò không tồn tại.',
                'update_error' => 'Có lỗi xảy ra khi cập nhật vai trò. Vui lòng thử lại.',
            ],
            'permissions' => [
                'already_exists' => 'Quyền đó đã tồn tại. Vui lòng chọn một tên khác.',
                'cant_delete_admin' => 'Bạn không thể xóa quyền Quản trị viên.',
                'create_error' => 'Có lỗi xảy ra khi tạo quyền. Vui lòng thử lại.',
                'delete_error' => 'Có lỗi xảy ra khi xóa quyền. Vui lòng thử lại.',
                'has_users' => 'Bạn không thể xóa quyền khi có người dùng đang được liên kết.',
                'needs_permission' => 'Bạn phải chọn ít nhất một quyền cho quyền này.',
                'not_found' => 'Quyền không tồn tại.',
                'update_error' => 'Có lỗi xảy ra khi cập nhật quyền. Vui lòng thử lại.',
            ],

            'users' => [
                'already_confirmed' => 'Người dùng này đã được xác nhận.',
                'cant_confirm' => 'Đã xảy ra sự cố khi xác nhận tài khoản người dùng.',
                'cant_deactivate_self' => 'Bạn không thể làm điều đó với chính mình.',
                'cant_delete_admin' => 'Bạn không thể xóa super administrator.',
                'cant_delete_self' => 'Bạn không thể xóa chính mình.',
                'cant_delete_own_session' => 'Bạn không thể xóa phiên của riêng bạn.',
                'cant_restore' => 'Người dùng này không bị xóa nên không thể khôi phục.',
                'cant_unconfirm_admin' => 'Bạn không thể hủy xác nhận super administrator.',
                'cant_unconfirm_self' => 'Bạn không thể tự xác nhận.',
                'create_error' => 'Có lỗi xảy ra khi tạo người dùng. Vui lòng thử lại.',
                'delete_error' => 'Có lỗi xảy ra khi xóa người dùng. Vui lòng thử lại.',
                'delete_first' => 'Người dùng này phải bị xóa trước khi có thể bị hủy vĩnh viễn.',
                'email_error' => 'Địa chỉ email đó thuộc về một người dùng khác.',
                'username_error' => 'Tài khoản đó thuộc về một người dùng khác.',
                'mark_error' => 'Có lỗi xảy ra khi cập nhật người dùng. Vui lòng thử lại.',
                'not_confirmed' => 'Người dùng không được xác nhận.',
                'not_found' => 'Người dùng không tồn tại.',
                'restore_error' => 'Có lỗi xảy ra khi khôi phục người dùng này. Vui lòng thử lại.',
                'role_needed_create' => 'Bạn phải chọn ít nhất một vai trò.',
                'role_needed' => 'Bạn phải chọn ít nhất một vai trò.',
                'social_delete_error' => 'Có lỗi xảy ra khi xóa tài khoản xã hội khỏi người dùng.',
                'update_error' => 'Có lỗi xảy ra khi cập nhật người dùng này. Vui lòng thử lại.',
                'update_password_error' => 'Có lỗi xảy ra khi thay đổi mật khẩu người dùng này. Vui lòng thử lại.',
            ],
        ],
    ],

    'frontend' => [
        'auth' => [
            'confirmation' => [
                'already_confirmed' => 'Your account is already confirmed.',
                'confirm' => 'Confirm your account!',
                'created_confirm' => 'Your account was successfully created. We have sent you an e-mail to confirm your account.',
                'created_pending' => 'Your account was successfully created and is pending approval. An e-mail will be sent when your account is approved.',
                'mismatch' => 'Your confirmation code does not match.',
                'not_found' => 'That confirmation code does not exist.',
                'pending' => 'Your account is currently pending approval.',
                'resend' => 'Your account is not confirmed. Please click the confirmation link in your e-mail, or <a href=":url">click here</a> to resend the confirmation e-mail.',
                'success' => 'Your account has been successfully confirmed!',
                'resent' => 'A new confirmation e-mail has been sent to the address on file.',
            ],

            'deactivated' => 'Your account has been deactivated.',
            'email_taken' => 'That e-mail address is already taken.',

            'password' => [
                'change_mismatch' => 'That is not your old password.',
                'reset_problem' => 'There was a problem resetting your password. Please resend the password reset email.',
            ],

            'registration_disabled' => 'Registration is currently closed.',
        ],
    ],
];
