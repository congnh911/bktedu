<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Labels Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in labels throughout the system.
    | Regardless where it is placed, a label can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'general' => [
        'all' => 'Tất cả',
        'yes' => 'Có',
        'no' => 'Không',
        'general' => 'Thông tin chung',
        'info' => 'Thông tin',
        'copyright' => 'Copyright',
        'custom' => 'Tùy biến',
        'actions' => 'Tác vụ',
        'active' => 'Hoạt động',
        'buttons' => [
            'save' => 'Lưu',
            'update' => 'Cập nhật',
        ],
        'hide' => 'Ẩn',
        'inactive' => 'Không hoạt động',
        'none' => 'None',
        'show' => 'Hiển thị',
        'toggle_navigation' => 'Chuyển đổi điều hướng',
        'create_new' => 'Thêm mới',
        'toolbar_btn_groups' => 'Thanh công cụ với các nhóm nút',
        'more' => 'Xem thêm',
        'management' => 'Quản lý',
        'title' => 'Tiêu đề',
        'avatar' => 'Ảnh đại diện',
        'created_at' => 'Ngày tạo',
        'highlight' => 'Nổi bật',
        'status' => 'Trạng thái',
        'blog' => 'bài viết',
        'product' => 'sản phẩm',
        'category' => 'Danh mục',
        'history' => 'Lịch sử',
        'add' => 'Thêm mới',
        'create' => 'Thêm mới',
        'edit' => 'Sửa',
        'update' => 'Cập nhật',
        'delete' => 'Xóa',
        'delete-permanent' => 'Xóa vĩnh viễn',
        'deleted' => 'Đã xóa',
        'list' => 'Danh sách',
        'description' => 'Mô tả',
        'content' => 'Nội dung',
        'file-attach' => 'Tệp đính kèm',
        'download' => 'Tải xuống',
        'close' => 'Đóng',
        'creator' => 'Người tạo',
        'last_update' => 'Cập nhật lần cuối',
        'deleted_at' => 'Thời gian xóa',
        'name_category' => 'Tên danh mục',
        'album' => 'Thư viện ảnh',
        'type' => 'Kiểu',
        'keyword' => 'Từ khóa',
        'department' => 'Phòng ban',
        'note' => 'Ghi chú',
        'setup' => 'Cài đặt chung',
        'slide' => 'Slide',
        'position' => 'Vị trí',
        'gallery' => 'Gallery'


    ],

    'backend' => [
        'access' => [
            'roles' => [
                'create' => 'Thêm mới vai trò',
                'edit' => 'Cập nhật vai trò',
                'management' => 'Quản lý vai trò',

                'table' => [
                    'number_of_users' => 'Số lượng thành viên',
                    'permissions' => 'Quyền',
                    'role' => 'Vai trò',
                    'sort' => 'Sắp xếp',
                    'total' => 'Tổng số vai trò',
                ],
            ],
            'permissions' => [
                'create' => 'Thêm mới quyền',
                'edit' => 'Cập nhật quyền',
                'management' => 'Quản lý quyền',

                'table' => [
                    'number_of_users' => 'Số lượng thành viên',
                    'permissions' => 'Quyền',
                    'sort' => 'Sắp xếp',
                    'total' => 'Tổng số quyền',
                ],
            ],

            'users' => [
                'active' => 'Thành viên hoạt động',
                'all_permissions' => 'Tất cả quyền',
                'change_password' => 'Thay đổi mật khẩu',
                'change_password_for' => 'Thay đổi mật khẩu cho :user',
                'create' => 'Thêm mới thành viên',
                'deactivated' => 'Thành viên không hoạt động',
                'deleted' => 'Thành viên đã xóa',
                'edit' => 'Cập nhật thành viên',
                'management' => 'Quản lý thành viên',
                'no_permissions' => 'Không có quyền',
                'no_roles' => 'Không có vai trò.',
                'permissions' => 'Quyền',
                'user_actions' => 'Tác vụ thành viên',

                'table' => [
                    'confirmed' => 'Đã xác nhận',
                    'created' => 'Đã tạo',
                    'email' => 'E-mail',
                    'username' => 'Tài khoản',
                    'id' => 'ID',
                    'last_updated' => 'Cập nhật lần cuối',
                    'name' => 'Tên',
                    'first_name' => 'Tên',
                    'last_name' => 'Họ',
                    'no_deactivated' => 'Không có thành viên bị vô hiệu hóa',
                    'no_deleted' => 'Không có thành viên đã xóa',
                    'other_permissions' => 'Quyền khác',
                    'permissions' => 'Quyền',
                    'abilities' => 'Khả năng',
                    'roles' => 'Vai trò',
                    'social' => 'Mạng xã hội',
                    'total' => 'Tổng số thành viên',
                ],

                'tabs' => [
                    'titles' => [
                        'overview' => 'Tổng quan',
                        'history' => 'Lịch sử',
                    ],

                    'content' => [
                        'overview' => [
                            'avatar' => 'Ảnh đại diện',
                            'confirmed' => 'Đã xác nhận',
                            'created_at' => 'Thời gian tạo',
                            'deleted_at' => 'Thời gian xóa',
                            'email' => 'E-mail',
                            'username' => 'Tài khoản',
                            'last_login_at' => 'Thời gian đăng nhập cuối',
                            'last_login_ip' => 'IP đăng nhập cuối',
                            'last_updated' => 'Thời gian cập nhật cuối',
                            'name' => 'Tên',
                            'first_name' => 'Tên',
                            'last_name' => 'Họ',
                            'status' => 'Trạng thái',
                            'timezone' => 'Múi giờ',
                        ],
                    ],
                ],

                'view' => 'Xem thành viên',
            ],
        ]
    ],

    'frontend' => [
        'auth' => [
            'login_box_title' => 'Đăng nhập',
            'login_button' => 'Đăng nhập',
            'login_with' => 'Đăng nhập với :social_media',
            'register_box_title' => 'Đăng ký',
            'register_button' => 'Đăng ký',
            'remember_me' => 'Ghi nhớ',
        ],

        'contact' => [
            'box_title' => 'Liên hệ',
            'button' => 'Gửi thông tin',
        ],

        'passwords' => [
            'expired_password_box_title' => 'Mật khẩu của bạn đã hết hạn.',
            'forgot_password' => 'Quên mật khẩu?',
            'reset_password_box_title' => 'Đặt lại mật khẩu',
            'reset_password_button' => 'Đặt lại mật khẩu',
            'update_password_button' => 'Cập nhật mật khẩu',
            'send_password_reset_link_button' => 'Gửi liên kết đặt lại mật khẩu',
        ],

        'user' => [
            'passwords' => [
                'change' => 'Đổi mật khẩu',
            ],

            'profile' => [
                'avatar' => 'Ảnh đại diện',
                'created_at' => 'Ngày tạo',
                'edit_information' => 'Sửa thông tin',
                'email' => 'E-mail',
                'username' => 'Tài khoản',
                'last_updated' => 'Cập nhật lần cuối',
                'name' => 'Tên',
                'first_name' => 'Tên',
                'last_name' => 'Họ',
                'update_information' => 'Cập nhật thông tin',
            ],
        ],
    ],
];
