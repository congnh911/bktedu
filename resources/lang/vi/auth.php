<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Tài khoản hoặc mật khẩu không đúng.',
    'general_error' => 'Bạn không có quyền truy cập để làm điều đó.',
    'password_rules' => 'Mật khẩu của bạn phải dài hơn 8 ký tự, phải chứa ít nhất 1 chữ hoa, 1 chữ thường và 1 số.',
    'password_used' => 'Bạn không thể đặt mật khẩu mà bạn đã sử dụng trước đó.',
    'socialite' => [
        'unacceptable' => ':provider Không được chấp nhận.',
    ],
    'throttle' => 'Quá nhiều lần đăng nhập. Vui lòng thử lại sau :seconds giây.',
    'unknown' => 'Xảy ra lỗi không xác định',
];
