<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'categories' => [
            'activate' => 'Kích hoạt',
            'deactivate' => 'Hủy kích hoạt',
            'delete_permanently' => 'Xóa vĩnh viễn',
            'restore' => 'Khôi phục',
        ],
        'access' => [
            'users' => [
                'activate' => 'Kích hoạt',
                'change_password' => 'Đổi mật khẩu',
                'clear_session' => 'Xóa Session',
                'confirm' => 'Xác nhận',
                'deactivate' => 'Khóa',
                'delete_permanently' => 'Xóa vĩnh viễn',
                'login_as' => 'Đăng nhập với quyền :user',
                'resend_email' => 'Gửi lại email xác nhận',
                'restore_user' => 'Khôi phục tài khoản',
                'unconfirm' => 'Un-confirm',
                'unlink' => 'Unlink',
            ],
        ],
    ],

    'emails' => [
        'auth' => [
            'confirm_account' => 'Xác nhận tài khoản',
            'reset_password' => 'Đặt lại mật khẩu',
        ],
    ],

    'general' => [
        'cancel' => 'Hủy',
        'continue' => 'Tiếp tục',

        'crud' => [
            'create' => 'Thêm mới',
            'delete' => 'Xóa',
            'delete-permanently' => 'Xóa vĩnh viễn',
            'restore' => 'Khôi phục',
            'edit' => 'Cập nhật',
            'update' => 'Cập nhật',
            'view' => 'Xem',
        ],

        'yes' => 'Đồng ý',
        'save' => 'Lưu',
        'view' => 'Xem',
    ],
];
