<?php

return [
    'content_type' => [
        'main' => ['id' => 1, 'name' => 'Nội dung chính'],
        'info_tech' => ['id' => 2, 'name' => 'Thông tin kỹ thuật'],
    ]
];