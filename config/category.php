<?php

return [
  'type' => [
      'blog' => [
          'id' => 1,
          'name' => 'Blog',
          'name_en' => 'Blog'
      ],
      'product' => [
          'id' => 2,
          'name' => 'Sản phẩm',
          'name_en' => 'Product',
      ],
      'department' => [
          'id' => 3,
          'name' => 'Phòng ban',
          'name_en' => 'Department',
      ],
  ]
];
