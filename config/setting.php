<?php
return [
    'menu_sidebar' => [
        'home' => [
            'key' => 'home',
            'text' => 'Trang chủ',
            'link' => 'frontend.v1.index',
            'icon' => 'fa fa-home'
        ],
        'newsfeed' => [
            'key' => 'newsfeed',
            'text' => 'Bảng tin',
            'link' => 'frontend.v1.newsfeed',
            'icon' => 'fa fa-newspaper'
        ],
        'blog' => [
            'key' => 'blog',
            'text' => 'Blog',
            'link' => 'frontend.v1.blog.index',
            'icon' => 'fa fa-blog'
        ],
//        'tag' => [
//            'key' => 'tag',
//            'text' => 'Khám phá',
//            'link' => 'frontend.v1.index',
//            'icon' => 'fa fa-hashtag'
//        ],
        'notification' => [
            'key' => 'notification',
            'text' => 'Thông báo',
            'link' => 'frontend.v1.notification.index',
            'icon' => 'fa fa-bell'
        ],
//        'message' => [
//            'key' => 'envelope',
//            'text' => 'Tin nhắn',
//            'link' => 'frontend.v1.index',
//            'icon' => 'fa fa-envelope'
//        ],
        'bookmark' => [
            'key' => 'bookmark',
            'text' => 'Đã lưu',
            'link' => 'frontend.v1.bookmark.index',
            'icon' => 'fa fa-bookmark'
        ],
        'setting' => [
            'key' => 'setting',
            'text' => 'Thêm',
            'link' => 'frontend.v1.index',
            'icon' => 'fa fa-ellipsis-h'
        ],
    ]
];
