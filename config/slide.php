<?php
return [
    'position' => [
        'header' => ['id' => 'header', 'name_en' =>'Header','name' => 'Trang chủ'],
        'footer' => ['id' => 'footer', 'name_en' =>'Footer','name' => 'Trang dưới'],
        'left' => ['id' => 'left', 'name_en' =>'Left','name' => 'Phải'],
        'right' => ['id' => 'right', 'name_en' =>'Right','name' => 'Trái'],
    ]
];
