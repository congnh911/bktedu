<?php

//Phục vụ cho việc dùng ảnh quảng cáo

namespace App\Core\ImageTemplate;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class AdsNormal implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(600, 600);
    }
}