<?php

namespace App\Providers;

use App\Models\Auth\User;
use App\Models\Categories\Category;
use App\Models\Content;
use App\Models\Product\Product;
use App\Models\Blog\Blog;
use App\Models\Image;
use App\Models\Video;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

/**
 * Class RouteServiceProvider.
 */
class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     */
    public function boot()
    {
        // Register route model bindings
        parent::boot();

        // Allow this to select all users regardless of status
        $this->bind('user', function ($value) {
            $user = new User;

            return User::withTrashed()->where($user->getRouteKeyName(), $value)->first();
        });

        $this->bind('category', function ($value) {
            $category = new Category;

            return Category::withTrashed()->where($category->getRouteKeyName(), $value)->first();
        });

        $this->bind('album', function ($value) {
            $album = new Image;

            return Image::where($album->getRouteKeyName(), $value)->first();
        });

        $this->bind('video', function ($value) {
            $video = new Video;

            return Video::where($video->getRouteKeyName(), $value)->first();
        });

        $this->bind('content', function ($value) {
            $content = new Content;

            return Content::where($content->getRouteKeyName(), $value)->first();
        });

        $this->bind('product', function ($value) {
            $product = new Product;

            return Product::where($product->getRouteKeyName(), $value)->first();
        });

        $this->bind('blog', function ($value) {
            $blog = new Blog;

            return Blog::where($blog->getRouteKeyName(), $value)->first();
        });
    }

    /**
     * Define the routes for the application.
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
