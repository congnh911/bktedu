<?php

namespace App\Models;

use App\Core\MyStorage;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    protected $table = 'files';

    protected $guarded = ['id'];

    public function getImage(){
        if($this->disk != '' && $this->path != ''){
            return MyStorage::getThumbLinkAttribute($this->disk, $this->path, 'superlarge');
        } else {
            return $this->avatar_refer_link;
        }
    }
}
