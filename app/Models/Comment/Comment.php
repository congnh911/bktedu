<?php

namespace App\Models\Comment;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $guarded = ['id'];

    public function creator(){
        return $this->belongsTo(User::class,'creator_id');
    }
}
