<?php

namespace App\Models;

use App\Core\MyStorage;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $guarded = ['id'];

    public function avatar($temp = 'normal')
    {
        if (!is_null($this->path)) {
            return MyStorage::getThumbLinkAttribute($this->disk, $this->path, $temp);
        } else {
            return $this->showDefaultAvatar($temp);
        }
    }

    public function gallery($temp = 'normal')
    {
        return MyStorage::getThumbLinkAttribute($this->disk, $this->path, $temp);
    }

    public function showDefaultAvatar($temp = "medium_category")
    {
        return MyStorage::get_default_image($temp, 'user.png');
    }
}
