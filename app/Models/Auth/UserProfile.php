<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profiles';

    protected $guarded = ['id'];
}
