<?php

namespace App\Models\Auth;

use OwenIt\Auditing\Auditable;
use App\Models\Auth\Traits\Method\PermissionMethod;
use Spatie\Permission\Models\Permission as SpatiePermission;
use App\Models\Auth\Traits\Attribute\PermissionAttribute;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class Permission.
 */
class Permission extends SpatiePermission implements AuditableContract
{
    use Auditable,
        PermissionAttribute,
        PermissionMethod;

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        'id',
    ];

    public function user(){
        return $this->belongsTo(User::class,'creator_id');
    }
}
