<?php

namespace App\Models\Auth;

use App\Core\MyStorage;
use App\Models\Auth\Traits\Scope\UserScope;
use App\Models\Auth\Traits\Method\UserMethod;
use App\Models\Auth\Traits\Attribute\UserAttribute;
use App\Models\Auth\Traits\Relationship\UserRelationship;
use Illuminate\Notifications\Notifiable;

/**
 * Class User.
 */
class User extends BaseUser
{
    use UserAttribute,
        UserMethod,
        UserRelationship,
        Notifiable,
        UserScope;

    public function link(){
        return route('frontend.v1.user.account');
    }

    public function routeNotificationForMail()
    {
        return $this->email;
    }

    public function fullName(){
        return $this->first_name. ' '. $this->last_name;
    }

    public function avatar($temp = 'normal')
    {
        if (!is_null($this->path)) {
            return MyStorage::getThumbLinkAttribute($this->disk, $this->path, $temp);
        } else {
            return $this->picture;
        }
    }
    public function showDefaultAvatar($temp = "medium_category")
    {
        return MyStorage::get_default_image($temp, 'user.png');
    }
}
