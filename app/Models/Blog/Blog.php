<?php

namespace App\Models\Blog;

use App\Core\MyStorage;
use App\Models\Auth\User;
use App\Models\Categories\Category;
use App\Models\Content;
use App\Models\Blog\Traits\Attribute\BlogAttribute;
use App\Models\Blog\Traits\Scope\BlogScope;
use App\Models\File;
use App\Models\Image;
use App\Models\Video;
use Baum\Extensions\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Blog extends Model implements AuditableInterface
{
    use Auditable;
    use BlogScope;
    use BlogAttribute;
    use Notifiable;
    use SoftDeletes;

    protected $table = 'blogs';

    protected $dates = ['deleted_at'];

    public $timestamps = true;

    protected $auditExclude = ['id'];

    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function link()
    {
        return route('frontend.v1.blog.detail', [$this, 'slug' => $this->slug]);
    }

    /**
     * Lấy trạng thái của Cat là đang active hoặc không
     * @return bool
     */

    public function isActive()
    {
        return boolval($this->active);
    }

    public function avatar($temp = 'normal')
    {
        if ($this->avatar != null && $this->disk != null) {
            return MyStorage::getThumbLinkAttribute($this->disk, $this->avatar, $temp);
        } elseif ($this->disk == null && $this->avatar != null) {
            return $this->avatar;
        } else {
            return $this->showDefaultAvatar($temp);
        }
    }

    public function showDefaultAvatar($temp = "medium_category")
    {
        return MyStorage::get_default_image($temp, 'user.png');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'model_id')
            ->where('model_type', self::class);
    }

    public function videos()
    {
        return $this->hasMany(Video::class, 'model_id')
            ->where('model_type', self::class);
    }

    public function albums()
    {
        return $this->hasMany(Image::class, 'model_id')
            ->where('model_type', self::class)->orderBy('order','asc');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function blogCategories()
    {
        return $this->belongsToMany(Category::class, 'blog_categories', 'category_id', 'blog_id');
    }

    public function contents()
    {
        return $this->hasMany(Content::class, 'model_id')->where('model_type', self::getMorphClass());
    }

    //noi dung chinh
    public function contentMain()
    {
        return $this->hasOne(Content::class, 'model_id')
            ->where('type', config('blog.content_type.main.id'))
            ->where('model_type', self::getMorphClass());
    }

    public function userPermission()
    {
        return $this->belongsToMany(User::class, 'blog_user_permissions', 'user_id', 'blog_id');
    }
}
