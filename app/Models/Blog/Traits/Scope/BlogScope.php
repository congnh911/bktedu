<?php

namespace App\Models\Blog\Traits\Scope;

/**
 * Class UserScope
 * @package App\Models\Blog\Traits\Scope
 */
trait BlogScope
{
	/**
	 * @param $query
	 * @param bool $status
	 * @return mixed
	 */
	public function scopeActive($query, $status = true) {
		return $query->where('active', $status);
	}
}
