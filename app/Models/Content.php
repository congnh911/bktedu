<?php

namespace App\Models;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'contents';

    protected $guarded = ['id'];

    public function creator(){
        return $this->belongsTo(User::class,'creator_id');
    }
}
