<?php

namespace App\Models\Categories\Traits\Scope;

/**
 * Class UserScope
 * @package App\Models\Categories\Traits\Scope
 */
trait CategoriesScope
{
	/**
	 * @param $query
	 * @param bool $status
	 * @return mixed
	 */
	public function scopeActive($query, $status = true) {
		return $query->where('active', $status);
	}
}