<?php

namespace App\Models\Categories\Traits\Attribute;

/**
 * Class CategoriesAttribute
 * @package App\Models\Access\Categories\Traits\Attribute
 */
trait CategoriesAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive())
            return "<label class='label label-success'>" . trans('labels.general.active') . "</label>";
        return "<label class='label label-danger'>" . trans('labels.general.inactive') . "</label>";
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active == 1;
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.category.show', $this) . '" data-toggle="modal" data-target="#modal-md" 
        class="btn btn-sm btn-light modal_action"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" 
        title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.category.edit', [$this, 'type' => $this->type]) . '"
         class="btn btn-sm btn-light">
        <i class="fa fa-edit" data-toggle="tooltip" data-placement="top" 
        title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a href="javascript:void(0)" data-href="' . route('admin.category.destroy', $this) . '"
             data-method="delete"
             data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
             data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
             data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
             class="btn btn-sm btn-light action-confirm">
             <i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';

    }

    /**
     * @return string
     */
    public function getActiveButtonAttribute()
    {
        if (!$this->isActive()) {
            return '<a href="' . route('admin.category.account.confirm.resend', $this) . '" class="btn btn-sm btn-success"><i class="fa fa-refresh" data-toggle="tooltip" data-placement="top" title=' . trans('buttons.backend.categories.resend_email') . '"></i></a> ';
        }

        return '';
    }

    public function getSwitchActiveAttribute()
    {
        $checked = $this->active == 1 ? 'checked' : null;
        $html = '<label class="switch switch-3d switch-success">
                    <input class="switch-input" type="checkbox" name="active" 
                        data-method="patch"
                        data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                        data-trans-button-confirm="' . trans('buttons.general.yes') . '"
                        data-trans-title="' . trans('strings.backend.general.are_you_sure') . '" 
                        data-url="' . route('admin.category.switch01', $this) . '" ' . $checked . '>
                    <span class="switch-slider"></span>
                </label>';

        return $html;
    }

    public function getSwitchHighlightAttribute()
    {
        $checked = $this->highlight == 1 ? 'checked' : null;
        $html = '<label class="switch switch-3d switch-success">
                    <input class="switch-input" type="checkbox" name="highlight"
                        data-method="patch"
                        data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                        data-trans-button-confirm="' . trans('buttons.general.yes') . '"
                        data-trans-title="' . trans('strings.backend.general.are_you_sure') . '" 
                        data-url="' . route('admin.category.switch01', $this) . '" ' . $checked . '>
                    <span class="switch-slider"></span>
                </label>';

        return $html;
    }

    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        switch ($this->active) {
            case 0:
                return '<a href="' . route('admin.category.mark', [
                        $this,
                        1
                    ]) . '" class="btn btn-sm btn-success"><i class="fa fa-play" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.categories.activate') . '"></i></a> ';
            // No break

            case 1:
                return '<a href="' . route('admin.category.mark', [
                        $this,
                        0
                    ]) . '" class="btn btn-sm btn-warning"><i class="fa fa-pause" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.categories.deactivate') . '"></i></a> ';
            // No break

            default:
                return '';
            // No break
        }
    }


    /**
     * @return string
     */
    public function getRestoreButtonAttribute()
    {
        return '<a href="javascript:void(0)" 
             data-href="' . route('admin.category.restore', $this) . '"
             data-method="post"
             data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
             data-trans-button-confirm="' . trans('buttons.general.crud.restore') . '"
             data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
             class="btn btn-sm btn-light action-confirm">
        <i class="fa fa-recycle" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.categories.restore') . '"></i>
        </a> ';
    }

    /**
     * @return string
     */
    public function getDeletePermanentlyButtonAttribute()
    {
        return '<a href="javascript:void(0)" data-href="' . route('admin.category.delete-permanently', $this) . '"
             data-method="delete"
             data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
             data-trans-button-confirm="' . trans('buttons.general.crud.delete-permanently') . '"
             data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
             class="btn btn-sm btn-light action-confirm" 
             data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.categories.restore') . '">
             <i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.backend.categories.delete_permanently') . '"></i></a> ';
    }


    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        if ($this->trashed()) {
            return
                $this->getRestoreButtonAttribute() .
                $this->getDeletePermanentlyButtonAttribute();
        }

        return
			$this->getShowButtonAttribute() .
            $this->getEditButtonAttribute() .
//            $this->getStatusButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }
}
