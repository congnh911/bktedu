<?php

namespace App\Models\Categories;

use App\Core\MyStorage;
use App\Models\Auth\User;
use App\Models\Categories\Traits\Attribute\CategoriesAttribute;
use App\Models\Categories\Traits\Scope\CategoriesScope;
use App\Models\File;
use App\Models\Image;
use Baum\Node;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Category extends Node implements AuditableInterface
{
    use Auditable;
    use CategoriesScope;
    use CategoriesAttribute;
    use Notifiable;
    use SoftDeletes;

    protected $table = 'categories';

    protected $dates = ['deleted_at'];

    const LIST_INDENT = " === ";

    public $timestamps = true;

    protected $auditExclude = ['id', 'lft', 'rgt', 'depth'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function linkBlog(){
        return route('frontend.v1.blog_category.category',[$this,$this->slug]);
    }

    public static function getNestedListType($column, $key = null, $seperator = self::LIST_INDENT, $type = null)
    {
        $instance = new static;

        $key = $key ?: $instance->getKeyName();
        $depthColumn = $instance->getDepthColumnName();

        $nodes = $instance->newNestedSetQuery()->where('type', $type)->get()->toArray();

        return array_combine(array_map(function ($node) use ($key) {
            return $node[$key];
        }, $nodes), array_map(function ($node) use ($seperator, $depthColumn, $column) {
            return str_repeat($seperator, $node[$depthColumn]) . $node[$column];
        }, $nodes));
    }

    public function ancestorsCategories()
    {
        return $this->ancestors()->get(['name', 'name_en', 'id']);
    }

    public function siblingsCategories()
    {
        return $this->siblings()->get(['name', 'name_en', 'id']);
    }

    /**
     * @return array
     * select mang categories ma khong co chinh no
     */
    public static function categoryNotSefl($type)
    {
        $name = \App::isLocale('en') ? 'name_en' : 'name';
        return $data = ['' => trans('common.root')]
            + Category::getNestedListType($name, 'id', self::LIST_INDENT, $type);

    }

    /**
     * Lấy trạng thái của Cat là đang active hoặc không
     * @return bool
     */
    public function isActive()
    {
        return boolval($this->active);
    }

    public function avatar($temp = 'normal')
    {
        if (!is_null($this->path)) {
            return MyStorage::getThumbLinkAttribute($this->disk, $this->path, $temp);
        } else {
            return $this->showDefaultAvatar($temp);
        }
    }

    public function showDefaultAvatar($temp = "medium_category")
    {
        return MyStorage::get_default_image($temp, 'user.png');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'model_id')
            ->where('model_type', self::class);
    }

    public function albums()
    {
        return $this->hasMany(Image::class, 'model_id')
            ->where('model_type', self::class)->orderBy('order','asc');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }
}
