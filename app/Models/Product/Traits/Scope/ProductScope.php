<?php

namespace App\Models\Product\Traits\Scope;

/**
 * Class UserScope
 * @package App\Models\Product\Traits\Scope
 */
trait ProductScope
{
	/**
	 * @param $query
	 * @param bool $status
	 * @return mixed
	 */
	public function scopeActive($query, $status = true) {
		return $query->where('active', $status);
	}
}
