<?php
/**
 * Created by PhpStorm.
 * User: congnh
 * Date: 14/08/2019
 * Time: 16:28
 */

namespace App\Models\Product;


use App\Models\Categories\Category;
use Baum\Extensions\Eloquent\Model;

class ProductCategories extends Model
{
    protected $table = 'product_categories';

    protected $guarded = ['id'];

    protected $timestamps = true;

    public function category(){
        return $this->hasOne(Category::class,'category_id');
    }
}
