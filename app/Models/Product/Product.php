<?php

namespace App\Models\Product;

use App\Core\MyStorage;
use App\Models\Auth\User;
use App\Models\Categories\Category;
use App\Models\Content;
use App\Models\Product\Traits\Attribute\ProductAttribute;
use App\Models\Product\Traits\Scope\ProductScope;
use App\Models\File;
use App\Models\Image;
use Baum\Extensions\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

class Product extends Model implements AuditableInterface
{
    use Auditable;
    use ProductScope;
    use ProductAttribute;
    use Notifiable;
    use SoftDeletes;

    protected $table = 'products';

    protected $dates = ['deleted_at'];

    public $timestamps = true;

    protected $auditExclude = ['id'];

    protected $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }


    /**
     * Lấy trạng thái của Cat là đang active hoặc không
     * @return bool
     */
    public function isActive()
    {
        return boolval($this->active);
    }

    public function avatar($temp = 'normal')
    {
        if (!is_null($this->path)) {
            return MyStorage::getThumbLinkAttribute($this->disk, $this->path, $temp);
        } else {
            return $this->showDefaultAvatar($temp);
        }
    }

    public function showDefaultAvatar($temp = "medium_category")
    {
        return MyStorage::get_default_image($temp, 'user.png');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'model_id')
            ->where('model_type', self::class);
    }

    public function albums()
    {
        return $this->hasMany(Image::class, 'model_id')
            ->where('model_type', self::class)->orderBy('order','asc');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'creator_id');
    }
    public function productCategoryMain(){
        return $this->belongsTo(Category::class,'category_id');
    }

    public function productCategories(){
        return $this->belongsToMany(Category::class,'product_categories','category_id','product_id');
    }

    public function contents(){
        return $this->hasMany(Content::class,'model_id')->where('model_type',self::getMorphClass());
    }
}
