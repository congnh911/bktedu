<?php

namespace App\Models;

use App\Models\Auth\User;
use App\Models\Traits\Attribute\SlideAttribute;
use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    use SlideAttribute;

    protected $table = 'slides';

    protected $guarded = ['id'];

    public function albums()
    {
        return $this->hasMany(Image::class, 'model_id')
            ->where('model_type', self::class)->orderBy('order','asc');
    }

    public function creator(){
        return $this->belongsTo(User::class,'creator_id');
    }
}
