<?php

namespace App\Models\Traits\Attribute;

/**
 * Class SlideAttribute
 * @package App\Models\Access\Traits\Attribute
 */
trait SlideAttribute
{
    /**
     * @return string
     */
    public function getStatusLabelAttribute()
    {
        if ($this->isActive())
            return "<label class='label label-success'>" . trans('labels.general.active') . "</label>";
        return "<label class='label label-danger'>" . trans('labels.general.inactive') . "</label>";
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active == 1;
    }

    /**
     * @return string
     */
    public function getShowButtonAttribute()
    {
        return '<a href="' . route('admin.slide.show', $this) . '" data-toggle="modal" data-target="#modal-md" 
        class="btn btn-sm btn-light modal_action"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" 
        title="' . trans('buttons.general.crud.view') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        return '<a href="' . route('admin.slide.edit', [$this, 'type' => $this->type]) . '"
         class="btn btn-sm btn-light">
        <i class="fa fa-edit" data-toggle="tooltip" data-placement="top" 
        title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
    }

    /**
     * @return string
     */
    public function getDeleteButtonAttribute()
    {
        return '<a href="javascript:void(0)" data-href="' . route('admin.slide.destroy', $this) . '"
             data-method="delete"
             data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
             data-trans-button-confirm="' . trans('buttons.general.crud.delete') . '"
             data-trans-title="' . trans('strings.backend.general.are_you_sure') . '"
             class="btn btn-sm btn-light action-confirm">
             <i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';

    }

    public function getSwitchActiveAttribute()
    {
        $checked = $this->active == 1 ? 'checked' : null;
        $html = '<label class="switch switch-3d switch-success">
                    <input class="switch-input" type="checkbox" name="active" 
                        data-method="patch"
                        data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                        data-trans-button-confirm="' . trans('buttons.general.yes') . '"
                        data-trans-title="' . trans('strings.backend.general.are_you_sure') . '" 
                        data-url="' . route('admin.slide.switch01', $this) . '" ' . $checked . '>
                    <span class="switch-slider"></span>
                </label>';

        return $html;
    }

    public function getSwitchHighlightAttribute()
    {
        $checked = $this->highlight == 1 ? 'checked' : null;
        $html = '<label class="switch switch-3d switch-success">
                    <input class="switch-input" type="checkbox" name="highlight"
                        data-method="patch"
                        data-trans-button-cancel="' . trans('buttons.general.cancel') . '"
                        data-trans-button-confirm="' . trans('buttons.general.yes') . '"
                        data-trans-title="' . trans('strings.backend.general.are_you_sure') . '" 
                        data-url="' . route('admin.slide.switch01', $this) . '" ' . $checked . '>
                    <span class="switch-slider"></span>
                </label>';

        return $html;
    }


    /**
     * @return string
     */
    public function getActionButtonsAttribute()
    {
        return
			$this->getShowButtonAttribute() .
            $this->getEditButtonAttribute() .
//            $this->getStatusButtonAttribute() .
            $this->getDeleteButtonAttribute();
    }
}
