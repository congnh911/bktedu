<?php

namespace App\Events\Backend\Blog;

use Illuminate\Queue\SerializesModels;

class BlogRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $product;

    /**
     * @param $product
     */
    public function __construct($product)
    {
        $this->product = $product;
    }
}
