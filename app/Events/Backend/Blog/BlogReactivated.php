<?php

namespace App\Events\Backend\Blog;

use Illuminate\Queue\SerializesModels;

class BlogReactivated
{
    use SerializesModels;

    /**
     * @var
     */
    public $product;

    /**
     * @param $product
     */
    public function __construct($product)
    {
        $this->product = $product;
    }
}
