<?php

namespace App\Events\Backend\Auth\Permission;

use Illuminate\Queue\SerializesModels;

/**
 * Class RoleDeleted.
 */
class PermissionDeleted
{
    use SerializesModels;

    /**
     * @var
     */
    public $permission;

    /**
     * @param $permission
     */
    public function __construct($permission)
    {
        $this->permission = $permission;
    }
}
