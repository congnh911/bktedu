<?php

namespace App\Events\Backend\Product;

use Illuminate\Queue\SerializesModels;

class ProductRestored
{
    use SerializesModels;

    /**
     * @var
     */
    public $product;

    /**
     * @param $product
     */
    public function __construct($product)
    {
        $this->product = $product;
    }
}
