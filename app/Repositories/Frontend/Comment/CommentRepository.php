<?php

namespace App\Repositories\Frontend\Comment;

use App\Models\Comment\Comment;
use App\Repositories\BaseRepository;

/**
 * Class CommentRepository.
 */
class CommentRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Comment::class;
    }

    public function getDataWithCondition($filter, array $order = [],$pageSize = 10,$columns = ['*'])
    {
        $model = $this->filter($filter);
        if (!empty($order)) {
            foreach ($order as $key => $value) {
                $model->orderBy($key, $value);
            }
        }

        if(!empty($filter['deleted_at'])){
            $model->withTrashed();
        }

        $model =  $model->orderBy('created_at', 'asc')->select($columns)->paginate($pageSize);


        return $model;
    }

    public function filter($filter){
        $model = $this->model;
        if(!empty($filter)) {
            $model = addFilter($model, $filter);
        }
        return $model;
    }

}
