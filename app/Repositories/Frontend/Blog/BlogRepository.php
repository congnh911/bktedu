<?php

namespace App\Repositories\Frontend\Blog;

use App\Core\Uploader;
use App\Events\Backend\Blog\SlideCreated;
use App\Events\Backend\Blog\BlogDeleted;
use App\Events\Backend\Blog\BlogPermanentlyDeleted;
use App\Events\Backend\Blog\BlogRestored;
use App\Events\Backend\Blog\BlogUpdated;
use App\Models\Blog\Blog;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class BlogRepository.
 */
class BlogRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Blog::class;
    }

    public function getData($params){

        if($params['user_id']){
            $this->model->where('creator_id',$params['user_id']);
        }

        return $this->model->paginate();
    }
}
