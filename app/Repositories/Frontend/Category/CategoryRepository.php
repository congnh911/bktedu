<?php

namespace App\Repositories\Frontend\Category;

use App\Models\Categories\Category;
use App\Repositories\BaseRepository;

/**
 * Class CategoryCategoryRepository.
 */
class CategoryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Category::class;
    }


}
