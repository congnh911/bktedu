<?php

namespace App\Repositories\Frontend\Product;

use App\Core\Uploader;
use App\Events\Backend\Product\ProductCreated;
use App\Events\Backend\Product\ProductDeleted;
use App\Events\Backend\Product\ProductPermanentlyDeleted;
use App\Events\Backend\Product\ProductRestored;
use App\Events\Backend\Product\ProductUpdated;
use App\Models\Product\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class ProductRepository.
 */
class ProductRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Product::class;
    }


}
