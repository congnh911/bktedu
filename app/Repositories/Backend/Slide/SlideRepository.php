<?php

namespace App\Repositories\Backend\Slide;

use App\Core\Uploader;
use App\Models\Auth\User;
use App\Models\Slide;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class SlideRepository.
 */
class SlideRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Slide::class;
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'asc'): LengthAwarePaginator
    {
        return $this->model->orderBy($orderBy, $sort)->paginate($paged);
    }

    public function getActiveAll()
    {
        return $this->model->get();
    }


    public function create(array $data): Slide
    {

        $checkSlide = $this->model->where('position',$data['position'])->count();

        if($checkSlide > 0){
            throw new GeneralException('Đã tồn tại bản ghi này vui lòng cập nhật và kích hoạt để sử dụng');
        }

        return DB::transaction(function () use ($data) {
            $name = !empty($data['name']) ? $data['name'] : '';
            $description = !empty($data['description']) ? $data['description'] : '';
            $type = !empty($data['type']) ? $data['type'] : 0;
            $highlight = !empty($data['highlight']) ? 1 : 0;
            $active = !empty($data['active']) ? 1 : 0;
            $postition = !empty($data['position']) ? $data['position'] : '';
            $creator_id = !empty($data['creator_id']) ? $data['creator_id'] : auth()->user()->id;

            $slide = parent::create([
                'name' => $name,
                'type' => $type,
                'description' => $description,
                'position' => $postition,
                'active' => $active,
                'highlight' => $highlight,
                'creator_id' => $creator_id,
            ]);

            if ($slide) {
                $this->model = $slide;
                return $slide;
            }

            throw new GeneralException('Có lỗi xảy ra khi thêm mới!');
        });
    }

    public function update(Slide $slide, array $data): Slide
    {

        return DB::transaction(function () use ($slide, $data) {
            $this->model = $slide;

            array_forget($data, config('common.input_forget'));

            if ($slide->update($data)) {
                return $slide;
            }

            throw new GeneralException('Có lỗi xảy ra khi cập nhật!');
        });
    }


    public function getDataTable($filter, $option = [])
    {
        $model = $this->model;

        if (!empty($filter)) {

            if (!empty($filter['slides.trashed'])) {
                $model = $model->onlyTrashed();
                array_forget($filter, 'slides.trashed');
            }
            $model = addFilter($model, $filter);
        }

        if (!empty($option['order'])) {
            foreach ($option['order'] as $key => $value) {
                $model->orderBy($key, $value);
            }
        }

        //Nếu yêu cầu lấy tổng số bản ghi thì trả về tổng luôn
        if(isset($option['countTotal']) && $option['countTotal']){
            return $model->count();
        }

        if(isset($option['start']) && isset($option['length'])){
            $model = $model->skip((int)$option['start'])->take((int)$option['length']);
        }

        return $model->orderBy($this->model->getTable() . '.created_at', 'asc')->get();
    }


    protected function checkUpdateSlideByLink(Slide $slide, $link)
    {
        if ($slide->link !== $link && $this->model->where('link', '=', $link)->first()) {
            throw new GeneralException('Đường link: ' . $link . ' đã tồn tại');
        }
    }

    protected function calSlideDiscount($listed_price = 0, $actual_price = 0)
    {
        if ($listed_price > $actual_price) {
            $discount = round(($actual_price / $listed_price) * 100);
            $discount = 100 - $discount;
        } else {
            $discount = 0;
        }
        return $discount;
    }

    public function destroy(Slide $slide): Slide
    {

        return DB::transaction(function () use ($slide) {
            if ($slide->forceDelete()) {
                $slide->albums()->delete();
                return $slide;
            }

            throw new GeneralException('Có lỗi xảy ra xóa vĩnh  Danh mục!');
        });
    }


}
