<?php

namespace App\Repositories\Backend;

use App\Core\Uploader;
use App\Events\Backend\Category\CategoryCreated;
use App\Events\Backend\Category\CategoryDeleted;
use App\Events\Backend\Category\CategoryPermanentlyDeleted;
use App\Events\Backend\Category\CategoryRestored;
use App\Events\Backend\Category\CategoryUpdated;
use App\Models\Categories\Category;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class CategoryCategoryRepository.
 */
class CategoryRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'asc'): LengthAwarePaginator
    {
        return $this->model->orderBy($orderBy, $sort)->paginate($paged);
    }

    public function getActiveAll()
    {
        return $this->model->where('active',1)->get();
    }

    public function create(array $data): Category
    {

        return DB::transaction(function () use ($data) {
            $name = !empty($data['name']) ? $data['name'] : '';
            $name_en = !empty($data['name_en']) ? $data['name_en'] : '';
            $slug = !empty($data['slug']) ? $data['slug'] : '';
            $avatar = !empty($data['avatar']) ? $data['avatar'] : '';
            $parent_id = !empty($data['parent_id']) && $data['parent_id'] != '' ? (int)$data['parent_id'] : null;
            $description = !empty($data['description']) ? $data['description'] : '';
            $description_en = !empty($data['description_en']) ? $data['description_en'] : '';
            $seo_title = !empty($data['seo_title']) ? $data['seo_title'] : '';
            $seo_keyword = !empty($data['seo_keyword']) ? $data['seo_keyword'] : '';
            $seo_description = !empty($data['seo_description']) ? $data['seo_description'] : '';
            $type = !empty($data['type']) ? $data['type'] : 0;
            $order = !empty($data['order']) ? $data['order'] : 0;
            $highlight = !empty($data['highlight']) ? 1 : 0;
            $active = !empty($data['active']) ? 1 : 0;

            $category = parent::create([
                'name' => $name,
                'name_en' => $name_en,
                'slug' => $slug,
                'type' => $type,
                'parent_id' => $parent_id,
                'description' => $description,
                'description_en' => $description_en,
                'seo_title' => $seo_title,
                'seo_keyword' => $seo_keyword,
                'seo_description' => $seo_description,
                'active' => $active,
                'order' => $order,
                'highlight' => $highlight,
                'creator_id' => auth()->user()->id,
            ]);

            Category::where('id', $category->id)->update(['parent_id' => $parent_id]);
            Category::rebuild();

            if ($category) {
                $this->model = $category;
                $this->uploadAvatar($avatar);
                event(new CategoryCreated($category));
                return $category;
            }

            throw new GeneralException('Có lỗi xảy ra khi thêm mới Danh mục!');
        });
    }

    public function update(Category $category, array $data): Category
    {

        return DB::transaction(function () use ($category, $data) {
            $this->model = $category;

            $avatar = !empty($data['avatar']) ? $data['avatar'] : '';
            array_forget($data,['avatar','files','_token','_method','image_title']);
            Category::where('id', $category->id)->update(['parent_id' => $data['parent_id']]);
            Category::rebuild();

            if ($category->update($data)) {
                $this->uploadAvatar($avatar);
                event(new CategoryUpdated($category));
                return $category;
            }

            throw new GeneralException('Có lỗi xảy ra khi cập nhật Danh mục!');
        });
    }

    public function destroy(Category $category): Category
    {

        return DB::transaction(function () use ($category) {
            if ($category->update([
                'deleted_at' => Carbon::now(),
            ])) {
                event(new CategoryDeleted($category));
                return $category;
            }

            throw new GeneralException('Có lỗi xảy ra xóa nhật Danh mục!');
        });
    }

    public function restore(Category $category): Category
    {

        return DB::transaction(function () use ($category) {
            if ($category->update([
                'deleted_at' => null,
            ])) {
                event(new CategoryRestored($category));
                return $category;
            }

            throw new GeneralException('Có lỗi xảy ra khôi phục Danh mục!');
        });
    }

    public function permanentlyDelete(Category $category): Category
    {

        return DB::transaction(function () use ($category) {
            if ($category->forceDelete()) {
                event(new CategoryPermanentlyDeleted($category));
                return $category;
            }

            throw new GeneralException('Có lỗi xảy ra xóa vĩnh  Danh mục!');
        });
    }

    public function uploadAvatar($avatar)
    {
        if ($avatar != '') {
            $file = $avatar;
            $disk = 'local';
            $dir_path = 'categories/' . Carbon::now()->year . '/' . Carbon::now()->month . '/' . Carbon::now()->day;
            try {
                $file_name = Uploader::upload($disk, $file, $dir_path);
                if ($file_name != "") {

                    //neu ton tai file cu thi xoa di
                    if ($this->model->path != '') {
                        Uploader::delete($this->model->disk, $this->model->path);
                    }

                    $this->model->disk = $disk;
                    $this->model->path = $file_name;
                    $this->model->save();
                }
            } catch (\Exception $exception) {
                \Log::error($exception);
            }
        }
    }


    public function getDataTable($filter, $option = [])
    {
        $model = $this->model;

        if (!empty($filter)) {

            if (!empty($filter['categories.trashed'])) {
                $model = $model->onlyTrashed();
                array_forget($filter, 'categories.trashed');
            }
            $model = addFilter($model, $filter);
        }

        if (!empty($option['order'])) {
            foreach ($option['order'] as $key => $value) {
                $model->orderBy($key, $value);
            }
        }

        //Nếu yêu cầu lấy tổng số bản ghi thì trả về tổng luôn
        if(isset($option['countTotal']) && $option['countTotal']){
            return $model->count();
        }

        if(isset($option['start']) && isset($option['length'])){
            $model = $model->skip((int)$option['start'])->take((int)$option['length']);
        }

        return $model->orderBy($this->model->getTable() . '.lft', 'ASC')->get();
    }


}
