<?php

namespace App\Repositories\Backend\Blog;

use App\Core\Uploader;
use App\Events\Backend\Blog\SlideCreated;
use App\Events\Backend\Blog\BlogDeleted;
use App\Events\Backend\Blog\BlogPermanentlyDeleted;
use App\Events\Backend\Blog\BlogRestored;
use App\Events\Backend\Blog\BlogUpdated;
use App\Models\Auth\User;
use App\Models\Blog\Blog;
use App\Models\Notification;
use App\Notifications\Frontend\BlogPostUserPermission;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class BlogRepository.
 */
class BlogRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Blog::class;
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'asc'): LengthAwarePaginator
    {
        return $this->model->orderBy($orderBy, $sort)->paginate($paged);
    }

    public function getActiveAll()
    {
        return $this->model->get();
    }


    public function create(array $data): Blog
    {

        return DB::transaction(function () use ($data) {
            $name = !empty($data['name']) ? $data['name'] : '';
            $name_en = !empty($data['name_en']) ? $data['name_en'] : '';
            $slug = !empty($data['slug']) ? $data['slug'] : '';
            $avatar = !empty($data['avatar']) ? $data['avatar'] : '';
            $description = !empty($data['description']) ? $data['description'] : '';
            $description_en = !empty($data['description_en']) ? $data['description_en'] : '';
            $seo_title = !empty($data['seo_title']) ? $data['seo_title'] : '';
            $seo_keyword = !empty($data['seo_keyword']) ? $data['seo_keyword'] : '';
            $seo_description = !empty($data['seo_description']) ? $data['seo_description'] : '';
            $type = !empty($data['type']) ? $data['type'] : 0;
            $order = !empty($data['order']) ? $data['order'] : 0;
            $highlight = !empty($data['highlight']) ? 1 : 0;
            $active = !empty($data['active']) ? 1 : 0;
            $categories = $data['category_id'];
            $creator_id = !empty($data['creator_id']) ? $data['creator_id'] : auth()->user()->id;

            $blog = parent::create([
                'name' => $name,
                'name_en' => $name_en,
                'slug' => $slug,
                'type' => $type,
                'description' => $description,
                'description_en' => $description_en,
                'seo_title' => $seo_title,
                'seo_keyword' => $seo_keyword,
                'seo_description' => $seo_description,
                'active' => $active,
                'order' => $order,
                'highlight' => $highlight,
                'category_id' => array_first($categories),
                'creator_id' => $creator_id,
                'views' => 0
            ]);

            if ($blog) {

                if (!empty($data['category_id'])) {
                    $blog->blogCategories()->sync($data['category_id']);
                }

                $this->model = $blog;
                $this->uploadAvatar($avatar);
                event(new SlideCreated($blog));
                return $blog;
            }

            throw new GeneralException('Có lỗi xảy ra khi thêm mới Danh mục!');
        });
    }

    public function update(Blog $blog, array $data): Blog
    {

        return DB::transaction(function () use ($blog, $data) {
            $this->model = $blog;
            $categories = $data['category_id'];
            $userPermission = !empty($data['blog_user_permission']) ? $data['blog_user_permission'] : [];//nguoi duoc quyen

            $avatar = !empty($data['avatar']) ? $data['avatar'] : '';
            array_forget($data, config('common.input_forget'));
            if ($blog->update($data)) {

                $blog->userPermission()->sync($userPermission);

                $dataNotify = [
                    'action' => ' đã đăng bài viết mới',
                    'message' => $blog->name,
                    'link' => $blog->link()
                ];

                $users = User::whereIn('id', $userPermission)->get();
                \Notification::send($users, new BlogPostUserPermission($dataNotify));

                if (!empty($categories)) {
                    $blog->blogCategories()->sync($categories);
                    $blog->update(['category_id' => array_first($categories)]);
                }
                $this->uploadAvatar($avatar);
                event(new BlogUpdated($blog));
                return $blog;
            }

            throw new GeneralException('Có lỗi xảy ra khi cập nhật Danh mục!');
        });
    }

    public function destroy(Blog $blog): Blog
    {

        return DB::transaction(function () use ($blog) {
            if ($blog->update([
                'deleted_at' => Carbon::now(),
            ])) {
                event(new BlogDeleted($blog));
                return $blog;
            }

            throw new GeneralException('Có lỗi xảy ra xóa nhật Danh mục!');
        });
    }

    public function restore(Blog $blog): Blog
    {

        return DB::transaction(function () use ($blog) {
            if ($blog->update([
                'deleted_at' => null,
            ])) {
                event(new BlogRestored($blog));
                return $blog;
            }

            throw new GeneralException('Có lỗi xảy ra khôi phục Danh mục!');
        });
    }

    public function permanentlyDelete(Blog $blog): Blog
    {

        return DB::transaction(function () use ($blog) {
            if ($blog->forceDelete()) {
                event(new BlogPermanentlyDeleted($blog));
                return $blog;
            }

            throw new GeneralException('Có lỗi xảy ra xóa vĩnh  Danh mục!');
        });
    }

    public function uploadAvatar($avatar)
    {
        if ($avatar != '') {
            $file = $avatar;
            $disk = 'local';
            $dir_path = 'blogs/' . Carbon::now()->year . '/' . Carbon::now()->month . '/' . Carbon::now()->day;
            try {
                $file_name = Uploader::upload($disk, $file, $dir_path);
                if ($file_name != "") {

                    //neu ton tai file cu thi xoa di
                    if ($this->model->path != '') {
                        Uploader::delete($this->model->disk, $this->model->path);
                    }

                    $this->model->disk = $disk;
                    $this->model->path = $file_name;
                    $this->model->save();
                }
            } catch (\Exception $exception) {
                \Log::error($exception);
            }
        }
    }


    public function getDataTable($filter, $option = [])
    {
        $model = $this->model;

        if (!empty($filter)) {

            if (!empty($filter['blogs.trashed'])) {
                $model = $model->onlyTrashed();
                array_forget($filter, 'blogs.trashed');
            }
            $model = addFilter($model, $filter);
        }

        if (!empty($option['order'])) {
            foreach ($option['order'] as $key => $value) {
                $model->orderBy($key, $value);
            }
        }

        //Nếu yêu cầu lấy tổng số bản ghi thì trả về tổng luôn
        if (isset($option['countTotal']) && $option['countTotal']) {
            return $model->count();
        }

        if (isset($option['start']) && isset($option['length'])) {
            $model = $model->skip((int)$option['start'])->take((int)$option['length']);
        }

        return $model->orderBy($this->model->getTable() . '.created_at', 'desc')->get();
    }


    protected function checkUpdateBlogByLink(Blog $blog, $link)
    {
        if ($blog->link !== $link && $this->model->where('link', '=', $link)->first()) {
            throw new GeneralException('Đường link: ' . $link . ' đã tồn tại');
        }
    }

    protected function checkStoreBlogByLink($link)
    {
        if ($this->model->where('link', '=', $link)->first()) {
            $link = createLinkWithRandomString($link);
        }
        return $link;
    }

    protected function calBlogDiscount($listed_price = 0, $actual_price = 0)
    {
        if ($listed_price > $actual_price) {
            $discount = round(($actual_price / $listed_price) * 100);
            $discount = 100 - $discount;
        } else {
            $discount = 0;
        }
        return $discount;
    }

}
