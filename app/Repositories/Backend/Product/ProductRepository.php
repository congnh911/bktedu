<?php

namespace App\Repositories\Backend\Product;

use App\Core\Uploader;
use App\Events\Backend\Product\ProductCreated;
use App\Events\Backend\Product\ProductDeleted;
use App\Events\Backend\Product\ProductPermanentlyDeleted;
use App\Events\Backend\Product\ProductRestored;
use App\Events\Backend\Product\ProductUpdated;
use App\Models\Product\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\BaseRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class ProductRepository.
 */
class ProductRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    public function getActivePaginated($paged = 25, $orderBy = 'created_at', $sort = 'asc'): LengthAwarePaginator
    {
        return $this->model->orderBy($orderBy, $sort)->paginate($paged);
    }

    public function getActiveAll()
    {
        return $this->model->get();
    }


    public function create(array $data): Product
    {

        return DB::transaction(function () use ($data) {
            $name = !empty($data['name']) ? $data['name'] : '';
            $name_en = !empty($data['name_en']) ? $data['name_en'] : '';
            $slug = !empty($data['slug']) ? $data['slug'] : '';
            $avatar = !empty($data['avatar']) ? $data['avatar'] : '';
            $description = !empty($data['description']) ? $data['description'] : '';
            $description_en = !empty($data['description_en']) ? $data['description_en'] : '';
            $regular_price = !empty($data['regular_price']) ? $data['regular_price'] : 0;
            $sale_price = !empty($data['sale_price']) ? $data['sale_price'] : 0;
            $discount = !empty($data['discount']) ? $data['discount'] : 0;
            $seo_title = !empty($data['seo_title']) ? $data['seo_title'] : '';
            $seo_keyword = !empty($data['seo_keyword']) ? $data['seo_keyword'] : '';
            $seo_description = !empty($data['seo_description']) ? $data['seo_description'] : '';
            $type = !empty($data['type']) ? $data['type'] : 0;
            $order = !empty($data['order']) ? $data['order'] : 0;
            $highlight = !empty($data['highlight']) ? 1 : 0;
            $active = !empty($data['active']) ? 1 : 0;

            $product = parent::create([
                'name' => $name,
                'name_en' => $name_en,
                'slug' => $slug,
                'type' => $type,
                'description' => $description,
                'description_en' => $description_en,
                'regular_price' => cleanSpecial($regular_price),
                'sale_price' => cleanSpecial($sale_price),
                'discount' => cleanSpecial($discount),
                'seo_title' => $seo_title,
                'seo_keyword' => $seo_keyword,
                'seo_description' => $seo_description,
                'active' => $active,
                'order' => $order,
                'highlight' => $highlight,
                'creator_id' => auth()->user()->id,
            ]);

            if ($product) {

                if (!empty($data['category_id'])) {
                    $product->productCategories()->sync($data['category_id']);
                }

                $this->model = $product;
                $this->uploadAvatar($avatar);
                event(new ProductCreated($product));
                return $product;
            }

            throw new GeneralException('Có lỗi xảy ra khi thêm mới Danh mục!');
        });
    }

    public function update(Product $product, array $data): Product
    {

        return DB::transaction(function () use ($product, $data) {
            $this->model = $product;
            $categories = $data['category_id'];

            $avatar = !empty($data['avatar']) ? $data['avatar'] : '';
            $regular_price = !empty($data['regular_price']) ? $data['regular_price'] : 0;
            $sale_price = !empty($data['sale_price']) ? $data['sale_price'] : 0;
            $discount = !empty($data['discount']) ? $data['discount'] : 0;
            $data['regular_price'] = (float)(cleanSpecial($regular_price));
            $data['sale_price'] = (float)cleanSpecial($sale_price);
            $data['discount'] = (float)cleanSpecial($discount);

            array_forget($data, config('common.input_forget'));

            if ($product->update($data)) {

                if (!empty($categories)) {
                    $product->productCategories()->sync($categories);
                    $product->update(['category_id' => array_first($categories)]);
                }
                $this->uploadAvatar($avatar);
                event(new ProductUpdated($product));
                return $product;
            }

            throw new GeneralException('Có lỗi xảy ra khi cập nhật Danh mục!');
        });
    }

    public function destroy(Product $product): Product
    {

        return DB::transaction(function () use ($product) {
            if ($product->update([
                'deleted_at' => Carbon::now(),
            ])) {
                event(new ProductDeleted($product));
                return $product;
            }

            throw new GeneralException('Có lỗi xảy ra xóa nhật Danh mục!');
        });
    }

    public function restore(Product $product): Product
    {

        return DB::transaction(function () use ($product) {
            if ($product->update([
                'deleted_at' => null,
            ])) {
                event(new ProductRestored($product));
                return $product;
            }

            throw new GeneralException('Có lỗi xảy ra khôi phục Danh mục!');
        });
    }

    public function permanentlyDelete(Product $product): Product
    {

        return DB::transaction(function () use ($product) {
            if ($product->forceDelete()) {
                event(new ProductPermanentlyDeleted($product));
                return $product;
            }

            throw new GeneralException('Có lỗi xảy ra xóa vĩnh  Danh mục!');
        });
    }

    public function uploadAvatar($avatar)
    {
        if ($avatar != '') {
            $file = $avatar;
            $disk = 'local';
            $dir_path = 'products/' . Carbon::now()->year . '/' . Carbon::now()->month . '/' . Carbon::now()->day;
            try {
                $file_name = Uploader::upload($disk, $file, $dir_path);
                if ($file_name != "") {

                    //neu ton tai file cu thi xoa di
                    if ($this->model->path != '') {
                        Uploader::delete($this->model->disk, $this->model->path);
                    }

                    $this->model->disk = $disk;
                    $this->model->path = $file_name;
                    $this->model->save();
                }
            } catch (\Exception $exception) {
                \Log::error($exception);
            }
        }
    }


    public function getDataTable($filter, $option = [])
    {
        $model = $this->model;

        if (!empty($filter)) {

            if (!empty($filter['products.trashed'])) {
                $model = $model->onlyTrashed();
                array_forget($filter, 'products.trashed');
            }
            $model = addFilter($model, $filter);
        }

        if (!empty($option['order'])) {
            foreach ($option['order'] as $key => $value) {
                $model->orderBy($key, $value);
            }
        }

        //Nếu yêu cầu lấy tổng số bản ghi thì trả về tổng luôn
        if(isset($option['countTotal']) && $option['countTotal']){
            return $model->count();
        }

        if(isset($option['start']) && isset($option['length'])){
            $model = $model->skip((int)$option['start'])->take((int)$option['length']);
        }

        return $model->orderBy($this->model->getTable() . '.created_at', 'ASC')->get();
    }


    protected function checkUpdateProductByLink(Product $product, $link)
    {
        if ($product->link !== $link && $this->model->where('link', '=', $link)->first()) {
            throw new GeneralException('Đường link: ' . $link . ' đã tồn tại');
        }
    }

    protected function checkStoreProductByLink($link)
    {
        if ($this->model->where('link', '=', $link)->first()) {
            $link = createLinkWithRandomString($link);
        }
        return $link;
    }

    protected function calProductDiscount($listed_price = 0, $actual_price = 0)
    {
        if ($listed_price > $actual_price) {
            $discount = round(($actual_price / $listed_price) * 100);
            $discount = 100 - $discount;
        } else {
            $discount = 0;
        }
        return $discount;
    }

}
