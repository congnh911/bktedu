<?php

namespace App\Repositories\Backend\Auth;

use App\Events\Backend\Auth\Permission\PermissionUpdated;
use App\Exceptions\GeneralException;
use App\Models\Auth\Permission;
use App\Repositories\BaseRepository;
use Illuminate\Support\Facades\DB;

/**
 * Class PermissionRepository.
 */
class PermissionRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Permission::class;
    }

    public function update(Permission $permission, array $data)
    {

        return DB::transaction(function () use ($permission, $data) {
            if ($permission->update([
                'name' => strtolower($data['name']),
                'title' => !empty($data['title']) ? $data['title'] : null,
                'note' => !empty($data['note']) ? $data['note'] : null,
                'creator_id' => auth()->user()->id
            ])) {
                event(new PermissionUpdated($permission));

                return $permission;
            }

            throw new GeneralException(trans('exceptions.backend.access.roles.update_error'));
        });
    }

}
