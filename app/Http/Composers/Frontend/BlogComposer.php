<?php

namespace App\Http\Composers\Frontend;

use App\Models\Blog\Blog;
use App\Models\Categories\Category;
use App\Models\Slide;
use Illuminate\View\View;

/**
 * Class BlogComposer.
 */
class BlogComposer
{

    public function compose(View $view)
    {
        $categoryMenuGlobal = Category::where('type', config('category.type.blog.id'))->limit(7)->get();

        $view->with('category_menu_global', $categoryMenuGlobal);

        $cat_video = Category::find(10);
        if(!empty($cat_video)) {
            $blog_video = Blog::where('active', 1)
                ->where('category_id', $cat_video->id)
                ->orderBy('created_at', 'desc')
                ->limit(4)
                ->get();
            $view->with('blog_video', $blog_video);
        }

        $cat_handbook = Category::where('slug','cam-nang-du-hoc')->first();
        if(!empty($cat_handbook)){
            $blog_handbook = Blog::where('active',1)
                ->where('category_id',$cat_handbook->id)
                ->orderBy('created_at','desc')
                ->limit(4)
                ->get();
            $view->with('blog_handbook', $blog_handbook);
        }

        $slide_header = Slide::where('position', config('slide.position.header.id'))->first();
        $view->with('slide_header',  $slide_header);

    }
}
