<?php

namespace App\Http\Composers;

use App\Models\Blog\Blog;
use App\Models\Categories\Category;
use App\Models\Slide;
use Illuminate\View\View;

/**
 * Class GlobalComposer.
 */
class GlobalComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {

        $totalUnreadNotifications =  !empty(auth()->user()->unreadNotifications) ?
            auth()->user()->unreadNotifications->count() : 0;

        $view->with('logged_in_user', auth()->user());
        $view->with('total_unread_notification', $totalUnreadNotifications);
    }
}
