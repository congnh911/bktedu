<?php

namespace App\Http\Requests\Backend\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => ['required', 'max:300'],
            'seo_title'         => ['max:300'],
            'seo_keyword'       => ['max:500'],
            'seo_description'   => ['max:1000'],
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'Tên danh mục không được để trống',
            'name.max'              => 'Tên danh mục không được quá :max ký tự',
            'seo_title.max'         => 'SEO Title không được quá :max ký tự',
            'seo_keyword.max'       => 'SEO Keyword không được quá :max ký tự',
            'seo_description.max'   => 'SEO Description không được quá :max ký tự',
        ];
    }
}
