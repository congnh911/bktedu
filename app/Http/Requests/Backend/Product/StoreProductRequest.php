<?php

namespace App\Http\Requests\Backend\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'       => ['required'],
            'name'              => ['required', 'max:300'],
            'link'              => ['max:305'],
        ];
    }

    public function messages()
    {
        return [
            'category_id.required'  => 'Danh mục không được để trống',
            'name.required'         => 'Tên sản phẩm không được để trống',
            'name.max'              => 'Tên sản phẩm không được quá :max ký tự',
            'link.max'              => 'Link không được quá :max ký tự',
        ];
    }
}
