<?php

namespace App\Http\Requests\Backend\Slide;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => ['required', 'max:300'],
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'Tên danh mục không được để trống',
            'name.max'              => 'Tên danh mục không được quá :max ký tự',
        ];
    }
}
