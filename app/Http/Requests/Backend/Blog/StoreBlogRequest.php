<?php

namespace App\Http\Requests\Backend\Blog;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreBlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => ['required', 'max:300'],
        ];
    }

    public function messages()
    {
        return [
            'name.required'         => 'Tên sản phẩm không được để trống',
            'name.max'              => 'Tên sản phẩm không được quá :max ký tự',
        ];
    }
}
