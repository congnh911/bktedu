<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Resources\Comment\CommentResource;
use App\Models\Comment\Comment;
use App\Repositories\Frontend\Comment\CommentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    protected $comment;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->comment = $commentRepository;
    }

    public function index(Request $request)
    {
        $query = base64_decode($request->get('query'));
        if (!empty($query)) {
            $input = parseQueryString($query);
        } else {
            $input = $request->all();
        }

        $columns = array_get($input, 'column',['*']); //nhung truong can hien thi
        $sort = array_get($input, 'sort'); //loc theo orderBy id:asc,created:des
        $pageSize = array_get($input, 'page_size'); //do dai moi page mac dinh la 10

        $model_id = array_get($input,'model_id');
        $model_type = array_get($input,'model_type');
        $creator_id = array_get($input,'creator_id');
        $type = array_get($input,'type');
        $status = array_get($input,'status');
        $parent_id = array_get($input,'parent_id');


        $filter = []; //bo loc
        $order = []; //mang order by column

        $filter = array_add_dot($filter, 'active', 1);

        if(!empty($model_id)){
            $filter = array_add_dot($filter, 'model_id', $model_id);
        }
        if(!empty($model_id)){
            $filter = array_add_dot($filter, 'model_type', $model_type);
        }

        if (!empty($columns) && count($columns) > 1) {
            $columns = explode(',', $columns);
        }

        /**
         * order by column
         * sort=id:asc,name:desc
         */
        if ($sort != '') {
            $sort = explode(',', $sort);
            if (!empty($sort)) {
                foreach ($sort as $item) {
                    $item = explode(':', $item);
                    $order[$item[0]] = $item[1];
                }
            }
        }

        $comments = $this->comment->getDataWithCondition($filter, $order, $pageSize, $columns);

        return CommentResource::collection($comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $content = $request->get('content',null);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Comment\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Comment\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Comment\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
