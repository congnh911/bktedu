<?php


namespace App\Http\Controllers\Api\V1;


use App\Events\Api\CommentEvent;
use App\Http\Controllers\Controller;
use App\Models\Comment\Comment;
use App\Repositories\Frontend\Comment\CommentRepository;
use Illuminate\Http\Request;

class CommentController1 extends Controller
{

    protected $comment;
    public function __construct(CommentRepository $commentRepository)
    {
        $this->comment = $commentRepository;
    }
    public function index(){

    }

    public function fetch(Request $request)
    {

        $query = base64_decode($request->get('query'));
        if ($query != "") {
            $input = parseQueryString($query);
        }else{
            $input = $request->all();
        }

        $column = array_get($input,'column'); //nhung truong can hien thi
        $sort = array_get($input,'sort'); //loc theo orderBy id:asc,created:des
        $pageSize = array_get($input,'page_size'); //do dai moi page mac dinh la 10

        $filter = []; //bo loc
        $order = []; //mang order by column

        $filter = array_add_dot($filter,'active',1);

        /**
         * ============
         * select column
         */

        if(!empty($column)){
            $this->column = explode(',',$column);
        }

        /**
         * order by column
         * sort=id:asc,name:desc
         */
        if($sort != ''){
            $sort = explode(',',$sort);
            if(!empty($sort)){
                foreach ($sort as $item){
                    $item = explode(':',$item);
                    $order[$item[0]] = $item[1];
                }
            }
        }

        $comments = $this->comment->getDataWithCondition($filter,$order,$pageSize);

        return response()->json($comments);
    }

    public function store(Request $request)
    {
        $comment = Comment::create($request->all());

        event(new CommentEvent($comment));
        return response()->json('ok');

    }

}
