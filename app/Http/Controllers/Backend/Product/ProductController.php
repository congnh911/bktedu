<?php

namespace App\Http\Controllers\Backend\Product;

use App\Http\Requests\Backend\Product\ManageProductRequest;
use App\Http\Requests\Backend\Product\StoreProductRequest;
use App\Http\Requests\Backend\Product\UpdateProductRequest;
use App\Models\Categories\Category;
use App\Models\Product\Product;
use App\Repositories\Backend\CategoryRepository;
use App\Repositories\Backend\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class ProductController extends Controller
{

    protected $product;
    protected $category;
    const LIST_INDENT = ' === ';

    public function __construct(ProductRepository $productRepository,CategoryRepository $categoryRepository)
    {
        $this->product = $productRepository;
        $this->category = $categoryRepository;
    }


    public function index(ManageProductRequest $request)
    {
        $data = [];
        $category_id = $request->get('category_id',[]);

        if(!empty($category_id) && count($category_id) > 0){
            $category_product_selected = $this->category->getById($category_id);
            $data['category_product_selected'] = $category_product_selected;
        }

        return view('backend.product.index', $data);
    }


    public function create(ManageProductRequest $request)
    {
        $input = $request->all();
        $data = [];
        $data['categories'] = $this->getCategories(config('category.type.product.id'));
        return view('backend.product.create', $data);
    }


    public function store(StoreProductRequest $request)
    {
        $input = $request->all();
        $model = $this->product->create($input);
        return response()->redirectToRoute('admin.product.edit',$model)
            ->withFlashSuccess('Thêm mới Danh mục thành công');
    }


    public function edit(ManageProductRequest $request, Product $product)
    {
        $data['data'] = $product;
        $data['categories_selected'] = $product->productCategories()->get();
        $data['categories'] = $this->getCategories(config('category.type.product.id'));
        return view('backend.product.edit', $data);
    }

    public function show(Product $product){
        $data['data'] = $product;
        return view('backend.product.includes.modal.show',$data);
    }


    public function update(UpdateProductRequest $request, Product $product)
    {
        $input = $request->all();
        $this->product->update($product, $input);
        return back()->withFlashSuccess('Cập nhật Danh mục thành công');
    }


    public function destroy(ManageProductRequest $request, Product $product)
    {
        //
        $delete = $this->product->destroy($product);
        if ($delete) {
            return response()->json(['success' => true, 'message' => 'Xóa bản ghi thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function get(Request $request)
    {
        $query = $request->get('query');
        $input = parseQueryString(base64_decode($query));

        //param
        $trashed = array_get($input, 'trashed', 0);
        $name = array_get($input, 'name', '');
        $type = array_get($input, 'type', '');

        $start = $request->get('start');
        $length = $request->get('length');

        $filter = [];
        $order = [];

        //condition multi
        if ($name != '') {
            $filter = array_add_dot($filter, 'products.name', [
                'operator' => 'LIKE',
                'value' => '%' . $name . '%'
            ]);
        }
        if ($type != '') {
            $filter = array_add_dot($filter, 'products.type', $type);
        }
        if ($trashed == true) {
            $filter = array_add_dot($filter, 'products.trashed', true);
        }

        $countTotal = $this->product->getDataTable($filter, ['countTotal' => true]);
        $items = $this->product->getDataTable($filter, ['order' => $order, 'start' => $start, 'length' => $length]);

        return Datatables::of($items)
            ->addColumn('actions', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'actions'
                ])->render();
            })->addColumn('categories', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'categories'
                ])->render();
            })
            ->editColumn('created_at', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'created_at'
                ])->render();
            })
            ->editColumn('avatar', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'avatar'
                ])->render();
            })
            ->editColumn('active', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'active'
                ])->render();
            })
            ->editColumn('highlight', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'highlight'
                ])->render();
            })
            ->setTotalRecords($countTotal)
            ->skipPaging()
            ->rawColumns(['avatar', 'actions','categories', 'active', 'highlight','parent'])
            ->make(true);
    }

    public function getCategories($type)
    {
        return Category::categoryNotSefl($type);
    }

    //nhung truong bat tat 0 1
    public function switch01(Product $product, Request $request)
    {
        $input = $request->all();
        $column = array_get($input, 'column', '');
        $value = array_get($input, 'value', 0);

        $save = $product->update([$column => $value]);
        if ($save) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function restore(Product $product)
    {

        $restore = $product->restore();

        if ($restore) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function deletePermanently(Product $product)
    {

        $destroy = $product->forceDelete();

        if ($destroy) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }
}
