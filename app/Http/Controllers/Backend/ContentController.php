<?php
/**
 * Created by PhpStorm.
 * User: congnh
 * Date: 06/08/2019
 * Time: 10:00
 */

namespace App\Http\Controllers\Backend;


use App\Core\Uploader;
use App\Http\Controllers\Controller;
use App\Models\Content;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    protected $model;

    public function __construct(Content $content)
    {
        $this->model = $content;
    }

    public function index()
    {

    }

    public function store(Request $request)
    {
        $model_id = $request->get('model_id');
        $model_type = $request->get('model_type');
        $type = $request->get('content_type');
        $content_name = $request->get('content_name');
        $content_summary = $request->get('content_summary');

        $check_duplicate = Content::where('model_id', $model_id)
            ->where('model_type', $model_type)
            ->where('type', $type)
            ->first();
        if ($check_duplicate) {
            return response()->json(['message' => 'Đã có nội dung này']);
        }

        $data['model_id'] = $model_id;
        $data['model_type'] = $model_type;
        $data['type'] = $type;
        $data['name'] = $content_name;
        $data['content'] = $content_summary;
        $data['creator_id'] = auth()->user()->id;
        $data['active'] = 1;


        $content = Content::create($data);
        if ($content) {
            return response()->json([
                'success' => true,
                'message' => 'Thêm mới nội dung thành công',
                'data' => view('backend.includes.partials.list_content_item',['data' => $content])->render()
            ]);
        } else {
            return response()->json([
                'message' => 'Không thể thêm mới nội dung'
            ]);
        }

    }

    public function update(Content $content, Request $request)
    {
        $model_id = $request->get('model_id');
        $model_type = $request->get('model_type');
        $type = $request->get('content_type');
        $content_name = $request->get('content_name');
        $content_summary = $request->get('content_summary');
        $id = $content->id;

        //ngoai tru id == $type
        $content_type_ids = collect(config('product.content_type'))
            ->where('id','<>',$type)->pluck('id')->toArray();
        $check_duplicate = Content::where('model_id', $model_id)
            ->where('model_type', $model_type)
            ->where('type', $type)
            ->whereIn('type',$content_type_ids)
            ->first();
        if ($check_duplicate) {
            return response()->json(['message' => 'Đã có nội dung này']);
        }

        $data['model_id'] = $model_id;
        $data['model_type'] = $model_type;
        $data['type'] = $type;
        $data['name'] = $content_name;
        $data['content'] = $content_summary;
        $data['creator_id'] = auth()->user()->id;
        $data['active'] = 1;

        if ($content->update($data)) {
            return response()->json([
                'success' => true,
                'message' => 'Cập nhật nội dung thành công',
                'id' => $id,
                'data_html' => view('backend.includes.partials.list_content_item_update',['data' => $content])
                    ->render(),
            ]);
        } else {
            return response()->json([
                'message' => 'Không thể cập nhât nội dung'
            ]);
        }

    }

    public function delete(Content $content)
    {
        if ($content->delete()) {
            return response()->json(['success' => true, 'message' => 'Xóa thành công','data' => $content]);
        }else{
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function popupCreate(Request $request)
    {
        $data['model_id'] = $request->get('model_id');
        $data['model_type'] = $request->get('model_type');

        return view('backend.includes.partials.popup_create_content', $data)->render();
    }

    public function popupEdit(Content $content, Request $request)
    {
        $data['data'] = $content;
        return view('backend.includes.partials.popup_edit_content',$data)->render();
    }
}
