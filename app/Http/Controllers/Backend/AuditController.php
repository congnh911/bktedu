<?php
/**
 * Created by PhpStorm.
 * User: congnh
 * Date: 19/07/2019
 * Time: 13:45
 */

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class AuditController extends Controller
{

    public function get(Request $request)
    {

        $query = $request->get('query');
        $input = parseQueryString(base64_decode($query));

        //param
        $auditable_type = array_get($input, 'auditable_type', '');

        $audits = \OwenIt\Auditing\Models\Audit::with('user')
            ->orderBy('created_at', 'desc')
            ->where('auditable_type', $auditable_type)->get();

        return DataTables::of($audits)
            ->addColumn('user', function ($item) {
                return !empty($item->user) ? $item->user->link() : null;
            })
            ->addColumn('event', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'module' => 'audit',
                    'column' => 'event'
                ])->render();
            })
            ->editColumn('created_at', function ($item) {
                return \Carbon\Carbon::parse($item->created_at)->format('H:i d/m/Y');
            })
            ->addColumn('data_old', function ($item) {
                $html = '';
                foreach ($item->old_values as $attribute => $value) {
                    $html .= '<b>' . $attribute . '</b> : ' . $value. ' ';
                }
                return $html;
            })
            ->addColumn('data_new', function ($item) {
                $html = '';
                foreach ($item->new_values as $attribute => $value) {
                    $html .= '<b>' . $attribute . '</b> : ' . $value.' ';
                }
                return $html;
            })
            ->rawColumns(['user','data_old', 'data_new', 'event'])
            ->make(true);
    }
}
