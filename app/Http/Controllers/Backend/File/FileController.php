<?php

namespace App\Http\Controllers\Backend\File;

use App\Core\Uploader;
use App\Models\File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FileController extends Controller
{
    protected $model;

    public function __construct(File $file)
    {
        $this->model   =    $file;
    }

    /**
     * Download File the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function downloadFile($id){
        $uploadFile = $this->model->find($id);
        if($uploadFile){
            $file   = Uploader::getFileFromStorage($uploadFile->disk,$uploadFile->path);
            return response()->download($file->getAdapter()->getPathPrefix().$file->getPath(),$uploadFile->original_name);
        } else {
            abort(404);
        }
    }

    public function upload(Request $request)
    {
        $input = $request->all();
        $model_id = array_get($input,'model_id');
        $model_type = array_get($input,'model_type');
        $files = array_get($input,'files');

        $disk =  'local';
        $model_name = strtolower(last(explode('\\',$model_type)));
        $path = 'files/'.$model_name.'/' . Carbon::now()->year . '/' . Carbon::now()->month . '/' . Carbon::now()->day;
        $model = $model_type::find($model_id);
        if($model){
            $array_file = [];
            if(count($files) > 0){
                foreach ($files as $key => $file){
                    $file_path = Uploader::uploadFileOrigin($disk,$file,$path);
                    $array_file[] = [
                        'disk' => $disk,
                        'path' => $file_path,
                        'original_name' => $file->getClientOriginalName(),
                        'original_ext' => $file->getClientOriginalExtension(),
                        'original_size' => $file->getSize(),
                        'creator_id' => auth()->user()->id,
                        'model_id' => $model_id,
                        'model_type' => $model_type
                    ];
                }
            }
            if(!empty($array_file)){
                $save = File::insert($array_file);
                if($save){
                    $html = view('backend.includes.partials.item_file', ['files' => $model->files])->render();
                    return response()->json(['success' => true,'message' => 'Cập nhật thành công','data' => $html]);
                }else{
                    return response()->json(['message' => config('common.message_error')]);
                }
            }else{
                return response()->json(['message' => config('common.message_error')]);
            }
        }

    }

    public function viewFile($id){
        $uploadFile = $this->model->find($id);
        if($uploadFile){
            $file      = Uploader::getFileFromStorage($uploadFile->disk,$uploadFile->path);
            $full_path = $file->getAdapter()->getPathPrefix().$file->getPath();

            if($uploadFile->original_ext == 'pdf'){

                if (\File::isFile($full_path)) {
                    $file = \File::get($full_path);
                    $response = \Response::make($file, 200);
                    $response->header('Content-Type', 'application/pdf');
                    return $response;
                }

            } else {
                return response()->download($full_path,$uploadFile->original_name);
            }

        } else {
            abort(404);
        }
    }

    /**
     * Remove File the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroyFile(Request $request,$id)
    {
        $uploadFile = $this->model->find($id);
        if($uploadFile){
            $old_disk = $uploadFile->disk;
            $old_path = $uploadFile->path;
            $uploadFile->delete();

            Uploader::delete($old_disk,$old_path);

            return response()->json(['success' => true,'message' => 'Xóa tệp thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }
}
