<?php

namespace App\Http\Controllers\Backend\Blog;

use App\Http\Requests\Backend\Blog\ManageBlogRequest;
use App\Http\Requests\Backend\Blog\StoreBlogRequest;
use App\Http\Requests\Backend\Blog\UpdateBlogRequest;
use App\Models\Auth\Role;
use App\Models\Categories\Category;
use App\Models\Blog\Blog;
use App\Models\Image;
use App\Repositories\Backend\CategoryRepository;
use App\Repositories\Backend\Blog\BlogRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class BlogController extends Controller
{

    protected $blog;
    protected $category;
    const LIST_INDENT = ' === ';

    public function __construct(BlogRepository $blogRepository,CategoryRepository $categoryRepository)
    {
        $this->blog = $blogRepository;
        $this->category = $categoryRepository;
    }


    public function index(ManageBlogRequest $request)
    {
        $data = [];
        $category_id = $request->get('category_id',[]);

        if(!empty($category_id) && count($category_id) > 0){
            $category_blog_selected = $this->category->getById($category_id);
            $data['category_blog_selected'] = $category_blog_selected;
        }

        return view('backend.blog.index', $data);
    }


    public function create(ManageBlogRequest $request)
    {
        $input = $request->all();
        $data = [];
        $data['categories'] = $this->getCategories(config('category.type.blog.id'));
        return view('backend.blog.create', $data);
    }


    public function store(StoreBlogRequest $request)
    {
        $input = $request->all();
        $model = $this->blog->create($input);
        return response()->redirectToRoute('admin.blog.edit',$model)
            ->withFlashSuccess('Thêm mới Danh mục thành công');
    }


    public function edit(ManageBlogRequest $request, Blog $blog)
    {
        $data['data'] = $blog;
        $data['roles'] = Role::get();
        $data['userPermissions'] = $blog->userPermission()->get();
        $data['categoriesSelected'] = $blog->blogCategories()->get();
        $data['categories'] = $this->getCategories(config('category.type.blog.id'));
        return view('backend.blog.edit', $data);
    }

    public function show(Blog $blog){
        $data['data'] = $blog;
        return view('backend.blog.includes.modal.show',$data);
    }


    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        $input = $request->all();
        $this->blog->update($blog, $input);
        return back()->withFlashSuccess('Cập nhật Danh mục thành công');
    }


    public function destroy(ManageBlogRequest $request, Blog $blog)
    {
        //
        $delete = $this->blog->destroy($blog);
        if ($delete) {
            return response()->json(['success' => true, 'message' => 'Xóa bản ghi thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function get(Request $request)
    {
        $query = $request->get('query');
        $input = parseQueryString(base64_decode($query));

        //param
        $trashed = array_get($input, 'trashed', 0);
        $name = array_get($input, 'name', '');
        $type = array_get($input, 'type', '');

        $start = $request->get('start');
        $length = $request->get('length');

        $filter = [];
        $order = [];

        //condition multi
        if ($name != '') {
            $filter = array_add_dot($filter, 'blogs.name', [
                'operator' => 'LIKE',
                'value' => '%' . $name . '%'
            ]);
        }
        if ($type != '') {
            $filter = array_add_dot($filter, 'blogs.type', $type);
        }
        if ($trashed == true) {
            $filter = array_add_dot($filter, 'blogs.trashed', true);
        }

        $countTotal = $this->blog->getDataTable($filter, ['countTotal' => true]);
        $items = $this->blog->getDataTable($filter, ['order' => $order, 'start' => $start, 'length' => $length]);

        return Datatables::of($items)
            ->addColumn('actions', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'actions'
                ])->render();
            })->addColumn('categories', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'categories'
                ])->render();
            })
            ->editColumn('created_at', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'created_at'
                ])->render();
            })
            ->editColumn('name', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'name'
                ])->render();
            })
            ->editColumn('avatar', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'avatar'
                ])->render();
            })
            ->editColumn('active', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'active'
                ])->render();
            })
            ->editColumn('highlight', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'highlight'
                ])->render();
            })
            ->setTotalRecords($countTotal)
            ->skipPaging()
            ->rawColumns(['avatar','name', 'actions','categories', 'active', 'highlight','parent'])
            ->make(true);
    }

    public function getCategories($type)
    {
        return Category::categoryNotSefl($type);
    }

    //nhung truong bat tat 0 1
    public function switch01(Blog $blog, Request $request)
    {
        $input = $request->all();
        $column = array_get($input, 'column', '');
        $value = array_get($input, 'value', 0);

        $save = $blog->update([$column => $value]);
        if ($save) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function restore(Blog $blog)
    {

        $restore = $blog->restore();

        if ($restore) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function deletePermanently(Blog $blog)
    {

        $destroy = $blog->forceDelete();

        if ($destroy) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }
}
