<?php

namespace App\Http\Controllers\Backend\Slide;

use App\Http\Requests\Backend\Slide\ManageSlideRequest;
use App\Http\Requests\Backend\Slide\StoreSlideRequest;
use App\Http\Requests\Backend\Slide\UpdateSlideRequest;
use App\Models\Auth\Role;
use App\Models\Categories\Category;
use App\Models\Slide;
use App\Models\Image;
use App\Repositories\Backend\CategoryRepository;
use App\Repositories\Backend\Slide\SlideRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class SlideController extends Controller
{

    protected $slide;
    protected $category;
    const LIST_INDENT = ' === ';

    public function __construct(SlideRepository $slideRepository,CategoryRepository $categoryRepository)
    {
        $this->slide = $slideRepository;
        $this->category = $categoryRepository;
    }


    public function index(ManageSlideRequest $request)
    {
        $data = [];
        $category_id = $request->get('category_id',[]);

        if(!empty($category_id) && count($category_id) > 0){
            $category_slide_selected = $this->category->getById($category_id);
            $data['category_slide_selected'] = $category_slide_selected;
        }

        return view('backend.slide.index', $data);
    }


    public function create(ManageSlideRequest $request)
    {
        $input = $request->all();
        $data = [];
        return view('backend.slide.create', $data);
    }


    public function store(StoreSlideRequest $request)
    {
        $input = $request->all();
        $model = $this->slide->create($input);
        return response()->redirectToRoute('admin.slide.edit',$model)
            ->withFlashSuccess('Thêm mới Danh mục thành công');
    }


    public function edit(ManageSlideRequest $request, Slide $slide)
    {
        $data['data'] = $slide;
        return view('backend.slide.edit', $data);
    }

    public function show(Slide $slide){
        $data['data'] = $slide;
        return view('backend.slide.includes.modal.show',$data);
    }


    public function update(UpdateSlideRequest $request, Slide $slide)
    {
        $input = $request->all();
        $this->slide->update($slide, $input);
        return back()->withFlashSuccess('Cập nhật Danh mục thành công');
    }


    public function destroy(ManageSlideRequest $request, Slide $slide)
    {
        //
        $delete = $this->slide->destroy($slide);
        if ($delete) {
            return response()->json(['success' => true, 'message' => 'Xóa bản ghi thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function get(Request $request)
    {
        $query = $request->get('query');
        $input = parseQueryString(base64_decode($query));

        //param
        $trashed = array_get($input, 'trashed', 0);
        $name = array_get($input, 'name', '');

        $start = $request->get('start');
        $length = $request->get('length');

        $filter = [];
        $order = [];

        //condition multi
        if ($name != '') {
            $filter = array_add_dot($filter, 'slides.name', [
                'operator' => 'LIKE',
                'value' => '%' . $name . '%'
            ]);
        }

        if ($trashed == true) {
            $filter = array_add_dot($filter, 'slides.trashed', true);
        }

        $countTotal = $this->slide->getDataTable($filter, ['countTotal' => true]);
        $items = $this->slide->getDataTable($filter, ['order' => $order, 'start' => $start, 'length' => $length]);

        return Datatables::of($items)
            ->addColumn('actions', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'actions'
                ])->render();
            })
            ->editColumn('created_at', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'created_at'
                ])->render();
            })
            ->editColumn('position', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'position'
                ])->render();
            })
            ->editColumn('gallery', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'gallery'
                ])->render();
            })
            ->editColumn('active', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'active'
                ])->render();
            })
            ->editColumn('highlight', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'highlight'
                ])->render();
            })
            ->setTotalRecords($countTotal)
            ->skipPaging()
            ->rawColumns(['avatar', 'actions', 'active', 'gallery','highlight'])
            ->make(true);
    }

    //nhung truong bat tat 0 1
    public function switch01(Slide $slide, Request $request)
    {
        $input = $request->all();
        $column = array_get($input, 'column', '');
        $value = array_get($input, 'value', 0);

        $save = $slide->update([$column => $value]);
        if ($save) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function restore(Slide $slide)
    {

        $restore = $slide->restore();

        if ($restore) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function deletePermanently(Slide $slide)
    {

        $destroy = $slide->forceDelete();

        if ($destroy) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }
}
