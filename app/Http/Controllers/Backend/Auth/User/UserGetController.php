<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Models\Auth\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\UserRepository;
use App\Http\Requests\Backend\Auth\User\ManageUserRequest;
use Illuminate\Http\Request;

/**
 * Class UserGetController.
 */
class UserGetController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getUserByName(Request $request)
    {
        $keyword = $request->get('term');
        $params['name'] = [
            'operator' => 'LIKE',
            'value' => '%' . $keyword . '%'
        ];
        if ($keyword != null) {
            $data = User::where('first_name', 'LIKE', '%' . $keyword . '%')
                ->orWhere('last_name', 'LIKE', '%' . $keyword . '%')
                ->limit(10)->select('id','first_name','last_name')->get()->toArray();
            return response()->json($data);
        } else {
            $data = User::limit(30)->select('id','first_name','last_name')->get()->toArray();
            return response()->json($data);
        }
    }

}
