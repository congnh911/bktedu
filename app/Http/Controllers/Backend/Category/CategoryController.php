<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Backend\Category\ManageCategoryRequest;
use App\Http\Requests\Backend\Category\StoreCategoryRequest;
use App\Http\Requests\Backend\Category\UpdateCategoryRequest;
use App\Models\Categories\Category;
use App\Repositories\Backend\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;

class CategoryController extends Controller
{

    protected $category;
    const LIST_INDENT = ' === ';

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->category = $categoryRepository;
    }


    public function index(ManageCategoryRequest $request)
    {
        $input = $request->all();
        $type = array_get($input, 'type', 0);
        $data['categories'] = $this->getCategories($type);
        return view('backend.category.index', $data)->withCategories($this->category->getActivePaginated());
    }


    public function create(ManageCategoryRequest $request)
    {
        $input = $request->all();
        $type = array_get($input, 'type', 0);

        $data = [];
        $data['categories'] = $this->getCategories($type);

        return view('backend.category.create', $data);
    }


    public function store(StoreCategoryRequest $request)
    {
        $input = $request->all();
        $this->category->create($input);

        return back()->withFlashSuccess('Thêm mới Danh mục thành công');
    }


    public function edit(ManageCategoryRequest $request, Category $category)
    {
        $input = $request->all();
        $type = array_get($input, 'type', 0);

        $data = [];
        $data['categories'] = $this->getCategories($type);
        $data['files'] = $category->files;
        $data['data'] = $category;
        return view('backend.category.edit', $data);
    }

    public function show(Category $category)
    {
        $data['data'] = $category;
        return view('backend.category.includes.modal.show', $data);
    }


    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $input = $request->all();
        $this->category->update($category, $input);
        return back()->withFlashSuccess('Cập nhật Danh mục thành công');
    }


    public function destroy(ManageCategoryRequest $request, Category $category)
    {
        //
        $delete = $this->category->destroy($category);
        if ($delete) {
            return response()->json(['success' => true, 'message' => 'Xóa bản ghi thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function get(Request $request)
    {
        $query = $request->get('query');
        $input = parseQueryString(base64_decode($query));

        //param
        $trashed = array_get($input, 'trashed', 0);
        $name = array_get($input, 'name', '');
        $type = array_get($input, 'type', '');

        $start = $request->get('start');
        $length = $request->get('length');

        $filter = [];
        $order = [];

        //condition multi
        if ($name != '') {
            $filter = array_add_dot($filter, 'categories.name', [
                'operator' => 'LIKE',
                'value' => '%' . $name . '%'
            ]);
        }
        if ($type != '') {
            $filter = array_add_dot($filter, 'categories.type', $type);
        }
        if ($trashed == true) {
            $filter = array_add_dot($filter, 'categories.trashed', true);
        }

        $countTotal = $this->category->getDataTable($filter, ['countTotal' => true]);
        $items = $this->category->getDataTable($filter, ['order' => $order, 'start' => $start, 'length' => $length]);

        return DataTables::of($items)
            ->editColumn('name', function ($item) use ($type) {
                $treeCategories
                    = Category::getNestedListType('name', null, self::LIST_INDENT, $type);
                if ($item->deleted_at != null) {
                    $category_name = $item->name;
                } else {
                    $category_name = !empty($treeCategories[$item->id]) ? $treeCategories[$item->id] : null;
                }
                return view('backend.includes.partials.column', [
                    'category_name' => $category_name,
                    'column' => 'category_name'
                ])->render();

            })->addColumn('actions', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'actions'
                ])->render();
            })->addColumn('parent', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'parent'
                ])->render();
            })
            ->editColumn('created_at', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'created_at'
                ])->render();
            })
            ->editColumn('avatar', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'avatar'
                ])->render();
            })
            ->editColumn('active', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'active'
                ])->render();
            })
            ->editColumn('highlight', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'highlight'
                ])->render();
            })
            ->addColumn('parent', function ($item) {
                return view('backend.includes.partials.column', [
                    'item' => $item,
                    'column' => 'parent'
                ])->render();
            })
            ->setTotalRecords($countTotal)
            ->skipPaging()
            ->rawColumns(['avatar', 'actions', 'active', 'highlight', 'parent'])
            ->make(true);
    }

    public function getCategories($type)
    {
        return Category::categoryNotSefl($type);
    }

    //nhung truong bat tat 0 1
    public function switch01(Category $category, Request $request)
    {
        $input = $request->all();
        $column = array_get($input, 'column', '');
        $value = array_get($input, 'value', 0);

        $save = $category->update([$column => $value]);
        if ($save) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function restore(Category $category)
    {

        $restore = $category->restore();

        if ($restore) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function deletePermanently(Category $category)
    {

        $destroy = $category->forceDelete();

        if ($destroy) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }
}
