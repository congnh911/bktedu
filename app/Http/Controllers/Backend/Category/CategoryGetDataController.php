<?php

namespace App\Http\Controllers\Backend;


use App\Models\Categories\Category;
use App\Repositories\Backend\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryGetDataController extends Controller
{

    protected $category;
    const LIST_INDENT = ' === ';

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->category = $categoryRepository;
    }


    public function getCategoryProduct(Request $request)
    {
        $type = config('category.type.product.id');//type
        $keyword = $request->get('term');
        $params['name'] = [
            'operator' => 'LIKE',
            'value' => '%' . $keyword . '%'
        ];
        if ($keyword != null) {
            $data = Category::where('type', $type)
                ->where('name', 'LIKE', '%' . $keyword . '%')
                ->limit(10)->get()->toArray();
            return response()->json($data);
        } else {
            $data = Category::where('highlight', 1)
                ->limit(30)->get()->toArray();
            return response()->json($data);
        }
    }

    public function getCategoryBlog(Request $request)
    {
        $type = config('category.type.blog.id');//type
        $keyword = $request->get('term');
        $params['name'] = [
            'operator' => 'LIKE',
            'value' => '%' . $keyword . '%'
        ];
        if ($keyword != null) {
            $data = Category::where('type', $type)
                ->where('name', 'LIKE', '%' . $keyword . '%')
                ->limit(10)->get()->toArray();
            return response()->json($data);
        } else {
            $data = Category::where('highlight', 1)
                ->limit(30)->get()->toArray();
            return response()->json($data);
        }
    }

}
