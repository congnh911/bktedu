<?php

namespace App\Http\Controllers\Backend\Video;

use App\Core\Uploader;
use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    protected $model;

    public function __construct(Video $video)
    {
        $this->model = $video;
    }

    /**
     * Download File the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function download($id)
    {
        $uploadFile = $this->model->find($id);
        if ($uploadFile) {
            $video = Uploader::getFileFromStorage($uploadFile->disk, $uploadFile->path);
            return response()->download($video->getAdapter()->getPathPrefix() . $video->getPath(), $uploadFile->original_name);
        } else {
            abort(404);
        }
    }

    public function upload(Request $request)
    {
        $input = $request->all();
        $model_id = array_get($input, 'model_id');
        $model_type = array_get($input, 'model_type');
        $videos = array_get($input, 'videos', []);

        $disk = 'local';
        $model_name = strtolower(last(explode('\\', $model_type)));
        $path = 'videos/' . $model_name . '/' . Carbon::now()->year . '/' . Carbon::now()->month . '/' . Carbon::now()->day;
        $model = $model_type::find($model_id);

        if ($model) {
            $array_file = [];
            if (count($videos) > 0) {
                foreach ($videos as $key => $video) {
                    $video_path = Uploader::uploadFileOrigin($disk, $video, $path);
                    $array_file[] = [
                        'disk' => $disk,
                        'path' => $video_path,
                        'original_name' => $video->getClientOriginalName(),
                        'original_ext' => $video->getClientOriginalExtension(),
                        'original_size' => $video->getSize(),
                        'creator_id' => auth()->user()->id,
                        'model_id' => $model_id,
                        'model_type' => $model_type
                    ];
                }

                if (!empty($array_file)) {
                    $save = Video::insert($array_file);
                    if ($save) {
                        $html = view('backend.includes.partials.item_video', ['videos' => $model->videos])->render();
                        return response()->json(['success' => true, 'message' => 'Cập nhật thành công', 'data' => $html]);
                    } else {
                        return response()->json(['message' => config('common.message_error')]);
                    }
                } else {
                    return response()->json(['message' => config('common.message_error')]);
                }
            } else {
                return response()->json(['message' => config('common.message_error')]);
            }

        }

    }

    public function view($id)
    {
        $uploadFile = $this->model->find($id);
        if ($uploadFile) {
            $video = Uploader::getFileFromStorage($uploadFile->disk, $uploadFile->path);
            $full_path = $video->getAdapter()->getPathPrefix() . $video->getPath();

            if ($uploadFile->original_ext == 'pdf') {

                if (\File::isFile($full_path)) {
                    $video = \File::get($full_path);
                    $response = \Response::make($video, 200);
                    //$response->header('Content-Type', 'application/pdf');
                    return $response;
                }

            } else {
                return response()->download($full_path, $uploadFile->original_name);
            }

        } else {
            abort(404);
        }
    }

    public function update(Video $video, Request $request)
    {
        $field = $request->get('field');
        $value = $request->get('value');

        if ($video->update([$field => $value])) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    /**
     * Remove File the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Request $request, Video $video)
    {
        $uploadFile = $this->model->find($video->id);
        if ($uploadFile) {
            $old_disk = $uploadFile->disk;
            $old_path = $uploadFile->path;
            $uploadFile->delete();

            Uploader::delete($old_disk, $old_path);

            return response()->json(['success' => true, 'message' => 'Xóa video thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }
}
