<?php
/**
 * Created by PhpStorm.
 * User: congnh
 * Date: 06/08/2019
 * Time: 10:00
 */

namespace App\Http\Controllers\Backend;


use App\Core\Uploader;
use App\Http\Controllers\Controller;
use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    protected $model;

    public function __construct(Image $image)
    {
        $this->model = $image;
    }

    public function index()
    {

    }

    public function store(Request $request)
    {
        $files = $request->file('files');
        $model_type = $request->get('model_type');
        $model_id = $request->get('model_id');

        if (!empty($files) && count($files) > 0) {
            $html = '';
            foreach ($files as $file) {
                $data = [
                    'model_id' => $model_id,
                    'model_type' => $model_type,
                    'creator_id' => auth()->user()->id
                ];
                $image = Image::create($data);
                $this->model = $image;

                $disk = 'public';
                $model_name = strtolower(last(explode('\\', $model_type)));
                $dir_path = 'albums/' . $model_name . '/' . Carbon::now()->year . '/' . Carbon::now()->month . '/' . Carbon::now()->day;
                try {
                    $file_name = Uploader::upload($disk, $file, $dir_path);
                    if ($file_name != "") {

                        //neu ton tai file cu thi xoa di
                        if ($this->model->path != '') {
                            Uploader::delete($this->model->disk, $this->model->path);
                        }
                        $this->model->disk = $disk;
                        $this->model->path = $file_name;
                        $this->model->save();
                    }
                } catch (\Exception $exception) {
                    \Log::error($exception);
                }
            }
            $model = $model_type::find($model_id);
            $html = view('backend.includes.partials.item_album', ['albums' => $model->albums,'data' => $model])->render();
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công', 'data' => $html]);
        }
    }

    public function update(Image $image, Request $request)
    {
        $column = $request->get('column');
        $value = $request->get('value');

        if ($image->update([$column => $value])) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function delete(Image $image)
    {
        if ($image->delete()) {
            //neu ton tai file cu thi xoa di
            if ($image->path != '') {
                Uploader::delete($image->disk, $image->path);
            }
            return response()->json(['success' => true, 'message' => 'Xóa thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function sortable(Request $request)
    {
        $data = $request->get('data');
        $sortId = explode(',',$data);
        foreach ($sortId as $key => $value){
            $save = Image::find($value)->update(['order' => $key]);
        }
        if ($save) {
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function isAvatar(Request $request)
    {
        $id = $request->get('id');
        $model_type = $request->get('model_type');
        $model_id = $request->get('model_id');
        $image = Image::find($id);
        if (!empty($model_type) && !empty($model_id)) {
            Image::where('model_type', $model_type)
                ->where('model_id', $model_id)
                ->update(['avatar' => 0]);
            $image->update(['avatar' => 1]);
            $blog = $model_type::find($model_id)->update([
                'disk' => $image->disk,
                'avatar' => $image->path,
            ]);
            return response()->json(['success' => true, 'message' => 'Cập nhật thành công']);
        } else {
            return response()->json(['message' => 'Co loi xay ra']);
        }


    }
}
