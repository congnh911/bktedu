<?php
/**
 * Created by PhpStorm.
 * User: congnh
 * Date: 19/08/2019
 * Time: 13:24
 */

namespace App\Http\Controllers;


use GuzzleHttp\Client;

class TestController extends Controller
{

    public function convertMapGeocode()
    {
        ini_set('max_execution_time', 1200);
        // lay data tu cache
        $data = \Cache::get('data_map');

//        dd($data);
        if (empty($data)) {
            //doc file txt
            $contents = \File::get(public_path('station_name.txt'));
            //tao mang station name
            $contents = preg_split("/\r\\n|\\r|\\n/", $contents);

//            $contents = array_slice($contents,0,5);
//            dd($contents);
            //foreach de search api tung dia diem
            foreach ($contents as $key => $content) {
                $client = new Client(['base_uri' => 'https://www.geocoding.jp']);
                $response = $client->get('api', [
                    'query' => [
                        'q' => $content,
                    ],
                    'headers' => [
                        'Accept' => 'application/xml'
                    ]
                ]);

                $body = (string)$response->getBody();
                //lay ra mang data lat long
                $xml = simplexml_load_string($body);
                $xml = $this->object2array($xml);
                $xml = $xml['coordinate'];
                $data[$content] = [
                    'lat' => $xml['lat'],
                    'lng' => $xml['lng'],
                    'lat_dms' => $xml['lat_dms'],
                    'lng_dms' => $xml['lng_dms'],
                ];
                sleep(5);
            }
            \Cache::forever('data_map', $data);
        }

        return response()->json($data);
    }

    function object2array($object)
    {
        return json_decode(json_encode($object), 1);
    }

    public function dequy(){
        $data = [
            'email' => '',
            'password' => '123456',
            'card_info' => [
                'card_no' => '123456789',
                'card_secure' => '123',
            ],
            'child' => [
                'email' => '',
                'password' => '123456',
                'card_info' => [
                    'card_no' => '123456789',
                    'card_secure' => '123'
                ]
            ]
        ];
        return $this->secuData($data);

//        $string = '';
//        for($i = 1; $i<=5;$i++){
//            for($j=1;$j<=$i;$j++){
//                $string .= $i;
//            }
//            $string .='<br/>';
//        }
//        echo $string;
    }


    function decode(&$arr) {
        $arr['password'] = str_repeat('*', strlen($arr['password']));
        $arr['card_info']['card_no'] = str_repeat('*', strlen($arr['card_info']['card_no']));
        $arr['card_info']['card_secure'] = str_repeat('*', strlen($arr['card_info']['card_secure']));
        foreach ($arr as $key => $value) {
            if (is_array($key)) {
                $this->decode($key);
            }
        }
        return $arr;

    }

    public function secuData(&$data, $field = ['password', 'card_no', 'card_secure'])
    {
        foreach ($data as $key => &$val) {
            if(is_array($val)){
                $this->secuData($val);
            }else{
                if (in_array($key, $field)) {
                    $val = str_repeat('*', strlen($val));
                }
            }
        }
        return $data;
    }

    public function encryptStr($length)
    {
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= '*';
        }
        return $string;
    }
}
