<?php

namespace App\Http\Controllers\Frontend\V1;

use App\Http\Controllers\Controller;
use App\Models\Blog\Blog;
use App\Models\Categories\Category;
use App\Models\Contact;
use Artesaos\SEOTools\Facades\SEOMeta;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function newsfeed()
    {
        $users[] = auth()->user();
        return view('frontend.v1.index');
    }
    public function index()
    {

        SEOMeta::setTitle('Công ty tư vấn du học BKT Education');
        SEOMeta::setDescription('Công ty tư vấn du học BKT Education');
        //SEOMeta::addKeyword(['key1', 'key2', 'key3']);

        $cat_news = Category::find(9);
        $blog_news = Blog::where('active',1)
            ->where('category_id',$cat_news->id)
            ->orderBy('created_at','desc')
            ->limit(4)
            ->get();

        $cat_duhoc = Category::find(1);
        $blog_duhoc = Blog::where('active',1)
            ->where('category_id',$cat_duhoc->id)
            ->orderBy('created_at','desc')
            ->limit(4)
            ->get();

        $cat_duhocchaua = Category::find(2);
        $blog_duhocchaua = Blog::where('active',1)
            ->where('category_id',$cat_duhocchaua->id)
            ->orderBy('created_at','desc')
            ->limit(4)
            ->get();

        $cat_duhocchauau = Category::find(3);
        $blog_duhocchauau = Blog::where('active',1)
            ->where('category_id',$cat_duhocchauau->id)
            ->orderBy('created_at','desc')
            ->limit(4)
            ->get();

        $cat_duhocchaumy = Category::find(4);
        $blog_duhocchaumy = Blog::where('active',1)
            ->where('category_id',$cat_duhocchaumy->id)
            ->orderBy('created_at','desc')
            ->limit(4)
            ->get();

        $cat_duhocchauuc = Category::find(5);
        $blog_duhocchauuc = Blog::where('active',1)
            ->where('category_id',$cat_duhocchauuc->id)
            ->orderBy('created_at','desc')
            ->limit(4)
            ->get();


        $data['cat_news'] = $cat_news;
        $data['blog_news'] = $blog_news;

        $data['cat_duhoc'] = $cat_duhoc;
        $data['blog_duhoc'] = $blog_duhoc;

        $data['cat_duhocchaua'] = $cat_duhocchaua;
        $data['blog_duhocchaua'] = $blog_duhocchaua;

        $data['cat_duhocchauau'] = $cat_duhocchauau;
        $data['blog_duhocchauau'] = $blog_duhocchauau;

        $data['cat_duhocchaumy'] = $cat_duhocchaumy;
        $data['blog_duhocchaumy'] = $blog_duhocchaumy;

        $data['cat_duhocchauuc'] = $cat_duhocchauuc;
        $data['blog_duhocchauuc'] = $blog_duhocchauuc;

        return view('frontend.v1.home.index',$data);
    }

    public function storeContact(Request $request){
        $full_name = $request->get('full_name');
        $email = $request->get('email');
        $phone = $request->get('phone');
        $birthday = $request->get('birthday');
        $address = $request->get('address');
        $choose = $request->get('chooose',0);
        $description = $request->get('description');

        if($birthday != ''){
            $birthday = Carbon::createFromFormat('d/m/Y',$birthday)->timestamp;
        }

        if($choose == ''){
            $choose = 0;
        }

        $data = [
            'full_name' => $full_name,
            'email' => $email,
            'phone' => $phone,
            'birthday' => $birthday,
            'address' => $address,
            'country_type' => $choose,
            'description' => $description,
        ];

        $save = Contact::insert($data);
        if($save){
            return back()->withFlashSuccess('Đăng ký thành công');
        }
    }
}
