<?php


namespace App\Http\Controllers\Frontend\V1;


use App\Http\Controllers\Controller;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    protected $model;

    public function __construct(Notification $notification)
    {
        $this->model = $notification;
    }

    public function index()
    {
        return view('frontend.v1.notification.index');
    }

    public function markAsRead(Request $request)
    {
        $id = $request->get('id');
        $notification = $this->model->find($id);
        if ($notification) {
            $notification->update(['read_at' => Carbon::now()]);
            return response()->json([
                'success' => true,
                'message' => 'Cập nhật thành công',
                'notification' => $notification,
                'totalNotificationUnread' => auth()->user()->unreadNotifications->count()
            ]);
        } else {
            return response()->json(['message' => 'Có lỗi xảy ra']);
        }
    }

    public function markAsReadAll()
    {
        auth()->user()->unreadNotifications->markAsRead();
    }

    public function get()
    {
        $data = auth()->user()->notifications()->paginate(10);
        return response()->json($data);
    }
}
