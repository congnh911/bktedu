<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Auth\UserRepository;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;

/**
 * Class ProfileController.
 */
class ProfileController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function update(UpdateProfileRequest $request)
    {
        $output = $this->userRepository->update(
            $request->user()->id,
            $request->only('first_name', 'last_name', 'email','phone', 'note', 'birthday')
//            $request->has('avatar_location') ? $request->file('avatar_location') : false
        );
        $user = $this->userRepository->getById($request->user()->id);
        // E-mail address was updated, user has to reconfirm
        if (is_array($output) && $output['email_changed']) {
            auth()->logout();

            return response()->json([
                'success' => true,
                'message' => __('strings.frontend.user.email_changed_notice'),
                'user' => $user,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => __('strings.frontend.user.profile_updated'),
            'user' => $user,
        ]);
    }

    public function popupEditAccount()
    {

        $data['user'] = auth()->user();

        return view('frontend.v1.user.modal.edit', $data);
    }
}
