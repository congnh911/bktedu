<?php


namespace App\Http\Controllers\Frontend\V1;


use App\Http\Controllers\Controller;

class BookmarkController extends Controller
{

    public function index(){
        return view('frontend.v1.bookmark.index');
    }
}
