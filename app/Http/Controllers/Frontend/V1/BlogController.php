<?php


namespace App\Http\Controllers\Frontend\V1;


use App\Http\Controllers\Controller;
use App\Models\Blog\Blog;
use App\Models\Categories\Category;
use App\Repositories\Frontend\Comment\CommentRepository;
use Artesaos\SEOTools\Facades\SEOMeta;
use Illuminate\Http\Request;
use function Sodium\increment;

class BlogController extends Controller
{

    protected $blog;

    public function __construct(CommentRepository $blogRepository)
    {
        $this->blog = $blogRepository;
    }

    public function detail(Blog $blog)
    {

        SEOMeta::setTitle($blog->name);
        SEOMeta::setDescription($blog->description);
        //SEOMeta::addKeyword(['key1', 'key2', 'key3']);

        $blogRelated = Blog::where('category_id', $blog->category_id)->where('active',1)->orderByRaw("RAND()")->limit(3)->get();
        $blog->increment('views', 1);
        $data['blog'] = $blog;
        $data['category_id'] = $blog->category_id;
        $data['blogRelated'] = $blogRelated;

        return view('frontend.v1.blog.detail', $data);
    }

    public function category(Category $category)
    {
        SEOMeta::setTitle($category->name);
        SEOMeta::setDescription($category->description);
        $listBlog = Blog::where('category_id', $category->id)->where('active',1)->orderBy('created_at', 'desc')->paginate();
        $data['listBlog'] = $listBlog;
        $data['category'] = $category;
        return view('frontend.v1.blog.list', $data);
    }

    public function getData(Request $request)
    {
        $input = $request->all();
        return $this->blog->getData($input);
    }

}
