<?php

namespace App\Http\Resources\Comment;

use App\Http\Resources\User\UserResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    public $preserveKeys = true;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];

        if (isset($this->content)) {
            $data['content'] = $this->content;
        }
        if (isset($this->active)) {
            $data['active'] = $this->active;
        }
        if (isset($this->status)) {
            $data['status'] = $this->status;
        }
        if (isset($this->type)) {
            $data['type'] = $this->type;
        }
        if (isset($this->parent_id)) {
            $data['parent_id'] = $this->parent_id;
        }
        if (isset($this->created_at)) {
            $data['created_at'] = Carbon::parse($this->created_at)->diffForHumans();
        }
        if (isset($this->creator_id)) {
            $data['creator'] = UserResource::make($this->creator);
        }

        return $data;
    }
}
