<?php

namespace App\Console\Commands\Blog;


use App\Models\Blog\Blog;
use App\Models\Categories\Category;
use App\Repositories\Backend\Blog\BlogRepository;
use Goutte\Client;
use Illuminate\Console\Command;
use Symfony\Component\HttpClient\HttpClient;

class ScraperBlogBKT extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:blog_bkt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lấy dữ liệu blog BKT';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $blog;

    public function __construct(BlogRepository $blogRepository)
    {
        parent::__construct();
        $this->blog = $blogRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $categories = [
            'http://atlantic.edu.vn/hoc-bong-du-hoc',
            'http://atlantic.edu.vn/du-hoc-chau-a/',
            'http://atlantic.edu.vn/du-hoc-chau-au/',
            'http://atlantic.edu.vn/du-hoc-chau-my/',
            'http://atlantic.edu.vn/du-hoc-chau-uc/',
            'http://atlantic.edu.vn/giao-luu-van-hoa/',
            'http://atlantic.edu.vn/trung-tam-ngoai-ngu/',
        ];

        foreach ($categories as $key => $url) {
            $slugCategory = str_replace('/', '', parse_url($url)['path']);
            for ($i = 2; $i < 20; $i++) {
                self::ScraperBlogList($url . '/page/' . $i, $slugCategory);
            }
        }

    }

    public function ScraperBlogList($url, $slugCategory)
    {
        $client = new Client(HttpClient::create(['timeout' => 60]));
        $crawler = $client->request('GET', $url);

        $links = $crawler->filter('h2.entry-title a')->each(function ($node) {
            return $node->attr('href');
        });

        foreach ($links as $key => $link) {
            $detail = $client->request('GET', $link);

            $title = $detail->filter('h1.entry-title')->each(function ($node) {
                return $node->text();
            });
            $description = $detail->filter('div.entry-content strong')->each(function ($node) {
                return $node->text();
            });
            $summary = $detail->filter('div.entry-content')->each(function ($node) {
                return $node->html();
            });
            $images = $detail->filter('div.entry-content img')->each(function ($node) {
                return $node->attr('src');
            });

            $avatar = !empty($images) ? str_replace('.info', '.vn', array_first($images)) : '';

            $title = !empty($title) ? array_first($title) : '';
            $description = !empty($description) ? array_first($description) : '';
            $description = strip_tags(str_replace('(VoThuat.vn) – ', '', $description));
            $summary = !empty($summary) ? array_first($summary) : '';
            $summary = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $summary);

            $checkPost = Blog::where('name', $title)->first();

            $categorySlug = Category::where('slug', $slugCategory)->first();

            if (empty($checkPost)) {
                $data = [
                    'name' => $title,
                    'slug' => str_slug($title),
                    'description' => $description,
                    'type' => 0,
                    'active' => 1,
                    'creator_id' => 1,
                    'views' => 0
                ];
                if (!empty($categorySlug)) {
                    $data['category_id'] = [$categorySlug->id];
                } else {
                    $data['category_id'] = [0];
                }

                $blog = $this->blog->create($data);
                if (!empty($blog)) {
                    $content = [];
                    $content['model_id'] = $blog->id;
                    $content['model_type'] = $blog->getMorphClass();
                    $content['type'] = config('blog.content_type.main.id');
                    $content['name'] = config('blog.content_type.main.name');
                    $content['content'] = $summary;
                    $content['creator_id'] = 1;
                    $content['active'] = 1;
                    $blog->contents()->firstOrCreate($content);

                }
            } else {
                $checkPost->update(['avatar' => $avatar, 'description' => $description]);
            }

        }
    }
}

