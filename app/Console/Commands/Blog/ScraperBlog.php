<?php

namespace App\Console\Commands\Blog;


use App\Models\Blog\Blog;
use App\Repositories\Backend\Blog\BlogRepository;
use Goutte\Client;
use Illuminate\Console\Command;
use Symfony\Component\HttpClient\HttpClient;

class ScraperBlog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scraper:blog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lấy dữ liệu blog';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    protected $blog;

    public function __construct(BlogRepository $blogRepository)
    {
        parent::__construct();
        $this->blog = $blogRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $url = 'http://vothuat.vn/cac-mon-phai/vinh-xuan';
        for($i=2;$i<20;$i++){
            self::ScraperBlogList($url.'/page/'.$i);
        }
    }

    public function ScraperBlogList($url)
    {
        $client = new Client(HttpClient::create(['timeout' => 60]));
        $crawler = $client->request('GET', $url);

        $links = $crawler->filter('h3.entry-title.td-module-title a')->each(function ($node) {
            return $node->attr('href');
        });

        foreach ($links as $key => $link) {
            $detail = $client->request('GET', $link);

            $title = $detail->filter('h1.entry-title')->each(function ($node) {
                return $node->text();
            });
            $description = $detail->filter('div.td-post-content p strong')->each(function ($node) {
                return $node->text();
            });
            $summary = $detail->filter('div.td-post-content')->each(function ($node) {
                return $node->html();
            });
            $images = $detail->filter('div.td-ss-main-content img')->each(function ($node) {
                return $node->attr('src');
            });

            $avatar = !empty($images) ? str_replace('.info','.vn',array_first($images)) : '';

            $title = !empty($title) ? array_first($title) : '';
            $description = !empty($description) ? array_first($description) : '';
            $description = str_replace('(VoThuat.vn) – ', '', $description);
            $summary = !empty($summary) ? array_first($summary) : '';
            $summary = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $summary);

            $checkPost = Blog::where('name', $title)->first();
            if (empty($checkPost)) {
                $data = [
                    'name' => $title,
                    'slug' => str_slug($title),
                    'description' => $description,
                    'type' => 0,
                    'active' => 1,
                    'category_id' => [2],
                    'creator_id' => 1,
                    'views' => 0
                ];

                $blog = $this->blog->create($data);
                if (!empty($blog)) {
                    $content = [];
                    $content['model_id'] = $blog->id;
                    $content['model_type'] = $blog->getMorphClass();
                    $content['type'] = config('blog.content_type.main.id');
                    $content['name'] = config('blog.content_type.main.name');
                    $content['content'] = $summary;
                    $content['creator_id'] = 1;
                    $content['active'] = 1;
                    $blog->contents()->firstOrCreate($content);

                }
            }else{
                $checkPost->update(['avatar' => $avatar]);
            }

        }
    }

}
