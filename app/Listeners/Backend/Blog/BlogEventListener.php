<?php

namespace App\Listeners\Backend\Auth\Blog;

/**
 * Class BlogEventListener.
 */
class BlogEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        \Log::info('Blog Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        \Log::info('Blog Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        \Log::info('Blog Deleted');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        \Log::info('Blog Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        \Log::info('Blog Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        \Log::info('Blog Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        \Log::info('Blog Restored');
    }


    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Blog\SlideCreated::class,
            'App\Listeners\Backend\Blog\BlogEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Blog\BlogUpdated::class,
            'App\Listeners\Backend\Blog\BlogEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Blog\BlogDeleted::class,
            'App\Listeners\Backend\Blog\BlogEventListener@onDeleted'
        );

        $events->listen(
            \App\Events\Backend\Blog\SlideDeactivated::class,
            'App\Listeners\Backend\Blog\BlogEventListener@onDeactivated'
        );

        $events->listen(
            \App\Events\Backend\Blog\BlogReactivated::class,
            'App\Listeners\Backend\Blog\BlogEventListener@onReactivated'
        );

        $events->listen(
            \App\Events\Backend\Blog\BlogPermanentlyDeleted::class,
            'App\Listeners\Backend\Blog\BlogEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            \App\Events\Backend\Blog\BlogRestored::class,
            'App\Listeners\Backend\Blog\BlogEventListener@onRestored'
        );
    }
}
