<?php

namespace App\Listeners\Backend\Auth\Category;

/**
 * Class CategoryEventListener.
 */
class CategoryEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        \Log::info('Category Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        \Log::info('Category Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        \Log::info('Category Deleted');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        \Log::info('Category Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        \Log::info('Category Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        \Log::info('Category Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        \Log::info('Category Restored');
    }


    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Category\ProductCreated::class,
            'App\Listeners\Backend\Category\CategoryEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Category\ProductUpdated::class,
            'App\Listeners\Backend\Category\CategoryEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Category\ProductDeleted::class,
            'App\Listeners\Backend\Category\CategoryEventListener@onDeleted'
        );

        $events->listen(
            \App\Events\Backend\Category\ProductDeactivated::class,
            'App\Listeners\Backend\Category\CategoryEventListener@onDeactivated'
        );

        $events->listen(
            \App\Events\Backend\Category\ProductReactivated::class,
            'App\Listeners\Backend\Category\CategoryEventListener@onReactivated'
        );

        $events->listen(
            \App\Events\Backend\Category\ProductPermanentlyDeleted::class,
            'App\Listeners\Backend\Category\CategoryEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            \App\Events\Backend\Category\ProductRestored::class,
            'App\Listeners\Backend\Category\CategoryEventListener@onRestored'
        );
    }
}
