<?php

namespace App\Listeners\Backend\Auth\Permission;

/**
 * Class PermissionEventListener.
 */
class PermissionEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        \Log::info('Permission Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        \Log::info('Permission Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        \Log::info('Permission Deleted');
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param \Illuminate\Events\Dispatcher $events
     */
    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Auth\Permission\PermissionCreated::class,
            'App\Listeners\Backend\Auth\Permission\PermissionEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Auth\Permission\PermissionUpdated::class,
            'App\Listeners\Backend\Auth\Permission\PermissionEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Auth\Permission\PermissionDeleted::class,
            'App\Listeners\Backend\Auth\Permission\PermissionEventListener@onDeleted'
        );
    }
}
