<?php

namespace App\Listeners\Backend\Auth\Product;

/**
 * Class ProductEventListener.
 */
class ProductEventListener
{
    /**
     * @param $event
     */
    public function onCreated($event)
    {
        \Log::info('Product Created');
    }

    /**
     * @param $event
     */
    public function onUpdated($event)
    {
        \Log::info('Product Updated');
    }

    /**
     * @param $event
     */
    public function onDeleted($event)
    {
        \Log::info('Product Deleted');
    }

    /**
     * @param $event
     */
    public function onDeactivated($event)
    {
        \Log::info('Product Deactivated');
    }

    /**
     * @param $event
     */
    public function onReactivated($event)
    {
        \Log::info('Product Reactivated');
    }

    /**
     * @param $event
     */
    public function onPermanentlyDeleted($event)
    {
        \Log::info('Product Permanently Deleted');
    }

    /**
     * @param $event
     */
    public function onRestored($event)
    {
        \Log::info('Product Restored');
    }


    public function subscribe($events)
    {
        $events->listen(
            \App\Events\Backend\Product\ProductCreated::class,
            'App\Listeners\Backend\Product\ProductEventListener@onCreated'
        );

        $events->listen(
            \App\Events\Backend\Product\ProductUpdated::class,
            'App\Listeners\Backend\Product\ProductEventListener@onUpdated'
        );

        $events->listen(
            \App\Events\Backend\Product\BlogDeleted::class,
            'App\Listeners\Backend\Product\ProductEventListener@onDeleted'
        );

        $events->listen(
            \App\Events\Backend\Product\ProductDeactivated::class,
            'App\Listeners\Backend\Product\ProductEventListener@onDeactivated'
        );

        $events->listen(
            \App\Events\Backend\Product\ProductReactivated::class,
            'App\Listeners\Backend\Product\ProductEventListener@onReactivated'
        );

        $events->listen(
            \App\Events\Backend\Product\BlogPermanentlyDeleted::class,
            'App\Listeners\Backend\Product\ProductEventListener@onPermanentlyDeleted'
        );

        $events->listen(
            \App\Events\Backend\Product\ProductRestored::class,
            'App\Listeners\Backend\Product\ProductEventListener@onRestored'
        );
    }
}
