<?php

use App\Helpers\General\HtmlHelper;

if (!function_exists('style')) {
    /**
     * @param       $url
     * @param array $attributes
     * @param null $secure
     *
     * @return mixed
     */
    function style($url, $attributes = [], $secure = null)
    {
        return resolve(HtmlHelper::class)->style($url, $attributes, $secure);
    }
}

if (!function_exists('script')) {
    /**
     * @param       $url
     * @param array $attributes
     * @param null $secure
     *
     * @return mixed
     */
    function script($url, $attributes = [], $secure = null)
    {
        return resolve(HtmlHelper::class)->script($url, $attributes, $secure);
    }
}

if (!function_exists('form_cancel')) {
    /**
     * @param        $cancel_to
     * @param        $title
     * @param string $classes
     *
     * @return mixed
     */
    function form_cancel($cancel_to, $title, $classes = 'btn btn-danger btn-sm')
    {
        return resolve(HtmlHelper::class)->formCancel($cancel_to, $title, $classes);
    }
}

if (!function_exists('form_submit')) {
    /**
     * @param        $title
     * @param string $classes
     *
     * @return mixed
     */
    function form_submit($title, $classes = 'btn btn-success btn-sm pull-right')
    {
        return resolve(HtmlHelper::class)->formSubmit($title, $classes);
    }
}


if (!function_exists('makeLink')) {

    function makeLink($str)
    {
        $reg_exUrl = "/(http|https|ftp|ftps)\\:\\/\\/[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(\\/\\S*)?/";
        $urls = array();
        $urlsToReplace = array();
        if (preg_match_all($reg_exUrl, $str, $urls)) {
            $numOfMatches = count($urls[0]);
            $numOfUrlsToReplace = 0;
            for ($i = 0; $i < $numOfMatches; $i++) {
                $alreadyAdded = false;
                $numOfUrlsToReplace = count($urlsToReplace);
                for ($j = 0; $j < $numOfUrlsToReplace; $j++) {
                    if ($urlsToReplace[$j] == $urls[0][$i]) {
                        $alreadyAdded = true;
                    }
                }
                if (!$alreadyAdded) {
                    array_push($urlsToReplace, $urls[0][$i]);
                }
            }
            $numOfUrlsToReplace = count($urlsToReplace);
            for ($i = 0; $i < $numOfUrlsToReplace; $i++) {
                $str = str_replace($urlsToReplace[$i], "" . $urlsToReplace[$i] . " ", $str);
            }
            return $str;
        } else {
            return $str;
        }

    }
}

if (!function_exists('get_user_timezone')) {

    /**
     * @return string
     */
    function get_user_timezone()
    {
        if (auth()->user()) {
            return auth()->user()->timezone;
        }

        return 'UTC';
    }
}

if (!function_exists('addFilter')) {
    /**
     * @param $model
     * @param array $filter
     * @return mixed
     */
    function addFilter($model, $filter = [])
    {
        if (!empty($filter)) {
            foreach ($filter as $key => $value) {
                if ($value == '') {
                    unset($filter[$key]);
                }

                //Trường hợp nếu param truyền vào dạng ['column' => ['operator'=>'>','value'=> 1]]
                if (is_array($value)) {
                    $op = array_get($value, 'operator');
                    $val = array_get($value, 'value');
                    $column = $key;
                    if ($op == 'IN') {
                        $model = $model->whereIn($column, $val);
                    } else if ($op == 'NOT IN') {
                        $model = $model->whereNotIn($column, $val);
                    } elseif ($op == 'BETWEEN') {
                        $model = $model->whereBetween($column, $val);
                    } elseif ($op == 'NOT BETWEEN') {
                        $model = $model->whereNotBetween($column, $val);
                    } elseif ($op == 'OR') {
                        $model = $model->orWhere($column, $val);
                    } elseif ($op == 'OR-BETWEEN') {
                        $model = $model->orWhereBetween($column, $val);
                    } elseif ($op == 'RAW') {
                        //voi whereRaw chi lay value
                        $model = $model->whereRaw($val);
                    } else {
                        $model = $model->where($column, $op, $val);
                    }
                } else {
                    //Trường hợp truyền vào simple ['column'=>'value']
                    $model = $model->where($key, $value);
                }
            }
        }

        return $model;
    }
}

if (!function_exists('array_add_dot')) {

    /**
     * Có thêm tùy chọn có giữ key (abc.dce)
     * Phục vụ tạo filter join cần dạng (table.column)
     */
    function array_add_dot($array, $key, $value, $dot = false)
    {

        if (is_null(\Illuminate\Support\Arr::get($array, $key))) {

            if (is_null($key)) {
                return $array = $value;
            }

            if ($dot) {
                $keys = explode('.', $key);
            } else {
                $keys = [$key];
            }

            while (count($keys) > 1) {
                $key = array_shift($keys);

                // If the key doesn't exist at this depth, we will just create an empty array
                // to hold the next value, allowing us to create the arrays to hold final
                // values at the correct depth. Then we'll keep digging into the array.
                if (!isset($array[$key]) || !is_array($array[$key])) {
                    $array[$key] = [];
                }

                $array = &$array[$key];
            }

            $array[array_shift($keys)] = $value;

            return $array;
        }

        return $array;

    }
}

if (!function_exists('createUrlFilter')) {

    /**
     * @param $url
     * @param $query
     * @return string
     */
    function createUrlFilter($url, $query)
    {
        $params = [];
        $new_query = "";

        if ($query != '') {
            $input_all = explode('&', $query);
            foreach ($input_all as $param) {
                if ($param != '') {
                    list($name, $value) = explode('=', $param, 2);
                    $params[$name] = $value;
                }
            }
        }

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                if ($new_query == "") {
                    $new_query = $new_query . '?' . $key . '=' . $param;
                } else {
                    $new_query = $new_query . '&' . $key . '=' . $param;
                }
            }
        }

        return $url . $new_query;
    }
}

if (!function_exists('parseQueryString')) {

    function parseQueryString($query, $ex = '&')
    {
        $params = [];
        if ($query != '') {
            $input_all = explode($ex, $query);
            foreach ($input_all as $param) {
                if ($param != '') {
                    list($name, $value) = explode('=', $param, 2);
                    $params[$name] = $value;
                }
            }
        }

        return $params;
    }
}

if (!function_exists('cleanSpecial')) {
    /**
     * Xóa 1 số ký tự đặc biệt
     **/
    function cleanSpecial($string)
    {
        $string = str_replace(' ', '', $string);
        $string = str_replace('-', '', $string);
        $string = str_replace('_', '', $string);
        $string = str_replace('.', '', $string);
        $string = str_replace(',', '', $string);
        return trim($string);
    }
}

if (!function_exists('cleanPhone')) {

    /**
     * Xóa ký tự đặc biệt khi tìm kiếm phone
     **/
    function cleanPhone($string)
    {
        return cleanSpecial($string);
    }
}

if (!function_exists('cleanEmail')) {
    /**
     * Xóa ký tự đặc biệt khi tìm kiếm email
     **/
    function cleanEmail($string)
    {
        $string = str_replace(' ', '', $string);
        return trim($string);
    }
}


if (!function_exists('substrwords')) {
    function substrwords($text, $maxchar, $end='...') {
        if (strlen($text) > $maxchar || $text == '') {
            $words = preg_split('/\s/', $text);
            $output = '';
            $i      = 0;
            while (1) {
                $length = strlen($output)+strlen($words[$i]);
                if ($length > $maxchar) {
                    break;
                }
                else {
                    $output .= " " . $words[$i];
                    ++$i;
                }
            }
            $output .= $end;
        }
        else {
            $output = $text;
        }
        return $output;
    }
}
