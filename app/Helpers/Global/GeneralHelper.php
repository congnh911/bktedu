<?php

if (!function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }
}

if (!function_exists('gravatar')) {
    /**
     * Access the gravatar helper.
     */
    function gravatar()
    {
        return app('gravatar');
    }
}

if (!function_exists('home_route')) {
    /**
     * Return the route to the "home" page depending on authentication/authorization status.
     *
     * @return string
     */
    function home_route()
    {
        if (auth()->check()) {
            if (auth()->user()->can('view backend')) {
                return 'admin.dashboard';
            }

            return 'frontend.v1.user.dashboard';
        }

        return 'frontend.v1.index';
    }
}

if (!function_exists('cleanPhone')) {

    /**
     * Xóa ký tự đặc biệt khi tìm kiếm phone
     **/
    function cleanPhone($string)
    {
        return cleanSpecial($string);
    }
}

if (!function_exists('cleanEmail')) {
    /**
     * Xóa ký tự đặc biệt khi tìm kiếm email
     **/
    function cleanEmail($string)
    {
        $string = str_replace(' ', '', $string);
        return trim(strtolower($string));
    }
}

if (!function_exists('cleanSpecial')) {
    /**
     * Xóa 1 số ký tự đặc biệt
     **/
    function cleanSpecial($string)
    {
        $string = str_replace(' ', '', $string);
        $string = str_replace('-', '', $string);
        $string = str_replace('_', '', $string);
        $string = str_replace('.', '', $string);
        $string = str_replace(',', '', $string);
        return trim($string);
    }
}

if (!function_exists('cleanPrice')) {
    /**
     * Xóa 1 số ký tự đặc biệt
     **/
    function cleanPrice($string)
    {
        $string = str_replace(',', '', $string);
        return trim($string);
    }
}



if (!function_exists('array_add_dot')) {

    /**
     * Có thêm tùy chọn có giữ key (abc.dce)
     * Phục vụ tạo filter join cần dạng (table.column)
     */
    function array_add_dot($array, $key, $value, $dot = false)
    {

        if (is_null(\Illuminate\Support\Arr::get($array, $key))) {

            if (is_null($key)) {
                return $array = $value;
            }

            if ($dot) {
                $keys = explode('.', $key);
            } else {
                $keys = [$key];
            }

            while (count($keys) > 1) {
                $key = array_shift($keys);

                // If the key doesn't exist at this depth, we will just create an empty array
                // to hold the next value, allowing us to create the arrays to hold final
                // values at the correct depth. Then we'll keep digging into the array.
                if (!isset($array[$key]) || !is_array($array[$key])) {
                    $array[$key] = [];
                }

                $array = &$array[$key];
            }

            $array[array_shift($keys)] = $value;

            return $array;
        }

        return $array;

    }
}

if (!function_exists('addFilter')) {

    function addFilter($model, $filter = [])
    {
        if (!empty($filter)) {
            foreach ($filter as $key => $value) {
                if ($value == '') {
                    unset($filter[$key]);
                }

                //Trường hợp nếu param truyền vào dạng ['column' => ['operator'=>'>','value'=> 1]]
                if (is_array($value)) {
                    $op = array_get($value, 'operator');
                    $val = array_get($value, 'value');
                    $column = $key;
                    if ($op == 'IN') {
                        $model = $model->whereIn($column, $val);
                    } else if ($op == 'NOT IN' || $op == 'NOT-IN') {
                        $model = $model->whereNotIn($column, $val);
                    } elseif ($op == 'BETWEEN') {
                        $model = $model->whereBetween($column, $val);
                    } elseif ($op == 'NOT BETWEEN') {
                        $model = $model->whereNotBetween($column, $val);
                    } elseif ($op == 'OR') {
                        $model = $model->orWhere($column, $val);
                    } elseif ($op == 'OR-AND') {
                        $attributes = $val;
                        $model->where(function ($model) use ($attributes) {
                            foreach ($attributes as $key => $value) {
                                $model->orWhere($key, $value);
                            }
                            return $model;
                        });
                    } elseif ($op == 'OR-BETWEEN') {
                        $model = $model->orWhereBetween($column, $val);
                    } elseif ($op == 'RAW') {
                        //voi whereRaw chi lay value
                        $model = $model->whereRaw($val);
                    } else {
                        $model = $model->where($column, $op, $val);
                    }
                } else {
                    //Trường hợp truyền vào simple ['column'=>'value']
                    $model = $model->where($key, $value);
                }
            }
        }

        return $model;
    }
}

if (!function_exists('parseQueryString')) {

    function parseQueryString($query, $ex = '&')
    {
        $params = [];
        if ($query != '') {
            $input_all = explode($ex, $query);
            foreach ($input_all as $key => $param) {
                if ($param != '') {
                    if (count(explode('=', $param, 2)) < 2) {
                        $param = $input_all[$key - 1] . '&' . $param;
                    }

                    if ($param != '') {
                        list($name, $value) = explode('=', $param, 2);
                        $params[$name] = $value;
                    }
                }
            }
        }

        return $params;
    }
}

if (!function_exists('createUrlFilter')) {

    function createUrlFilter($url, $query)
    {
        $params = [];
        $new_query = "";

        if ($query != '') {
            $input_all = explode('&', $query);
            foreach ($input_all as $param) {
                if ($param != '') {
                    list($name, $value) = explode('=', $param, 2);
                    $params[$name] = $value;
                }
            }
        }

        if (!empty($params)) {
            foreach ($params as $key => $param) {
                if ($new_query == "") {
                    $new_query = $new_query . '?' . $key . '=' . $param;
                } else {
                    $new_query = $new_query . '&' . $key . '=' . $param;
                }
            }
        }

        return $url . $new_query;
    }
}
