<?php

use Illuminate\Database\Seeder;
use App\Models\Comment\Comment;
/**
 * Class CommentTableSeeder.
 */
class CommentTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->disableForeignKeys();

        factory(Comment::class,200)->create();

        $this->enableForeignKeys();
    }
}
