<?php

use Illuminate\Database\Seeder;
use App\Models\Blog\Blog;

/**
 * Class BlogTableSeeder.
 */
class BlogTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        factory(Blog::class,200)->create();

        $this->enableForeignKeys();
    }
}
