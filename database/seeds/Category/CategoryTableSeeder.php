<?php

use Illuminate\Database\Seeder;
use App\Models\Categories\Category;
/**
 * Class CategoryTableSeeder.
 */
class CategoryTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        factory(Category::class,100)->create();

        $this->enableForeignKeys();
    }
}
