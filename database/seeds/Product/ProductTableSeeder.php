<?php

use Illuminate\Database\Seeder;
use App\Models\Product\Product;

/**
 * Class ProductTableSeeder.
 */
class ProductTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        factory(Product::class,1000)->create();

        $this->enableForeignKeys();
    }
}
