<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator;
use App\Models\Blog\Blog;
use App\Models\Categories\Category;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Blog::class, function (Generator $faker) {
    $category = Category::where('type',config('category.type.blog.id'))
        ->orderBy(DB::raw('RAND()'))->first();
    $name = $faker->unique()->name;
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'description' => $faker->text,
        'seo_title' => $faker->name,
        'seo_description' => $faker->text,
        'seo_keyword' => $faker->word,
        'category_id' => $category->id,
        'creator_id' => 1,
        'active' => true,
    ];
});

$factory->state(Blog::class, 'active', function () {
    return [
        'active' => true,
    ];
});
$factory->state(Blog::class, 'active', function () {
    return [
        'creator_id' => 1,
    ];
});

