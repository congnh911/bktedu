<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator;
use App\Models\Product\Product;
use App\Models\Categories\Category;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Product::class, function (Generator $faker) {
    $category = Category::where('type',config('category.type.product.id'))
        ->orderBy(DB::raw('RAND()'))->first();
    $name = $faker->unique()->name;
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'description' => $faker->text,
        'regular_price' => round(cleanPrice($faker->numberBetween(1000,99999999)),2),
        'sale_price' => round(cleanPrice($faker->numberBetween(1000,9999999)),2),
        'discount' => round(cleanPrice($faker->numberBetween(0,100)),2),
        'seo_title' => $faker->name,
        'seo_description' => $faker->text,
        'seo_keyword' => $faker->word,
        'category_id' => $category->id,
        'creator_id' => 1,
        'active' => true,
    ];
});

$factory->state(Product::class, 'active', function () {
    return [
        'active' => true,
    ];
});
$factory->state(Product::class, 'active', function () {
    return [
        'creator_id' => 1,
    ];
});

$factory->state(Product::class, 'active', function () {
    return [
        'slug' => str_slug(),
    ];
});

