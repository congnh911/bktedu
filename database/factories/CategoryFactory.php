<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator;
use App\Models\Categories\Category;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Category::class, function (Generator $faker) {
    $name = $faker->unique()->name;
    return [
        'name' => $name,
        'slug' => str_slug($name),
        'name_en' => $faker->unique()->name,
        'description' => $faker->text(255),
        'seo_title' => $faker->name,
        'seo_description' => $faker->text(255),
        'seo_keyword' => $faker->word,
        'creator_id' => 1,
        'type' => array_random(collect(array_values(config('category.type')))->pluck('id')->all()),
        'active' => 1,
    ];
});

$factory->state(Category::class, 'active', function () {
    return [
        'active' => 1,
    ];
});

$factory->state(Category::class, 'active', function () {
    return [
        'creator_id' => 1,
    ];
});

