<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator;
use App\Models\Comment\Comment;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Comment::class, function (Generator $faker) {
    return [
        'content' => $faker->text,
        'model_id' => $faker->randomNumber(),
        'model_type' => \App\Models\Blog\Blog::class,
        'creator_id' => 1,
        'parent_id' => 0,
        'active' => 1,
    ];
});

