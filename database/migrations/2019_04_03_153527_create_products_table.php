<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->default(0);
            $table->bigInteger('creator_id')->default(0);
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->text('description_en')->nullable();
            $table->float('regular_price',11,1)->default(0);
            $table->float('sale_price',11,1)->default(0);
            $table->float('discount',11,1)->default(0);
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->tinyInteger('highlight')->default(0);
            $table->integer('order')->default(0);
            $table->string('disk')->nullable()->comment('o dia');
            $table->string('avatar')->nullable()->comment('duong dan luu');
            $table->string('seo_title')->nullable();
            $table->string('seo_keyword')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('note')->nullable();
            $table->integer('views')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
