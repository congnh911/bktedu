<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('full_name')->nullable();
            $table->bigInteger('user_id');
            $table->integer('code')->default(0);
            $table->integer('date_of_birth')->default(0);
            $table->string('home_land')->nullable()->comment('que quan');
            $table->string('identity_card')->nullable()->comment('so the cmt');
            $table->tinyInteger('gender')->default(0);
            $table->text('description')->comment('ghi thong tin noi tam tru thuong tu ....');
            $table->string('rank')->nullable()->comment('dang cap');
            $table->integer('city_id')->default(0);
            $table->integer('district_id')->default(0);
            $table->integer('department_id')->default(0)->comment('phong ban');
            $table->integer('role_id')->default(0)->comment('Vai tro, vi tri');
            $table->string('disk')->nullable();
            $table->string('path')->nullable()->comment('luu avatar');
            $table->tinyInteger('status')->default(0)->comment('trang thai');
            $table->tinyInteger('active')->default(0);
            $table->text('note')->nullable();
            $table->timestamp('time_start')->nullable()->comment('ngay ra nhap');
            $table->timestamp('time_end')->nullable()->comment('ngay out');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
