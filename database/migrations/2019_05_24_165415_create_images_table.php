<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('creator_id')->default(0);
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->string('disk')->nullable();
            $table->string('path')->nullable();
            $table->bigInteger('model_id')->nullable();
            $table->string('model_type')->nullable();
            $table->tinyInteger('order')->default(0);
            $table->tinyInteger('avatar')->default(0)->comment('set status avatar');
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
