<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->nullable()->index();
            $table->bigInteger('creator_id')->default(0);
            $table->integer('lft')->default(0);
            $table->integer('rgt')->default(0);
            $table->integer('depth')->default(0);
            $table->string('type')->nullable();
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('slug')->nullable();
            $table->string('disk')->nullable();
            $table->string('path')->nullable();
            $table->text('description')->nullable();
            $table->text('description_en')->nullable();
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('highlight')->default(0);
            $table->integer('order')->default(0);
            $table->string('seo_title')->nullable();
            $table->string('seo_keyword')->nullable();
            $table->text('seo_description')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
