<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->default(0);
            $table->bigInteger('creator_id')->default(0);
            $table->string('name')->nullable();
            $table->string('name_en')->nullable();
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->text('description_en')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('type')->default(0);
            $table->tinyInteger('active')->default(0);
            $table->tinyInteger('highlight')->default(0);
            $table->integer('order')->default(0);
            $table->string('disk')->nullable();
            $table->string('avatar')->nullable();
            $table->string('seo_title')->nullable();
            $table->string('seo_keyword')->nullable();
            $table->text('seo_description')->nullable();
            $table->text('note')->nullable();
            $table->integer('views')->default(0);
            $table->integer('role_id')->default(0)->comment('phan quyen cho nhom nao');
            $table->integer('is_publish')->default(0)->comment('trang thai public hay ko');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
