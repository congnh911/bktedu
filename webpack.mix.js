const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public')
    .copy('node_modules/tinymce','public/plugin/tinymce')
    .copy('node_modules/bootbox','public/plugin/bootbox')
    .copy('node_modules/ckeditor5','public/plugin/ckeditor')
    .copy('node_modules/photoswipe','public/plugin/photoswipe')
    .copy('node_modules/lockfixed','public/plugin/lockfixed')
    .copy('resources/js','public/js')
    .copy('resources/fonts','public/fonts')
    .copy('resources/css','public/css')
    .copy('resources/img','public/img')
    .setResourceRoot('../') // turns assets paths in css relative to css file
    .sass('resources/sass/frontend/app.scss', 'css/frontend.css')
    .sass('resources/sass/backend/app.scss', 'css/backend.css')
    .js(['resources/js/frontend/app.js',], 'js/frontend.js')
    .js([
        'resources/js/backend/before.js',
        'resources/js/backend/app.js',
        'resources/js/backend/after.js',
        'resources/js/backend/modules/category.js',
        'resources/js/backend/modules/product.js',
        'resources/js/backend/modules/blog.js',
    ], 'js/backend.js')
    .extract([
        /* Extract packages from node_modules, only those used by front and
        backend, to vendor.js */
        'jquery',
        'bootstrap',
        'popper.js',
        'bootbox',
        'axios',
        'sweetalert2',
        'lodash',
    ])
    .sourceMaps();

if (mix.inProduction()) {
    mix.version()
        .options({
            // optimize js minification process
            terser: {
                cache: true,
                parallel: true,
                sourceMap: true
            }
        });
} else {
    // Uses inline source-maps on development
    mix.webpackConfig({ devtool: 'inline-source-map' });
}
