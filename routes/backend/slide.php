<?php
use App\Http\Controllers\Backend\Slide\SlideController;

Route::group(['prefix' => 'slide', 'as' => 'slide.'], function () {

    Route::get('/', [SlideController::class, 'index'])->name('index');

    Route::get('get', [SlideController::class, 'get'])->name('get');
    Route::get('create', [SlideController::class, 'create'])->name('create');
    Route::post('store', [SlideController::class, 'store'])->name('store');

    Route::group(['prefix' => '{slide}'], function () {
        Route::get('show', [SlideController::class, 'show'])->name('show');
        Route::get('edit', [SlideController::class, 'edit'])->name('edit');
        Route::put('update', [SlideController::class, 'update'])->name('update');
        Route::patch('update', [SlideController::class, 'update'])->name('update');
        Route::post('restore', [SlideController::class, 'restore'])->name('restore');
        Route::delete('delete-permanently', [SlideController::class, 'deletePermanently'])
            ->name('delete-permanently');
        Route::patch('switch01', [SlideController::class, 'switch01'])->name('switch01');
        Route::delete('destroy', [SlideController::class, 'destroy'])->name('destroy');
    });

});
