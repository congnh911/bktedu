<?php

use App\Http\Controllers\Backend\AlbumController;

Route::group(['prefix' => 'album', 'as' => 'album.'], function () {

    Route::get('/', [AlbumController::class, 'index'])->name('index');

    Route::get('create', [AlbumController::class, 'create'])->name('create');
    Route::post('store', [AlbumController::class, 'store'])->name('store');
    Route::post('sortable', [AlbumController::class, 'sortable'])->name('sortable');

    Route::group(['prefix' => '{album}'], function () {
        Route::get('edit', [AlbumController::class, 'edit'])->name('edit');
        Route::patch('update', [AlbumController::class, 'update'])->name('update');
        Route::put('update', [AlbumController::class, 'update'])->name('update');

        Route::delete('delete', [AlbumController::class, 'delete'])->name('delete');
    });

    Route::post('is-avatar', [AlbumController::class, 'isAvatar'])->name('is_avatar');
});
