<?php
Route::group([
    'namespace' => 'File',
    'prefix'     => 'file'
], function () {
    Route::delete('{id}/delete', 'FileController@destroyFile')->name('file.delete');
    Route::get('{id}/download', 'FileController@downloadFile')->name('file.download');
    Route::get('{id}/view', 'FileController@viewFile')->name('file.view');
    Route::post('upload', 'FileController@upload')->name('file.upload');
});
