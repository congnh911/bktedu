<?php
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\CategoryGetDataController;

Route::group(['prefix' => 'category', 'as' => 'category.'], function () {

    Route::get('/', [CategoryController::class, 'index'])->name('index');
    Route::get('get', [CategoryController::class, 'get'])->name('get');
    Route::get('create', [CategoryController::class, 'create'])->name('create');
    Route::post('store', [CategoryController::class, 'store'])->name('store');
    Route::group(['prefix' => '{category}'], function () {
        Route::get('show', [CategoryController::class, 'show'])->name('show');
        Route::get('edit', [CategoryController::class, 'edit'])->name('edit');
        Route::put('update', [CategoryController::class, 'update'])->name('update');
        Route::patch('update', [CategoryController::class, 'update'])->name('update');
        Route::post('restore', [CategoryController::class, 'restore'])->name('restore');
        Route::delete('delete-permanently', [CategoryController::class, 'deletePermanently'])
            ->name('delete-permanently');
        Route::patch('switch01', [CategoryController::class, 'switch01'])->name('switch01');
        Route::delete('destroy', [CategoryController::class, 'destroy'])->name('destroy');
    });


    //get category ajax
    Route::get('get-category-product', [CategoryGetDataController::class, 'getCategoryProduct'])
        ->name('get_category_product');
    Route::get('get-category-blog', [CategoryGetDataController::class, 'getCategoryBlog'])
        ->name('get_category_blog');

});
