<?php
use App\Http\Controllers\Backend\Blog\BlogController;

Route::group(['prefix' => 'blog', 'as' => 'blog.'], function () {

    Route::get('/', [BlogController::class, 'index'])->name('index');

    Route::get('get', [BlogController::class, 'get'])->name('get');
    Route::get('create', [BlogController::class, 'create'])->name('create');
    Route::post('store', [BlogController::class, 'store'])->name('store');

    Route::group(['prefix' => '{blog}'], function () {
        Route::get('show', [BlogController::class, 'show'])->name('show');
        Route::get('edit', [BlogController::class, 'edit'])->name('edit');
        Route::put('update', [BlogController::class, 'update'])->name('update');
        Route::patch('update', [BlogController::class, 'update'])->name('update');
        Route::post('restore', [BlogController::class, 'restore'])->name('restore');
        Route::delete('delete-permanently', [BlogController::class, 'deletePermanently'])
            ->name('delete-permanently');
        Route::patch('switch01', [BlogController::class, 'switch01'])->name('switch01');
        Route::delete('destroy', [BlogController::class, 'destroy'])->name('destroy');
    });

});
