<?php
use App\Http\Controllers\Backend\AuditController;

Route::group(['as' => 'audit.'], function () {

    Route::get('get', [AuditController::class, 'get'])->name('get');

});
