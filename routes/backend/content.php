<?php

use App\Http\Controllers\Backend\ContentController;

Route::group(['prefix' => 'content', 'as' => 'content.'], function () {

    Route::get('/', [ContentController::class, 'index'])->name('index');

    Route::get('create', [ContentController::class, 'create'])->name('create');
    Route::get('popup-create', [ContentController::class, 'popupCreate'])->name('popup_create');
    Route::get('{content}/popup-edit', [ContentController::class, 'popupEdit'])->name('popup_edit');
    Route::post('store', [ContentController::class, 'store'])->name('store');

    Route::group(['prefix' => '{content}'], function () {
        Route::get('edit', [ContentController::class, 'edit'])->name('edit');
        Route::patch('update', [ContentController::class, 'update'])->name('update');
        Route::put('update', [ContentController::class, 'update'])->name('update');

        Route::delete('delete', [ContentController::class, 'delete'])->name('delete');
    });

});
