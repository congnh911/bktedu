<?php

use App\Http\Controllers\Backend\Video\VideoController;

Route::group([
    'namespace' => 'Video',
    'prefix' => 'video',
], function () {
    Route::group(['prefix' => '{id}'], function () {
        Route::delete('delete', [VideoController::class, 'destroy'])->name('video.delete');
        Route::get('download-video', [VideoController::class, 'download'])->name('video.download');
        Route::get('view-video', [VideoController::class, 'view'])->name('video.view');
        Route::patch('update', [VideoController::class, 'update'])->name('video.update');
        Route::put('update', [VideoController::class, 'update'])->name('video.update');
    });
    Route::post('upload', [VideoController::class, 'upload'])->name('video.upload');

});
