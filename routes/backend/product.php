<?php
use App\Http\Controllers\Backend\Product\ProductController;

Route::group(['prefix' => 'product', 'as' => 'product.'], function () {

    Route::get('/', [ProductController::class, 'index'])->name('index');

    Route::get('get', [ProductController::class, 'get'])->name('get');
    Route::get('create', [ProductController::class, 'create'])->name('create');
    Route::post('store', [ProductController::class, 'store'])->name('store');

    Route::group(['prefix' => '{product}'], function () {
        Route::get('show', [ProductController::class, 'show'])->name('show');
        Route::get('edit', [ProductController::class, 'edit'])->name('edit');
        Route::put('update', [ProductController::class, 'update'])->name('update');
        Route::patch('update', [ProductController::class, 'update'])->name('update');
        Route::post('restore', [ProductController::class, 'restore'])->name('restore');
        Route::delete('delete-permanently', [ProductController::class, 'deletePermanently'])
            ->name('delete-permanently');
        Route::patch('switch01', [ProductController::class, 'switch01'])->name('switch01');
        Route::delete('destroy', [ProductController::class, 'destroy'])->name('destroy');
    });

});
