<?php

// product
Breadcrumbs::register('home', function ($trail) {
    $trail->push(__('navs.general.home'), route('aodai.index'));
});
Breadcrumbs::register('detail', function ($trail, $product) {
    $categoriesAncestor = $product->productCategoryMain->getAncestorsAndSelf();
    if (!empty($categoriesAncestor)) {
        foreach ($categoriesAncestor as $key => $item) {
            $trail->push($item->name, route('aodai.category', [$item, 'slug' => str_slug($item->name)]));
        }
    }
    $trail->push($product->name, route('aodai.detail', [$product, 'slug' => $product->slug]));
});


// blog
Breadcrumbs::for('frontend.v1.index', function ($trail) {
    $trail->push(ucfirst(strtolower(__('labels.general.blog'))), route('frontend.v1.index'));
});
Breadcrumbs::for('frontend.v1.blog.detail', function ($trail, $blog) {
    $categoriesAncestor = $blog->category->getAncestorsAndSelf();
    if (!empty($categoriesAncestor)) {
        foreach ($categoriesAncestor as $key => $item) {
            $trail->push($item->name, route('frontend.v1.blog_category.category', [$item, 'slug' => $item->slug]));
        }
    }
    $trail->push($blog->name, route('frontend.v1.blog.detail', [$blog,$blog->slug]));
});
