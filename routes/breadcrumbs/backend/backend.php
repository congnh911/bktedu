<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

// cateogry
Breadcrumbs::for('admin.category.index', function ($trail) {
    $trail->push(__('labels.general.category'), route('admin.category.index',['type' => request()->get('type')]));
});
Breadcrumbs::for('admin.category.create', function ($trail) {
    $trail->parent('admin.category.index',['type' => request()->get('type')]);
    $trail->push(__('labels.general.create'), route('admin.category.create'));
});
Breadcrumbs::for('admin.category.edit', function ($trail, $id) {
    $trail->parent('admin.category.index');
    $trail->push(__('labels.general.update'), route('admin.category.edit', $id));
});

// product
Breadcrumbs::for('admin.product.index', function ($trail) {
    $trail->push(ucfirst(strtolower(__('labels.general.product'))), route('admin.product.index'));
});
Breadcrumbs::for('admin.product.create', function ($trail) {
    $trail->parent('admin.product.index');
    $trail->push(__('labels.general.create'), route('admin.product.create'));
});
Breadcrumbs::for('admin.product.edit', function ($trail, $id) {
    $trail->parent('admin.product.index');
    $trail->push(__('labels.general.product'), route('admin.category.edit', $id));
});

// blog
Breadcrumbs::for('admin.blog.index', function ($trail) {
    $trail->push(ucfirst(strtolower(__('labels.general.blog'))), route('admin.blog.index'));
});
Breadcrumbs::for('admin.blog.create', function ($trail) {
    $trail->parent('admin.blog.index');
    $trail->push(__('labels.general.create'), route('admin.blog.create'));
});
Breadcrumbs::for('admin.blog.edit', function ($trail, $id) {
    $trail->parent('admin.blog.index');
    $trail->push(__('labels.general.update'), route('admin.blog.edit', $id));
});


Breadcrumbs::for('admin.slide.index', function ($trail) {
    $trail->push(ucfirst(strtolower(__('labels.general.slide'))), route('admin.slide.index'));
});
Breadcrumbs::for('admin.slide.create', function ($trail) {
    $trail->parent('admin.slide.index');
    $trail->push(__('labels.general.create'), route('admin.slide.create'));
});
Breadcrumbs::for('admin.slide.edit', function ($trail, $id) {
    $trail->parent('admin.slide.index');
    $trail->push(__('labels.general.update'), route('admin.slide.edit', $id));
});


require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
