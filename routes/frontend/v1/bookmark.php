<?php

use App\Http\Controllers\Frontend\V1\BookmarkController;

Route::group(['prefix' => 'bookmark', 'as' => 'bookmark.'], function () {
    Route::get('/', [BookmarkController::class, 'index'])->name('index');
});
