<?php

use App\Http\Controllers\Frontend\V1\NotificationController;

Route::group(['prefix' => 'notification', 'as' => 'notification.','middleware' => 'auth'], function () {
    Route::get('/', [NotificationController::class, 'index'])->name('index');
    Route::get('mark-as-read', [NotificationController::class, 'markAsRead'])->name('mark_as_read');
    Route::get('/mark-as-read-all', [NotificationController::class, 'markAsReadAll'])->name('mark_as_read_all');
    Route::get('/get', [NotificationController::class, 'get'])->name('get');
});
