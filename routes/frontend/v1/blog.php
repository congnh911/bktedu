<?php

use App\Http\Controllers\Frontend\V1\BlogController;

Route::group(['prefix' => 'blog', 'as' => 'blog.'], function () {
    Route::get('/', [BlogController::class, 'index'])->name('index');
    Route::get('get-data', [BlogController::class, 'getData'])->name('get_data');
    Route::get('{blog}-{slug}', [BlogController::class, 'detail'])->name('detail');
});

Route::group(['prefix' => 'blog-category', 'as' => 'blog_category.'], function () {
    Route::get('{category}-{slug}', [BlogController::class, 'category'])->name('category');
});
